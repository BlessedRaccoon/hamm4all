## 1.1.3

### Fixes
- L'inscription fonctionne à nouveau correctement
- Un message clair apparaît désormais quand on essaye de créer un compte avec un nom qui existe déjà
- Les outils tels que l'HammerfestCreator n'étaient plus téléchargeables
- Fix de quelques liens cassés
- Correction mineures de certaines routes d'API
- Test d'un nouveau système qui permettra de faire des tests plus facilement

## 1.1.2

### Fixes
- Les actions d'édition et suppression des items dans les tableaux d'items sont désormais cachées quand on n'est pas connecté
- Le jeu ne s'affiche pas lors de la visualisation des items de ce même jeu
- Remaniemennt des informations affichées sur la page de chaque jeux pour plus de cohérence, pertinence et clarté
- Les miniatures des jeux prennent l'espace en hauteur si leur nom dépasse une ligne, au lieu de s'étendre horizontalement
- Correction de la modification d'un screen, qui ne fonctionnait pas correctement

## 1.1.1

### Fixes
- Les numéros des ordres s'affichent désormais dans la même police que ceux du créateur de niveaux, quelque soit le navigateur.
- Les miniatures de jeux affichent correctement la nuance "difficulté inconnue" ou "difficulté variable".
- Les parties de jeu comptent les niveaux correctement.

## 1.1

### Fonctionnalités
- La page des jeux affiche désormais le nombre de niveaux du jeu
- La page des parties de jeu affiche désormais le nombre de niveaux de la partie

### Améliorations
- Les vidéos acceptent désormais les liens Youtube classiques, et non seulement les liens `embed`
- L'affichage des labyrinthes commence désormais à gauche au lieu d'au milPgamethumieu

### Fixes
- Correction de l'affichage des miniatures de niveaux des dimensions
- Corrections de certaines parties de jeu qui voyaient leur séquences de miniatures dupliquées
- Correction de l'affichage des objets, qui n'avaient pas de limite de hauteur
- Correction de l'affichage de la grille sur un screenshot, qui a désormais bien 21 cases de large, et qui s'arrête bien à la lisière du screenshot en bas
- Les noms des séquences de niveaux sont à nouveau affichés, quoiqu'il faille les réinsérer manuellement.
- Les liens de la page des techniques sont désormais réparés
- Correction du mauvais affichage de la page d'erreur
- Les nombres des ordres ne vont plus au dessus de l'interface
- Fautes d'orthographe corrigées


## 1.0

- Recodage entier du site pour le mettre au goût du jour

### Fonctionnalités
- Ajout des contrées in-game
- Possibilité de changer l'ordre des screens dans les niveaux
- Possibilité d'ajouter plusieurs vidéos par niveau
- Les vidéos sont triées automatiquement par ordre présent et nombre de vies perdues
- Possibilité de supprimer une miniature de niveaux sans supprimer celles d'avant
- Possibilité de modifier une miniature de niveau sans supprimer celles d'avant
- Possibilité de réordonner les séquences de niveaux
- Ajout d'un tri en temps réel des items par jeu
- Possibilité de supprimer un item
- Utilisation acceptée du Markdown dans les descriptions des items
- Possibilité d'ajouter un ordre à un screen en utilisant le drag and drop

### Améliorations
- Renommage des `Contrées` en `Jeux`
- Révision du système d'ordre : l'ajout des fruits a été retiré
- Les labyrinthes peuvent être créés sans obligation de niveau d'entrée
- Les items custom et non custom sont tous réunis sur une même page, et sont tous éditables
- Fluidité et réactivité du site grandement améliorées, surtout lors de passages répétés entre les pages
- La description et les vidéos affichent désormais les dates et auteur de la dernière modification

### Fixes
- La connexion ne charge plus à l'infini lors d'une connexion avec mauvais mot de passe
- Certains bugs d'ajout de jeux et de miniatures sont corrigés

### Bugs connus
- L'édition de miniature de niveau est impossible sous Firefox. C'est dû à une incompatibilité avec un élément de formulaire. Malheureusement, implémenter une solution me prendrait plusieurs heures, je ne pense donc pas régler ce bug. Utilisez un autre navigateur pour éditer les miniatures de niveaux, comme Chrome ou Brave.
- L'affichage des dimensions a quelques ratés sur des petites résolutions. Ce sera réglé dans une prochaine mise à jour.
- Certains éléments des pages ne se rafraîchissent pas très bien après des modifications.
- Renommer un niveau depuis ce même niveau ne redirige pas vers la bonne page.
- Pour les objets custom déjà implémentés, il faut renseigner leur ID in-game avant d'autres modifications. A terme, cela pourra servir à gameresult pour afficher les objets custom.
- Les vidéos contiennent désormais la possibilité d'ajouter si l'ordre est présent, et le nombre de vies perdues. Comme ces informations n'étaient pas là avant, il faudra éditer chaque vidéo pour les ajouter. Cela ne prend que quelques secondes à chaque fois, mais c'est un peu fastidieux.
- Si vous voyez "ajouté par admin", c'est parce que je ne disposais pas de l'information pour pouvoir afficher le nom de l'utilisateur qui a ajouté l'élément.

## 0.19.1
- Mise en place (à nouveau) de la navigation entre les niveaux avec les flèches du clavier
- Meilleur tri des niveaux précédents : le plus haut est le niveau dont le nom est le plus petit
- Correction de l'affichage de la boîte des dimensions, qui était trop grande

## 0.19.0
- Révision d'une partie de la charte graphique : les couleurs sont moins violentes pour les yeux
- Suppression des niveaux précédents : ils sont maintenant calculés à la volée grâce aux dimensions et aux niveaux suivants. Plus besoin de les renseigner manuellement.
- Amélioration de la page des niveaux sur mobile
- Les formulaires modérateur pour ajouter des informations sont suouvent plus jolis

## 0.18.1
- Suppression complète de la catégorie "Concours" de l'onglet "Créations"
- Les formulaires de modification et d'ajouts de données sont relayés en bas de page
- Correction du bug des objets custom qui affichaient à chaque fois le coefficient
- Modification de la page sur les objets spéciaux
- Suppression du bandeau blanc qu'on pouvait avoir en bas de page
- Modifications mineures sur la position du texte et de certains éléments
- Meilleure gestion de l'interface de la plupart des pages sur mobile

## 0.18.0
- Création d'une page pour voir, ajouter, et supprimer les objets custom d'une contrée
- Légère amélioration de l'affichage des miniatures de contrées
- Les miniatures de contrées sont maintenant affichées à la bonne taille sur leur page
- Le changelog est maintenant accessible à tout le monde, sans être connecté
- Mise à jour et correction de certains textes
- Mise à jour des dépendances
- "Igor le Bonhomme de Neige" est à nouveau écoutable sur le site
- Suppression de la catégorie "Concours" de l'onglet "Créations"
- Mise à jour de 1.7.3 à 1.8.1 de l'éditeur de niveaux de moulins en téléchargement
- Changement de certaines couleurs pour accentuer le contraste