export default {
    collectCoverageFrom: ['src/**/*.ts'],
    "moduleFileExtensions": [
        "js",
        "json",
        "ts"
    ],
    "testRegex": ".*\\.(spec|test)\\.ts$",
    "transform": {
        "^.+\\.ts$": "ts-jest"
    },
    setupFiles: ["dotenv/config"],
    "coverageDirectory": "coverage",
    "testEnvironment": "node",
    "maxWorkers": 1,
    "maxConcurrency": 1,
    "verbose": true
};
  