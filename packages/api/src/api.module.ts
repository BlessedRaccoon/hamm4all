import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { apiModuleConfig, testApiModuleConfig } from './config/api.config';
import { LoggerMiddleware } from './core/logging/logging.middleware';
import { H4A_API } from '@hamm4all/shared';

@Module(apiModuleConfig)
export class ApiModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware)
            .exclude(
                { path: H4A_API.v1.files.get.getFullRawUri(), method: RequestMethod.GET},
                { path: H4A_API.v1.files.getRaw.getFullRawUri(), method: RequestMethod.GET}
            )
            .forRoutes('*');
    }
}

@Module(testApiModuleConfig)
export class TestApiModule {}