import { Controller, Get } from "@nestjs/common";
import { AuthorsService } from "./authors.service";
import { H4A_API } from "@hamm4all/shared";

@Controller()
export class AuthorsController {

    constructor(
        private readonly authorsService: AuthorsService
    ) {}

    @Get(H4A_API.v1.authors.list.getFullRawUri())
    async list() {
        return this.authorsService.findAll()
    }

}