import { Module } from "@nestjs/common";
import { AuthorsController } from "./authors.controller";
import { AuthorsService } from "./authors.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthorEntity } from "../../database/entities/authors.entity";

@Module({
    "controllers": [AuthorsController],
    "providers": [AuthorsService],
    "imports": [TypeOrmModule.forFeature([AuthorEntity])],
    "exports": [AuthorsService],
})
export class AuthorsModule {}