import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { AuthorEntity } from "../../database/entities/authors.entity";

@Injectable()
export class AuthorsService {

    constructor(
        @InjectRepository(AuthorEntity) private readonly authorsRepository: Repository<AuthorEntity>
    ) {}

    async findAll() {
        return this.authorsRepository.find()
    }

}