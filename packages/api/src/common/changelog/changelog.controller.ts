import { H4A_API, AddChangelogBody } from "@hamm4all/shared";
import { Body, Controller, Get, HttpCode, Post, UseGuards } from "@nestjs/common";
import { ChangelogService } from "./changelog.service";
import { AuthGuard } from "../../shared/auth/auth.guard";

@Controller({
    "version": "1",
})
export class ChangelogController {

    constructor(
        private readonly changelogService: ChangelogService
    ) {}

    @Get(H4A_API.v1.changelog.list.getFullRawUri())
    async getChangelogs() {
        return this.changelogService.getChangelogs();
    }

    @Post(H4A_API.v1.changelog.add.getFullRawUri())
    @UseGuards(AuthGuard(5))
    @HttpCode(200)
    async addChangelog(
        @Body() changelogAddBody: AddChangelogBody
    ) {
        return this.changelogService.addChangelog(changelogAddBody);
    }
}