import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ChangelogEntity } from "../../database/entities/changelog/changelog.entity";
import { AddChangelogBody, ChangelogForVersion } from "@hamm4all/shared";

@Injectable()
export class ChangelogService {

    constructor(
        @InjectRepository(ChangelogEntity) private readonly changelogRepository: Repository<ChangelogEntity>
    ) {}

    async addChangelog(changelogAddBody: AddChangelogBody): Promise<void> {

        const changelog = new ChangelogEntity();
        changelog.name = changelogAddBody.versionName;
        changelog.description = changelogAddBody.description;
        changelog.date = new Date();
        await this.changelogRepository.save(changelog);
    }

    async getChangelogs(): Promise<ChangelogForVersion[]> {
        const changelogs = await this.changelogRepository.find({
            "order": {
                "date": "DESC"
            }
        });

        return changelogs.map((changelog) => this.convertChangelogToChangelogRow(changelog));
    }

    private convertChangelogToChangelogRow(changelog: ChangelogEntity): ChangelogForVersion {
        return {
            "versionName": changelog.name,
            "createdAt": changelog.date.toISOString(),
            "description": changelog.description
        }
    }

}