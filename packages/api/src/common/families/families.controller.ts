import { Family, H4A_API } from "@hamm4all/shared";
import { Controller, Get } from "@nestjs/common";
import { FamiliesService } from "./families.service";


@Controller()
export class FamiliesController {

    constructor(
        private readonly familiesService: FamiliesService
    ) {}

    @Get(H4A_API.v1.families.list.getFullRawUri())
    public async list(): Promise<Family[]> {
        return this.familiesService.findAll();
    }


}