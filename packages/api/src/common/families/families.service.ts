import { Injectable } from "@nestjs/common";

@Injectable()
export class FamiliesService {

    private readonly families = [
        {
            "id": 0,
            "name": {
                "fr": "Objets de base",
                "en": "Basic items",
                "es": "Objetos de base"
            },
            "items": [
                {
                    "id": 0,
                    "rarity": 0,
                    "unlock": 10,
                    "name": {
                        "fr": "Alphabet Cristallin",
                        "en": "Cristal Alphabet",
                        "es": "Alfabeto Cristalino"
                    },
                    "img": "/assets/items/0.svg"
                },
                {
                    "id": 3,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Ballon de banquise",
                        "en": "Beach Ball",
                        "es": "Balón de playa"
                    },
                    "img": "/assets/items/3.svg"
                },
                {
                    "id": 4,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Lampe Fétvoveu",
                        "en": "Magic Lamp",
                        "es": "Lámpara Pidedeseo"
                    },
                    "img": "/assets/items/4.svg"
                },
                {
                    "id": 7,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Basket IcePump",
                        "en": "Running Shoes",
                        "es": "Botín IcePump"
                    },
                    "img": "/assets/items/7.svg"
                },
                {
                    "id": 13,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Cass-Tet",
                        "en": "Biker Helmet",
                        "es": "Casco"
                    },
                    "img": "/assets/items/13.svg"
                },
                {
                    "id": 21,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Enceinte Bessel-Son",
                        "en": "Jukebox Igor",
                        "es": "Altavoz Bájaloya"
                    },
                    "img": "/assets/items/21.svg"
                },
                {
                    "id": 24,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Hippo-flocon",
                        "en": "Iceberg Shower",
                        "es": "Pedrusco de los montes"
                    },
                    "img": "/assets/items/24.svg"
                },
                {
                    "id": 64,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Arc-en-miel",
                        "en": "Honey Rainbow",
                        "es": "Arcomiel"
                    },
                    "img": "/assets/items/64.svg"
                },
                {
                    "id": 74,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Esprit de l'orange",
                        "en": "Spirit of Orange",
                        "es": "Espíritu de la naranja"
                    },
                    "img": "/assets/items/74.svg"
                },
                {
                    "id": 77,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Lucidjané à crête bleue",
                        "en": "Blue Crested Fish",
                        "es": "Lucidjané con cresta azul"
                    },
                    "img": "/assets/items/77.svg"
                },
                {
                    "id": 84,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Talisman scorpide",
                        "en": "Scorpion Talisman",
                        "es": "Talismán escorpión"
                    },
                    "img": "/assets/items/84.svg"
                },
                {
                    "id": 102,
                    "rarity": 0,
                    "unlock": 1,
                    "name": {
                        "fr": "La Carotte d'Igor",
                        "en": "Igor's Carrot",
                        "es": "La Zanahoria de Igor"
                    },
                    "img": "/assets/items/102.svg"
                },
                {
                    "id": 113,
                    "rarity": 0,
                    "unlock": 10,
                    "name": {
                        "fr": "Cape de Tuberculoz",
                        "en": "The Robe of Tuber",
                        "es": "Capa de Tubérculo"
                    },
                    "img": "/assets/items/113.svg"
                },
                {
                    "id": 115,
                    "rarity": 0,
                    "unlock": 10,
                    "name": {
                        "fr": "Casque de Volleyfest",
                        "en": "The Helmet of Volleyfest",
                        "es": "Casco de Volleyfest"
                    },
                    "img": "/assets/items/115.svg"
                },
                {
                    "id": 116,
                    "rarity": 0,
                    "unlock": 10,
                    "name": {
                        "fr": "Joyau d'Ankhel",
                        "en": "The Ankhel Jewel",
                        "es": "Joya de Ankhel"
                    },
                    "img": "/assets/items/116.svg"
                },
                {
                    "id": 117,
                    "rarity": 0,
                    "unlock": 10,
                    "name": {
                        "fr": "Clé de Gordon",
                        "en": "Gordon's Key",
                        "es": "Llave de Gordon"
                    },
                    "img": "/assets/items/117.svg"
                }
            ]
        },
        {
            "id": 1,
            "name": {
                "fr": "Kit de premiers secours",
                "en": "First aid kit",
                "es": "Kit de primeros auxilios"
            },
            "items": [
                {
                    "id": 1,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Bouclidur en or",
                        "en": "Golden Shield",
                        "es": "Escudo de madera y oro"
                    },
                    "img": "/assets/items/1.svg"
                },
                {
                    "id": 5,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Lampe Léveussonfé",
                        "en": "Black Magic Lam",
                        "es": "Lámpara Negra"
                    },
                    "img": "/assets/items/5.svg"
                },
                {
                    "id": 8,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Etoile des neiges",
                        "en": "Gold Star",
                        "es": "Estrella de las nieves"
                    },
                    "img": "/assets/items/8.svg"
                },
                {
                    "id": 11,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Parapluie rouge",
                        "en": "Red Umbrella",
                        "es": "Paraguas rojo"
                    },
                    "img": "/assets/items/11.svg"
                },
                {
                    "id": 18,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Pissenlit tropical",
                        "en": "Tropical Dandelion",
                        "es": "Flor tropical"
                    },
                    "img": "/assets/items/18.svg"
                },
                {
                    "id": 22,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Vieille chaussure trouée",
                        "en": "Heavy Weight Boots",
                        "es": "Zapato viejo con agujeros"
                    },
                    "img": "/assets/items/22.svg"
                },
                {
                    "id": 23,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Boule cristalline",
                        "en": "Orb of Ice",
                        "es": "Bola cristalina"
                    },
                    "img": "/assets/items/23.svg"
                },
                {
                    "id": 25,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Flamme froide",
                        "en": "Snow Flame",
                        "es": "Llama fría"
                    },
                    "img": "/assets/items/25.svg"
                },
                {
                    "id": 27,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Porte-grenouilles",
                        "en": "Special Lilypad",
                        "es": "Posa-ranas"
                    },
                    "img": "/assets/items/27.svg"
                },
                {
                    "id": 28,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Bibelot en argent",
                        "en": "Silver Trophy",
                        "es": "Baratija de plata"
                    },
                    "img": "/assets/items/28.svg"
                },
                {
                    "id": 38,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Totem des dinoz",
                        "en": "Dino Totem Pole",
                        "es": "Tótem de los Dinos"
                    },
                    "img": "/assets/items/38.svg"
                },
                {
                    "id": 39,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Tête de granit lestée de plomb",
                        "en": "Granite Easter Island",
                        "es": "Cabeza de granito"
                    },
                    "img": "/assets/items/39.svg"
                },
                {
                    "id": 65,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Bouée canard",
                        "en": "Rubber Ducky",
                        "es": "Flotador de pato"
                    },
                    "img": "/assets/items/65.svg"
                },
                {
                    "id": 66,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Branche de Kipik",
                        "en": "Magical Cactus",
                        "es": "Rama de Kipik"
                    },
                    "img": "/assets/items/66.svg"
                },
                {
                    "id": 69,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Koulraoule des îles",
                        "en": "Turtle Islands",
                        "es": "Tortuga de las islas"
                    },
                    "img": "/assets/items/69.svg"
                },
                {
                    "id": 71,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Chaud devant !",
                        "en": "Dragon's Breath",
                        "es": "¡Emana calor!"
                    },
                    "img": "/assets/items/71.svg"
                },
                {
                    "id": 82,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Jugement avant-dernier",
                        "en": "Eye of Providence",
                        "es": "Juicio penúltimo"
                    },
                    "img": "/assets/items/82.svg"
                },
                {
                    "id": 106,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "Livre des champignons",
                        "en": "Book of Mushrooms",
                        "es": "Libro de los hongos"
                    },
                    "img": "/assets/items/106.svg"
                },
                {
                    "id": 107,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "Livre des étoiles",
                        "en": "Book of Stars",
                        "es": "Libro de las estrellas"
                    },
                    "img": "/assets/items/107.svg"
                }
            ]
        },
        {
            "id": 2,
            "name": {
                "fr": "Destruction massive",
                "en": "Massive destruction",
                "es": "Destrucción masiva"
            },
            "items": [
                {
                    "id": 2,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Bouclidur argenté",
                        "en": "Silver Shield",
                        "es": "Escudo plateado"
                    },
                    "img": "/assets/items/2.svg"
                },
                {
                    "id": 6,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Paix intérieure",
                        "en": "Inner Peace",
                        "es": "Paz interior"
                    },
                    "img": "/assets/items/6.svg"
                },
                {
                    "id": 9,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Mauvais-oeil",
                        "en": "Bad eye",
                        "es": "Ojo malo"
                    },
                    "img": "/assets/items/9.svg"
                },
                {
                    "id": 12,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Parapluie bleu",
                        "en": "Blue Umbrella",
                        "es": "Paraguas azul"
                    },
                    "img": "/assets/items/12.svg"
                },
                {
                    "id": 19,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Tournelune",
                        "en": "Explosive Sunflower",
                        "es": "Giraluna"
                    },
                    "img": "/assets/items/19.svg"
                },
                {
                    "id": 30,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Lunettes tournantes bleues",
                        "en": "Blue Shades",
                        "es": "Gafas volteadoras azules"
                    },
                    "img": "/assets/items/30.svg"
                },
                {
                    "id": 31,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Lunettes renversantes rouges",
                        "en": "Red Shades",
                        "es": "Gafas revolteadoras rojas"
                    },
                    "img": "/assets/items/31.svg"
                },
                {
                    "id": 36,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Ig'or",
                        "en": "Golden Igor",
                        "es": "Igoro"
                    },
                    "img": "/assets/items/36.svg"
                },
                {
                    "id": 70,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Trêfle commun",
                        "en": "Four-Leaf Clover",
                        "es": "Trébol común"
                    },
                    "img": "/assets/items/70.svg"
                },
                {
                    "id": 72,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Chapeau de Mage-Gris",
                        "en": "Magician's Hat",
                        "es": "Sombrero de Mago Gris"
                    },
                    "img": "/assets/items/72.svg"
                },
                {
                    "id": 73,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Feuille de sinopée",
                        "en": "Leaf",
                        "es": "Hoja de árbol"
                    },
                    "img": "/assets/items/73.svg"
                },
                {
                    "id": 75,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Esprit de la pluie",
                        "en": "Spirit of Rain",
                        "es": "Espíritu de la lluvia"
                    },
                    "img": "/assets/items/75.svg"
                },
                {
                    "id": 76,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Esprit des arbres",
                        "en": "Spirit of Trees",
                        "es": "Espíritu de los árboles"
                    },
                    "img": "/assets/items/76.svg"
                },
                {
                    "id": 80,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Escargot Poussépa",
                        "en": "Snail Speed",
                        "es": "Caracol Noempujes"
                    },
                    "img": "/assets/items/80.svg"
                },
                {
                    "id": 81,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Perle nacrée des murlocs",
                        "en": "Murloc's Pearl",
                        "es": "Perla de nácar de los murlocs"
                    },
                    "img": "/assets/items/81.svg"
                },
                {
                    "id": 86,
                    "rarity": 4,
                    "unlock": 10,
                    "name": {
                        "fr": "Surprise de paille",
                        "en": "Ghost Candy",
                        "es": "Sorpresa fantasmagórica"
                    },
                    "img": "/assets/items/86.svg"
                },
                {
                    "id": 87,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Larve d'oenopterius",
                        "en": "King Larvae",
                        "es": "Larva de oenopterius"
                    },
                    "img": "/assets/items/87.svg"
                },
                {
                    "id": 88,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Pokuté",
                        "en": "Hare Nochi Guuuuu!",
                        "es": "Pokuté"
                    },
                    "img": "/assets/items/88.svg"
                },
                {
                    "id": 89,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Oeuf de Tzongre",
                        "en": "Dragon Egg",
                        "es": "Ojo de Tzongre"
                    },
                    "img": "/assets/items/89.svg"
                },
                {
                    "id": 90,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Fulguro pieds-en-mousse",
                        "en": "Slippery Bananas",
                        "es": "Fulguro pies-pesados"
                    },
                    "img": "/assets/items/90.svg"
                },
                {
                    "id": 93,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Boit'a'messages",
                        "en": "Mail Box",
                        "es": "Buzón zon"
                    },
                    "img": "/assets/items/93.svg"
                },
                {
                    "id": 95,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "Cagnotte de Tuberculoz",
                        "en": "Tuber's Bag",
                        "es": "Saco de Tubérculo"
                    },
                    "img": "/assets/items/95.svg"
                },
                {
                    "id": 96,
                    "rarity": 4,
                    "unlock": 10,
                    "name": {
                        "fr": "Perle flamboyante",
                        "en": "Flameberg Pearl",
                        "es": "Perla flameante"
                    },
                    "img": "/assets/items/96.svg"
                },
                {
                    "id": 99,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "Poils de Chourou",
                        "en": "Chourou's Hair",
                        "es": "Pelos de Chourou"
                    },
                    "img": "/assets/items/99.svg"
                },
                {
                    "id": 101,
                    "rarity": 0,
                    "unlock": 10,
                    "name": {
                        "fr": "Colis surprise",
                        "en": "Surprise Package",
                        "es": "Paquete sorpresa"
                    },
                    "img": "/assets/items/101.svg"
                },
                {
                    "id": 112,
                    "rarity": 6,
                    "unlock": 1,
                    "name": {
                        "fr": "Pioupiou carnivore",
                        "en": "Carnivorous PiouPiou",
                        "es": "Píopío carnívoro"
                    },
                    "img": "/assets/items/112.svg"
                }
            ]
        },
        {
            "id": 3,
            "name": {
                "fr": "Champignons",
                "en": "Mushrooms",
                "es": "Setas"
            },
            "items": [
                {
                    "id": 14,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Délice hallucinogène bleu",
                        "en": "Blue Hallucinogenic Mushroom",
                        "es": "Delicia alucinógena azul"
                    },
                    "img": "/assets/items/14.svg"
                },
                {
                    "id": 15,
                    "rarity": 4,
                    "unlock": 10,
                    "name": {
                        "fr": "Champignon rigolo rouge",
                        "en": "Red Delight Mushroom",
                        "es": "Seta chuli y roja"
                    },
                    "img": "/assets/items/15.svg"
                },
                {
                    "id": 16,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "Figonassée grimpante",
                        "en": "Green Beer Mushroom",
                        "es": "Pequeña Walalu de los bosques"
                    },
                    "img": "/assets/items/16.svg"
                },
                {
                    "id": 17,
                    "rarity": 6,
                    "unlock": 10,
                    "name": {
                        "fr": "Petit Waoulalu des bois",
                        "en": "Golded Mushroom",
                        "es": "Seta dorada trepadora"
                    },
                    "img": "/assets/items/17.svg"
                }
            ]
        },
        {
            "id": 4,
            "name": {
                "fr": "Cartes à jouer",
                "en": "Deck of cards",
                "es": "Baraja de cartas"
            },
            "items": [
                {
                    "id": 32,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "As de pique",
                        "en": "Ace of Spades",
                        "es": "As de picas"
                    },
                    "img": "/assets/items/32.svg"
                },
                {
                    "id": 33,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "As de trêfle",
                        "en": "Ace of Clubs",
                        "es": "As de tréboles"
                    },
                    "img": "/assets/items/33.svg"
                },
                {
                    "id": 34,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "As de carreau",
                        "en": "Ace of Diamonds",
                        "es": "As de diamantes"
                    },
                    "img": "/assets/items/34.svg"
                },
                {
                    "id": 35,
                    "rarity": 6,
                    "unlock": 10,
                    "name": {
                        "fr": "As de coeur",
                        "en": "Ace of Hearts",
                        "es": "As de corazones"
                    },
                    "img": "/assets/items/35.svg"
                }
            ]
        },
        {
            "id": 5,
            "name": {
                "fr": "Signes du zodiaque",
                "en": "Zodiac signs",
                "es": "Signos del zodíaco"
            },
            "items": [
                {
                    "id": 40,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Sagittaire",
                        "en": "Sagittarius",
                        "es": "Sagitario"
                    },
                    "img": "/assets/items/40.svg"
                },
                {
                    "id": 41,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Capricorne",
                        "en": "Capricorn",
                        "es": "Capricornio"
                    },
                    "img": "/assets/items/41.svg"
                },
                {
                    "id": 42,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Lion",
                        "en": "Leo",
                        "es": "Leo"
                    },
                    "img": "/assets/items/42.svg"
                },
                {
                    "id": 43,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Taureau",
                        "en": "Taurus",
                        "es": "Tauro"
                    },
                    "img": "/assets/items/43.svg"
                },
                {
                    "id": 44,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Balance",
                        "en": "Libra",
                        "es": "Libra"
                    },
                    "img": "/assets/items/44.svg"
                },
                {
                    "id": 45,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Bélier",
                        "en": "Aries",
                        "es": "Aries"
                    },
                    "img": "/assets/items/45.svg"
                },
                {
                    "id": 46,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Scorpion",
                        "en": "Scorpio",
                        "es": "Escorpión"
                    },
                    "img": "/assets/items/46.svg"
                },
                {
                    "id": 47,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Cancer",
                        "en": "Cancer",
                        "es": "Cáncer"
                    },
                    "img": "/assets/items/47.svg"
                },
                {
                    "id": 48,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Verseau",
                        "en": "Aquarius",
                        "es": "Acuario"
                    },
                    "img": "/assets/items/48.svg"
                },
                {
                    "id": 49,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Gémeaux",
                        "en": "Gemini",
                        "es": "Géminis"
                    },
                    "img": "/assets/items/49.svg"
                },
                {
                    "id": 50,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Poisson",
                        "en": "Pisces",
                        "es": "Piscis"
                    },
                    "img": "/assets/items/50.svg"
                },
                {
                    "id": 51,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Vierge",
                        "en": "Virgo",
                        "es": "Virgo"
                    },
                    "img": "/assets/items/51.svg"
                }
            ]
        },
        {
            "id": 6,
            "name": {
                "fr": "Potions du zodiaque",
                "en": "Zodiac potions",
                "es": "Pociones del zodíaco"
            },
            "items": [
                {
                    "id": 52,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Sagittaire",
                        "en": "Potion of Sagitarius",
                        "es": "Elixir de Sagitario"
                    },
                    "img": "/assets/items/52.svg"
                },
                {
                    "id": 53,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Capricorne",
                        "en": "Potion of Capricorn",
                        "es": "Elixir de Capricornio"
                    },
                    "img": "/assets/items/53.svg"
                },
                {
                    "id": 54,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Lion",
                        "en": "Potion of Leo",
                        "es": "Elixir de Leo"
                    },
                    "img": "/assets/items/54.svg"
                },
                {
                    "id": 55,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Taureau",
                        "en": "Potion of Taurus",
                        "es": "Elixir de Tauro"
                    },
                    "img": "/assets/items/55.svg"
                },
                {
                    "id": 56,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir de la Balance",
                        "en": "Potion of Libra",
                        "es": "Elixir de Libra"
                    },
                    "img": "/assets/items/56.svg"
                },
                {
                    "id": 57,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Bélier",
                        "en": "Potion of Aries",
                        "es": "Elixir de Aries"
                    },
                    "img": "/assets/items/57.svg"
                },
                {
                    "id": 58,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Scorpion",
                        "en": "Potion of Scorpio",
                        "es": "Elixir de Escorpión"
                    },
                    "img": "/assets/items/58.svg"
                },
                {
                    "id": 59,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Cancer",
                        "en": "Potion of Cancer",
                        "es": "Elixir de Cáncer"
                    },
                    "img": "/assets/items/59.svg"
                },
                {
                    "id": 60,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Verseau",
                        "en": "Potion of Aquarius",
                        "es": "Elixir de Acuario"
                    },
                    "img": "/assets/items/60.svg"
                },
                {
                    "id": 61,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir des Gémeaux",
                        "en": "Potion of Gemini",
                        "es": "Elixir de Géminis"
                    },
                    "img": "/assets/items/61.svg"
                },
                {
                    "id": 62,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir du Poisson",
                        "en": "Potion of Pisces",
                        "es": "Elixir de Acuario"
                    },
                    "img": "/assets/items/62.svg"
                },
                {
                    "id": 63,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Elixir de la Vierge",
                        "en": "Potion of Virgo",
                        "es": "Elixir de Virgo"
                    },
                    "img": "/assets/items/63.svg"
                }
            ]
        },
        {
            "id": 7,
            "name": {
                "fr": "Coeur 1",
                "en": "Heart 1",
                "es": "Corazón 1"
            },
            "items": [
                {
                    "id": 103,
                    "rarity": 1,
                    "unlock": 1,
                    "name": {
                        "fr": "Vie palpitante",
                        "en": "Wonderful Life!",
                        "es": "Vida palpitante"
                    },
                    "img": "/assets/items/103.svg"
                }
            ]
        },
        {
            "id": 8,
            "name": {
                "fr": "Coeur 2",
                "en": "Heart 2",
                "es": "Corazón 2"
            },
            "items": [
                {
                    "id": 104,
                    "rarity": 1,
                    "unlock": 1,
                    "name": {
                        "fr": "Vie aventureuse",
                        "en": "Heart of an Adventurer",
                        "es": "Vida aventurosa"
                    },
                    "img": "/assets/items/104.svg"
                }
            ]
        },
        {
            "id": 9,
            "name": {
                "fr": "Coeur 3",
                "en": "Heart 3",
                "es": "Corazón 3"
            },
            "items": [
                {
                    "id": 105,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Vie épique",
                        "en": "Final Destiny",
                        "es": "Vida épica"
                    },
                    "img": "/assets/items/105.svg"
                }
            ]
        },
        {
            "id": 10,
            "name": {
                "fr": "Trésor des pirates",
                "en": "Pirate treasure",
                "es": "Tesoro de los piratas"
            },
            "items": [
                {
                    "id": 20,
                    "rarity": 1,
                    "unlock": 10,
                    "name": {
                        "fr": "Coffre d'Anarchipel",
                        "en": "Treasure Chest",
                        "es": "Cofre de Anarchipiélago"
                    },
                    "img": "/assets/items/20.svg"
                },
                {
                    "id": 78,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Filandreux rougeoyant",
                        "en": "Flame Fish",
                        "es": "Fibroso rojito"
                    },
                    "img": "/assets/items/78.svg"
                },
                {
                    "id": 79,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Poisson empereur",
                        "en": "Fish Emperor",
                        "es": "Pez emperador"
                    },
                    "img": "/assets/items/79.svg"
                },
                {
                    "id": 91,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Couvre-chef de Luffy",
                        "en": "Luffy's Hat",
                        "es": "Sombrero de paja de Luffy"
                    },
                    "img": "/assets/items/91.svg"
                },
                {
                    "id": 94,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Anneau Antok",
                        "en": "Antok Ring",
                        "es": "Anillo Antok"
                    },
                    "img": "/assets/items/94.svg"
                }
            ]
        },
        {
            "id": 11,
            "name": {
                "fr": "Gadgets Motion-Twin",
                "en": "Motion-Twin Gadgets",
                "es": "Cachibaches Motion-Twin"
            },
            "items": [
                {
                    "id": 83,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Jugement dernier",
                        "en": "Final Judgment",
                        "es": "Juicio final"
                    },
                    "img": "/assets/items/83.svg"
                },
                {
                    "id": 97,
                    "rarity": 5,
                    "unlock": 10,
                    "name": {
                        "fr": "Perle vertillante",
                        "en": "Green-Moss Pearl ",
                        "es": "Perla verdeante"
                    },
                    "img": "/assets/items/97.svg"
                },
                {
                    "id": 98,
                    "rarity": 6,
                    "unlock": 10,
                    "name": {
                        "fr": "Grosse pé-perle ",
                        "en": "Super-Purple Pearl ",
                        "es": "Gran pe-perla"
                    },
                    "img": "/assets/items/98.svg"
                },
                {
                    "id": 100,
                    "rarity": 4,
                    "unlock": 10,
                    "name": {
                        "fr": "Pocket-Guu",
                        "en": "Pocket-Guu",
                        "es": "Guu de bolsillo"
                    },
                    "img": "/assets/items/100.svg"
                },
                {
                    "id": 108,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Parapluie Frutiparc",
                        "en": "Green Umbrella",
                        "es": "Paraguas Frutiparc"
                    },
                    "img": "/assets/items/108.svg"
                }
            ]
        },
        {
            "id": 12,
            "name": {
                "fr": "Armement norvégien expérimental",
                "en": "Norwegian experimental armament",
                "es": "Armamento noruego experimental"
            },
            "items": [
                {
                    "id": 29,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Bague 'Thermostat 8'",
                        "en": "Ring of Fire",
                        "es": "Anillo 'Termostato 8'"
                    },
                    "img": "/assets/items/29.svg"
                },
                {
                    "id": 37,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Collier rafraîchissant",
                        "en": "Necklace of Frost",
                        "es": "Collar refrescante"
                    },
                    "img": "/assets/items/37.svg"
                },
                {
                    "id": 67,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Anneau de Guillaume Tell",
                        "en": "Ring of William Tell",
                        "es": "Anillo de Omaíta"
                    },
                    "img": "/assets/items/67.svg"
                },
                {
                    "id": 85,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Baton tonnerre",
                        "en": "Hammer Time 3000",
                        "es": "Martillo"
                    },
                    "img": "/assets/items/85.svg"
                },
                {
                    "id": 92,
                    "rarity": 4,
                    "unlock": 10,
                    "name": {
                        "fr": "Chapeau violin",
                        "en": "Chopper's Hat",
                        "es": "Sombrero de Tony Tony Chopper"
                    },
                    "img": "/assets/items/92.svg"
                }
            ]
        },
        {
            "id": 13,
            "name": {
                "fr": "Eclairage antique",
                "en": "Ancient light",
                "es": "Alumbrado antiguo"
            },
            "items": [
                {
                    "id": 68,
                    "rarity": 2,
                    "unlock": 10,
                    "name": {
                        "fr": "Bougie",
                        "en": "Candle of Light",
                        "es": "Vela"
                    },
                    "img": "/assets/items/68.svg"
                }
            ]
        },
        {
            "id": 14,
            "name": {
                "fr": "Igor-Newton",
                "en": "Igor-Newton",
                "es": "Igor-Newton"
            },
            "items": [
                {
                    "id": 26,
                    "rarity": 3,
                    "unlock": 10,
                    "name": {
                        "fr": "Ampoule 30 watts",
                        "en": "Light Bulb",
                        "es": "Bombilla 30 watios"
                    },
                    "img": "/assets/items/26.svg"
                }
            ]
        },
        {
            "id": 15,
            "name": {
                "fr": "Flocon 1",
                "en": "Snowflake 1",
                "es": "Copo 1"
            },
            "items": [
                {
                    "id": 109,
                    "rarity": 3,
                    "unlock": 1,
                    "name": {
                        "fr": "Flocon simple",
                        "en": "Snowflake",
                        "es": "Copo de nieve simple"
                    },
                    "img": "/assets/items/109.svg"
                }
            ]
        },
        {
            "id": 16,
            "name": {
                "fr": "Flocon 2",
                "en": "Snowflake 2",
                "es": "Copo 2"
            },
            "items": [
                {
                    "id": 110,
                    "rarity": 4,
                    "unlock": 1,
                    "name": {
                        "fr": "Flocon bizarre",
                        "en": "Glacier",
                        "es": "Copo de nieve raro"
                    },
                    "img": "/assets/items/110.svg"
                }
            ]
        },
        {
            "id": 17,
            "name": {
                "fr": "Flocon 3",
                "en": "Snowflake 3",
                "es": "Copo 3"
            },
            "items": [
                {
                    "id": 111,
                    "rarity": 5,
                    "unlock": 1,
                    "name": {
                        "fr": "Flocon ENORME !",
                        "en": "Gon Ga Jora",
                        "es": "Copo de nieve gigantesco"
                    },
                    "img": "/assets/items/111.svg"
                }
            ]
        },
        {
            "id": 18,
            "name": {
                "fr": "Kit d'appel main libre",
                "en": "Car Kit",
                "es": "Kit de manos libres"
            },
            "items": [
                {
                    "id": 10,
                    "rarity": 1,
                    "unlock": 5,
                    "name": {
                        "fr": "Téléphone-phone-phone",
                        "en": "Telephone",
                        "es": "Teléfono-no-no"
                    },
                    "img": "/assets/items/10.svg"
                }
            ]
        },
        {
            "id": 19,
            "name": {
                "fr": "Mario party",
                "en": "Mario Party",
                "es": "Fiesta Mario"
            },
            "items": [
                {
                    "id": 114,
                    "rarity": 4,
                    "unlock": 10,
                    "name": {
                        "fr": "Mode Mario",
                        "en": "Mario Mode!",
                        "es": "Modo Mario"
                    },
                    "img": "/assets/items/114.svg"
                }
            ]
        },
        {
            "id": 1000,
            "name": {
                "fr": "Petites gâteries",
                "en": "Little treats",
                "es": "Pequeñas travesuras"
            },
            "items": [
                {
                    "id": 1000,
                    "rarity": 0,
                    "value": "--",
                    "name": {
                        "fr": "Cristaux d'Hammerfest",
                        "en": "Hammerfest Crystal",
                        "es": "Cristales de Hammerfest"
                    },
                    "img": "/assets/items/1000.svg"
                },
                {
                    "id": 1003,
                    "rarity": 1,
                    "value": 500,
                    "name": {
                        "fr": "Bonbon Berlinmauve",
                        "en": "Seed Candy",
                        "es": "Caramelo Berlinmalva"
                    },
                    "img": "/assets/items/1003.svg"
                },
                {
                    "id": 1008,
                    "rarity": 0,
                    "value": "--",
                    "name": {
                        "fr": "Diamant Oune-difaïned",
                        "en": "Difaïned Diamond",
                        "es": "Diamante difaïned"
                    },
                    "img": "/assets/items/1008.svg"
                },
                {
                    "id": 1013,
                    "rarity": 1,
                    "value": 300,
                    "name": {
                        "fr": "Muffin aux cailloux",
                        "en": "Muffin",
                        "es": "Magdalena de piedras"
                    },
                    "img": "/assets/items/1013.svg"
                },
                {
                    "id": 1017,
                    "rarity": 0,
                    "value": "--",
                    "name": {
                        "fr": "Pierres du Changement",
                        "en": "Magical Stones",
                        "es": "Piedras del Cambio"
                    },
                    "img": "/assets/items/1017.svg"
                },
                {
                    "id": 1027,
                    "rarity": 1,
                    "value": 600,
                    "name": {
                        "fr": "Réglisse rouge",
                        "en": "Red Licorice",
                        "es": "Chuchería"
                    },
                    "img": "/assets/items/1027.svg"
                },
                {
                    "id": 1041,
                    "rarity": 1,
                    "value": 750,
                    "name": {
                        "fr": "Sucette acidulée",
                        "en": "Lollipop",
                        "es": "Piruleta"
                    },
                    "img": "/assets/items/1041.svg"
                },
                {
                    "id": 1043,
                    "rarity": 1,
                    "value": 900,
                    "name": {
                        "fr": "Sorbet au plastique",
                        "en": "Plastic Sorbet",
                        "es": "Polo de plástico"
                    },
                    "img": "/assets/items/1043.svg"
                },
                {
                    "id": 1047,
                    "rarity": 0,
                    "value": 5,
                    "name": {
                        "fr": "Bleuet",
                        "en": "Sapphire Mushroom",
                        "es": "Seta azulada"
                    },
                    "img": "/assets/items/1047.svg"
                },
                {
                    "id": 1048,
                    "rarity": 0,
                    "value": 10,
                    "name": {
                        "fr": "Rougeoyant",
                        "en": "Ruby Mushroom",
                        "es": "Rojeante"
                    },
                    "img": "/assets/items/1048.svg"
                },
                {
                    "id": 1049,
                    "rarity": 0,
                    "value": 15,
                    "name": {
                        "fr": "Verdifiant",
                        "en": "Emerald Mushroom",
                        "es": "Verdifiante"
                    },
                    "img": "/assets/items/1049.svg"
                },
                {
                    "id": 1050,
                    "rarity": 0,
                    "value": 25,
                    "name": {
                        "fr": "KassDent",
                        "en": "Sliver Mushroom",
                        "es": "Rompedientes"
                    },
                    "img": "/assets/items/1050.svg"
                },
                {
                    "id": 1051,
                    "rarity": 0,
                    "value": 2500,
                    "name": {
                        "fr": "Pièce d'or secrète",
                        "en": "Secret Coin",
                        "es": "Moneda de oro secreta"
                    },
                    "img": "/assets/items/1051.svg"
                },
                {
                    "id": 1169,
                    "rarity": 0,
                    "value": 1500,
                    "name": {
                        "fr": "Etoile Toulaho",
                        "en": "Starfish",
                        "es": "Estrella Toulaho"
                    },
                    "img": "/assets/items/1169.svg"
                },
                {
                    "id": 1185,
                    "rarity": 0,
                    "value": 250,
                    "name": {
                        "fr": "Anneau hérissé",
                        "en": "Bristling Ring",
                        "es": "Anillo de erizo azul"
                    },
                    "img": "/assets/items/1185.svg"
                }
            ]
        },
        {
            "id": 1001,
            "name": {
                "fr": "Aliments classiques",
                "en": "Classic food",
                "es": "Alimentos clásicos"
            },
            "items": [
                {
                    "id": 1022,
                    "rarity": 3,
                    "value": 10000,
                    "name": {
                        "fr": "Liquide bizarre",
                        "en": "Bizarre Liquid",
                        "es": "Líquido raro"
                    },
                    "img": "/assets/items/1022.svg"
                },
                {
                    "id": 1024,
                    "rarity": 3,
                    "value": 20000,
                    "name": {
                        "fr": "Liquide étrange",
                        "en": "Strange Liquid",
                        "es": "Líquido extraño"
                    },
                    "img": "/assets/items/1024.svg"
                },
                {
                    "id": 1025,
                    "rarity": 1,
                    "value": 500,
                    "name": {
                        "fr": "Oeuf cru",
                        "en": "Raw Egg",
                        "es": "Huevo crudo"
                    },
                    "img": "/assets/items/1025.svg"
                },
                {
                    "id": 1026,
                    "rarity": 1,
                    "value": 1300,
                    "name": {
                        "fr": "Gland gnan-gnan",
                        "en": "Potato",
                        "es": "Gran ñan-ñan"
                    },
                    "img": "/assets/items/1026.svg"
                },
                {
                    "id": 1028,
                    "rarity": 1,
                    "value": 1000,
                    "name": {
                        "fr": "Oeuf Au Plat",
                        "en": "Plain Egg",
                        "es": "Huevo frito"
                    },
                    "img": "/assets/items/1028.svg"
                },
                {
                    "id": 1040,
                    "rarity": 3,
                    "value": 9000,
                    "name": {
                        "fr": "Sushi thon",
                        "en": "Fish Sushi",
                        "es": "Sushi de atún"
                    },
                    "img": "/assets/items/1040.svg"
                },
                {
                    "id": 1069,
                    "rarity": 2,
                    "value": 50,
                    "name": {
                        "fr": "Cacahuete secrète",
                        "en": "Peanut",
                        "es": "Cacahuete secreto"
                    },
                    "img": "/assets/items/1069.svg"
                },
                {
                    "id": 1070,
                    "rarity": 2,
                    "value": 2500,
                    "name": {
                        "fr": "P'tit fantome",
                        "en": "Small Ghost",
                        "es": "Fantasmita"
                    },
                    "img": "/assets/items/1070.svg"
                },
                {
                    "id": 1071,
                    "rarity": 2,
                    "value": 1800,
                    "name": {
                        "fr": "Cookie deshydraté",
                        "en": "Hard Cookie",
                        "es": "Cookie deshidratada"
                    },
                    "img": "/assets/items/1071.svg"
                },
                {
                    "id": 1073,
                    "rarity": 4,
                    "value": 12000,
                    "name": {
                        "fr": "Piment farceur",
                        "en": "Hot Pepper",
                        "es": "Pimiento del piquillo"
                    },
                    "img": "/assets/items/1073.svg"
                },
                {
                    "id": 1074,
                    "rarity": 2,
                    "value": 2250,
                    "name": {
                        "fr": "Soja Max IceCream",
                        "en": "Bread",
                        "es": "Helado de Soja Max"
                    },
                    "img": "/assets/items/1074.svg"
                },
                {
                    "id": 1075,
                    "rarity": 4,
                    "value": 7777,
                    "name": {
                        "fr": "Bouquet de steack",
                        "en": "Bouquet",
                        "es": "Ramo de carne"
                    },
                    "img": "/assets/items/1075.svg"
                },
                {
                    "id": 1077,
                    "rarity": 3,
                    "value": 5,
                    "name": {
                        "fr": "Graine de tournesol",
                        "en": "Grain",
                        "es": "Semilla de girasol"
                    },
                    "img": "/assets/items/1077.svg"
                },
                {
                    "id": 1078,
                    "rarity": 5,
                    "value": 1500,
                    "name": {
                        "fr": "Croissant confit",
                        "en": "Croissant",
                        "es": "Croissant confitado"
                    },
                    "img": "/assets/items/1078.svg"
                },
                {
                    "id": 1079,
                    "rarity": 1,
                    "value": 800,
                    "name": {
                        "fr": "Haricot paresseux",
                        "en": "Beans",
                        "es": "Judía perezosa"
                    },
                    "img": "/assets/items/1079.svg"
                },
                {
                    "id": 1080,
                    "rarity": 3,
                    "value": 1200,
                    "name": {
                        "fr": "Lapin-choco",
                        "en": "Chocolate Bunny",
                        "es": "Conejo de chocolate"
                    },
                    "img": "/assets/items/1080.svg"
                },
                {
                    "id": 1081,
                    "rarity": 1,
                    "value": 900,
                    "name": {
                        "fr": "Biloo",
                        "en": "Beer",
                        "es": "Hidromiel de los Bosques"
                    },
                    "img": "/assets/items/1081.svg"
                },
                {
                    "id": 1082,
                    "rarity": 2,
                    "value": 3000,
                    "name": {
                        "fr": "Graine de pechume en gelée",
                        "en": "Jelly with Corn",
                        "es": "Gota viscosa"
                    },
                    "img": "/assets/items/1082.svg"
                }
            ]
        },
        {
            "id": 1002,
            "name": {
                "fr": "Friandises",
                "en": "Bobble Cane",
                "es": "Golosinas"
            },
            "items": [
                {
                    "id": 1002,
                    "rarity": 7,
                    "value": 100000,
                    "name": {
                        "fr": "Canne de Bobble",
                        "en": "Candy Cane",
                        "es": "Bastón de Bobble"
                    },
                    "img": "/assets/items/1002.svg"
                },
                {
                    "id": 1004,
                    "rarity": 1,
                    "value": 1000,
                    "name": {
                        "fr": "Bonbon Chamagros",
                        "en": "Raindrop Candy",
                        "es": "Caramelo Chamagros"
                    },
                    "img": "/assets/items/1004.svg"
                },
                {
                    "id": 1005,
                    "rarity": 2,
                    "value": 2500,
                    "name": {
                        "fr": "Bonbon rosamelle-praline",
                        "en": "Cherry Candy",
                        "es": "Caramelo rosa-praliné"
                    },
                    "img": "/assets/items/1005.svg"
                },
                {
                    "id": 1006,
                    "rarity": 2,
                    "value": 5000,
                    "name": {
                        "fr": "Sucette aux fruits bleus",
                        "en": "Blue Raspberry Lollipop",
                        "es": "Chupakups de frutas azules"
                    },
                    "img": "/assets/items/1006.svg"
                },
                {
                    "id": 1007,
                    "rarity": 3,
                    "value": 15000,
                    "name": {
                        "fr": "Sucette chlorophylle",
                        "en": "Green Apple Lollipop",
                        "es": "Chupakups de clorofila"
                    },
                    "img": "/assets/items/1007.svg"
                },
                {
                    "id": 1012,
                    "rarity": 1,
                    "value": 800,
                    "name": {
                        "fr": "Surprise de Cerises",
                        "en": "Surprise Cake",
                        "es": "Sorpresa de Cerezas"
                    },
                    "img": "/assets/items/1012.svg"
                },
                {
                    "id": 1014,
                    "rarity": 1,
                    "value": 1700,
                    "name": {
                        "fr": "Gelée des bois",
                        "en": "Cake",
                        "es": "Pastel de madera"
                    },
                    "img": "/assets/items/1014.svg"
                },
                {
                    "id": 1015,
                    "rarity": 3,
                    "value": 12000,
                    "name": {
                        "fr": "Suprême aux framboises",
                        "en": "Wedding Cake",
                        "es": "Tarta suprema de frambuesas"
                    },
                    "img": "/assets/items/1015.svg"
                },
                {
                    "id": 1016,
                    "rarity": 3,
                    "value": 8000,
                    "name": {
                        "fr": "Petit pétillant",
                        "en": "Piece of Cake",
                        "es": "Tarta muy rica"
                    },
                    "img": "/assets/items/1016.svg"
                },
                {
                    "id": 1018,
                    "rarity": 2,
                    "value": 1200,
                    "name": {
                        "fr": "Glace Fraise",
                        "en": "Strawberry Ice Cream",
                        "es": "Helado de fresa"
                    },
                    "img": "/assets/items/1018.svg"
                },
                {
                    "id": 1019,
                    "rarity": 2,
                    "value": 3500,
                    "name": {
                        "fr": "Coupe Glacée",
                        "en": "Ice Cream Cup",
                        "es": "Copa helada"
                    },
                    "img": "/assets/items/1019.svg"
                },
                {
                    "id": 1023,
                    "rarity": 2,
                    "value": 5000,
                    "name": {
                        "fr": "Gros acidulé",
                        "en": "Taffy",
                        "es": "Gran azucarado"
                    },
                    "img": "/assets/items/1023.svg"
                },
                {
                    "id": 1045,
                    "rarity": 2,
                    "value": 3000,
                    "name": {
                        "fr": "Pétale mystérieuse",
                        "en": "Cashew",
                        "es": "Pétalo misterioso"
                    },
                    "img": "/assets/items/1045.svg"
                },
                {
                    "id": 1046,
                    "rarity": 4,
                    "value": 35000,
                    "name": {
                        "fr": "Gump",
                        "en": "Gump",
                        "es": "Gump"
                    },
                    "img": "/assets/items/1046.svg"
                }
            ]
        },
        {
            "id": 1003,
            "name": {
                "fr": "Pierres précieuses",
                "en": "Precious stones",
                "es": "Piedras preciosas"
            },
            "items": [
                {
                    "id": 1009,
                    "rarity": 1,
                    "value": 5000,
                    "name": {
                        "fr": "Oeil de tigre",
                        "en": "Eye of the Tiger",
                        "es": "Ojo del Tigre"
                    },
                    "img": "/assets/items/1009.svg"
                },
                {
                    "id": 1010,
                    "rarity": 2,
                    "value": 15000,
                    "name": {
                        "fr": "Jade de 12kg",
                        "en": "12kg Jade",
                        "es": "Jade de 12kg"
                    },
                    "img": "/assets/items/1010.svg"
                },
                {
                    "id": 1011,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Reflet-de-lune",
                        "en": "Moon Glade",
                        "es": "Reflejo-de-luna"
                    },
                    "img": "/assets/items/1011.svg"
                }
            ]
        },
        {
            "id": 1004,
            "name": {
                "fr": "Junk food",
                "en": "Junk food",
                "es": "Comida basura"
            },
            "items": [
                {
                    "id": 1001,
                    "rarity": 1,
                    "value": 1000,
                    "name": {
                        "fr": "Pain à la viande",
                        "en": "Hamburger",
                        "es": "Pan de carne"
                    },
                    "img": "/assets/items/1001.svg"
                },
                {
                    "id": 1021,
                    "rarity": 1,
                    "value": 10,
                    "name": {
                        "fr": "Poulet Surgelé",
                        "en": "Frozen Chicken",
                        "es": "Pollo congelado"
                    },
                    "img": "/assets/items/1021.svg"
                },
                {
                    "id": 1042,
                    "rarity": 5,
                    "value": 1,
                    "name": {
                        "fr": "Délice d'Aralé",
                        "en": "Chocolate Mousse",
                        "es": "Delicia de Arale"
                    },
                    "img": "/assets/items/1042.svg"
                },
                {
                    "id": 1055,
                    "rarity": 1,
                    "value": 1200,
                    "name": {
                        "fr": "Boisson kipik",
                        "en": "Soda",
                        "es": "Bebida kipik"
                    },
                    "img": "/assets/items/1055.svg"
                },
                {
                    "id": 1056,
                    "rarity": 1,
                    "value": 900,
                    "name": {
                        "fr": "Doigts-de-tuberculoz",
                        "en": "French Fries",
                        "es": "Dedos-de-Tubérculo"
                    },
                    "img": "/assets/items/1056.svg"
                },
                {
                    "id": 1057,
                    "rarity": 1,
                    "value": 1400,
                    "name": {
                        "fr": "Pizza de Donatello",
                        "en": "Slice of Pizza",
                        "es": "Pizza de Donatello"
                    },
                    "img": "/assets/items/1057.svg"
                },
                {
                    "id": 1142,
                    "rarity": 1,
                    "value": 600,
                    "name": {
                        "fr": "Café de fin de projet",
                        "en": "Project Finisher",
                        "es": "Café de fin de proyecto"
                    },
                    "img": "/assets/items/1142.svg"
                },
                {
                    "id": 1149,
                    "rarity": 1,
                    "value": 550,
                    "name": {
                        "fr": "Noodles crus",
                        "en": "Ramen Noodles",
                        "es": "Fideos chinos deshidratados"
                    },
                    "img": "/assets/items/1149.svg"
                },
                {
                    "id": 1166,
                    "rarity": 6,
                    "value": 57000,
                    "name": {
                        "fr": "Oeuf de Poire",
                        "en": "Pear Egg",
                        "es": "Huevo de pera"
                    },
                    "img": "/assets/items/1166.svg"
                }
            ]
        },
        {
            "id": 1005,
            "name": {
                "fr": "Exotic !",
                "en": "Exotic!",
                "es": "¡Exotismo!"
            },
            "items": [
                {
                    "id": 1020,
                    "rarity": 2,
                    "value": 5600,
                    "name": {
                        "fr": "Noodles",
                        "en": "Noodles",
                        "es": "Fideos chinos"
                    },
                    "img": "/assets/items/1020.svg"
                },
                {
                    "id": 1039,
                    "rarity": 4,
                    "value": 50000,
                    "name": {
                        "fr": "Monsieur radis",
                        "en": "Mr. Radish",
                        "es": "Señor rábano"
                    },
                    "img": "/assets/items/1039.svg"
                },
                {
                    "id": 1044,
                    "rarity": 5,
                    "value": 60000,
                    "name": {
                        "fr": "Manda",
                        "en": "Manda",
                        "es": "Manda"
                    },
                    "img": "/assets/items/1044.svg"
                },
                {
                    "id": 1060,
                    "rarity": 3,
                    "value": 10000,
                    "name": {
                        "fr": "Bleuette rouge",
                        "en": "Red Plant",
                        "es": "Nadie sabe lo que es"
                    },
                    "img": "/assets/items/1060.svg"
                },
                {
                    "id": 1061,
                    "rarity": 4,
                    "value": 45000,
                    "name": {
                        "fr": "Perroquet décapité en sauce",
                        "en": "Parrot Head",
                        "es": "Loro decapitado en salsa"
                    },
                    "img": "/assets/items/1061.svg"
                },
                {
                    "id": 1062,
                    "rarity": 4,
                    "value": 70000,
                    "name": {
                        "fr": "Morvo-morphe",
                        "en": "Alien Blob",
                        "es": "Morvo-morfo"
                    },
                    "img": "/assets/items/1062.svg"
                },
                {
                    "id": 1076,
                    "rarity": 5,
                    "value": 60000,
                    "name": {
                        "fr": "Daruma-Pastèque",
                        "en": "Daruma Watermelon",
                        "es": "Daruma-Sandía"
                    },
                    "img": "/assets/items/1076.svg"
                },
                {
                    "id": 1161,
                    "rarity": 3,
                    "value": 9000,
                    "name": {
                        "fr": "Nem aux anchois",
                        "en": "Breaded Anchovies",
                        "es": "Nem de anchoas"
                    },
                    "img": "/assets/items/1161.svg"
                },
                {
                    "id": 1162,
                    "rarity": 2,
                    "value": 150,
                    "name": {
                        "fr": "Surimi pamplemousse",
                        "en": "Grapefruit Sushi",
                        "es": "Surimi de pomelo"
                    },
                    "img": "/assets/items/1162.svg"
                },
                {
                    "id": 1163,
                    "rarity": 5,
                    "value": 80000,
                    "name": {
                        "fr": "Poulpi à l'encre",
                        "en": "Octopus in Ink",
                        "es": "Calamar en su tinta"
                    },
                    "img": "/assets/items/1163.svg"
                },
                {
                    "id": 1167,
                    "rarity": 6,
                    "value": 300000,
                    "name": {
                        "fr": "Truffe collante sans goût",
                        "en": "Tasteless Sticky Truffle",
                        "es": "Trufa pegajosa sin sabor"
                    },
                    "img": "/assets/items/1167.svg"
                }
            ]
        },
        {
            "id": 1006,
            "name": {
                "fr": "Apéritifs",
                "en": "Aperitives",
                "es": "Aperitivos"
            },
            "items": [
                {
                    "id": 1029,
                    "rarity": 1,
                    "value": 300,
                    "name": {
                        "fr": "Saucisse piquée",
                        "en": "Sausage on a Stick",
                        "es": "Salchica con palillo de dientes"
                    },
                    "img": "/assets/items/1029.svg"
                },
                {
                    "id": 1030,
                    "rarity": 1,
                    "value": 400,
                    "name": {
                        "fr": "Cerise-apéro confite",
                        "en": "Cherry on a Stick",
                        "es": "Cereza con palillo de dientes"
                    },
                    "img": "/assets/items/1030.svg"
                },
                {
                    "id": 1031,
                    "rarity": 1,
                    "value": 600,
                    "name": {
                        "fr": "Fromage piqué",
                        "en": "Cheese on a Stick",
                        "es": "Queso con palillo de dientes"
                    },
                    "img": "/assets/items/1031.svg"
                },
                {
                    "id": 1032,
                    "rarity": 1,
                    "value": 1000,
                    "name": {
                        "fr": "Olive pas mûre",
                        "en": "Green Olive on a Stick",
                        "es": "Aceituna de Estepa"
                    },
                    "img": "/assets/items/1032.svg"
                },
                {
                    "id": 1033,
                    "rarity": 2,
                    "value": 1500,
                    "name": {
                        "fr": "Olive noire",
                        "en": "Black Olive on a Stick",
                        "es": "Aceituna negra de Estepa"
                    },
                    "img": "/assets/items/1033.svg"
                },
                {
                    "id": 1034,
                    "rarity": 2,
                    "value": 2000,
                    "name": {
                        "fr": "Oeil de pomme",
                        "en": "Eyeball on a Stick",
                        "es": "Ojo del cochino"
                    },
                    "img": "/assets/items/1034.svg"
                },
                {
                    "id": 1035,
                    "rarity": 2,
                    "value": 2500,
                    "name": {
                        "fr": "Blob intrusif",
                        "en": "Jello on a Stick",
                        "es": "Aperitivo de Blandiblú"
                    },
                    "img": "/assets/items/1035.svg"
                },
                {
                    "id": 1036,
                    "rarity": 3,
                    "value": 5000,
                    "name": {
                        "fr": "Gouda mou",
                        "en": "Gouda on a Stick",
                        "es": "Queso holandés blando"
                    },
                    "img": "/assets/items/1036.svg"
                },
                {
                    "id": 1037,
                    "rarity": 3,
                    "value": 10000,
                    "name": {
                        "fr": "Poulpi empalé",
                        "en": "Fish on a Stick",
                        "es": "Tapa de calamar"
                    },
                    "img": "/assets/items/1037.svg"
                },
                {
                    "id": 1038,
                    "rarity": 4,
                    "value": 50000,
                    "name": {
                        "fr": "Olive oubliée",
                        "en": "Stuffed Olive on a Stick",
                        "es": "Aceituna olvidada"
                    },
                    "img": "/assets/items/1038.svg"
                },
                {
                    "id": 1164,
                    "rarity": 5,
                    "value": 70000,
                    "name": {
                        "fr": "Curly",
                        "en": "Curly",
                        "es": "Curly"
                    },
                    "img": "/assets/items/1164.svg"
                }
            ]
        },
        {
            "id": 1007,
            "name": {
                "fr": "Kapital Risk",
                "en": "Kapital Risk",
                "es": "Kapital Risk"
            },
            "items": [
                {
                    "id": 1052,
                    "rarity": 1,
                    "value": 5000,
                    "name": {
                        "fr": "Sou d'argent",
                        "en": "Silver Coin",
                        "es": "Moneda de plata"
                    },
                    "img": "/assets/items/1052.svg"
                },
                {
                    "id": 1053,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Sou d'or",
                        "en": "Gold Coin",
                        "es": "Moneda de oro"
                    },
                    "img": "/assets/items/1053.svg"
                },
                {
                    "id": 1054,
                    "rarity": 4,
                    "value": 75000,
                    "name": {
                        "fr": "Gros tas de sous",
                        "en": "Big Unemployment",
                        "es": "Un montón de dinero"
                    },
                    "img": "/assets/items/1054.svg"
                }
            ]
        },
        {
            "id": 1008,
            "name": {
                "fr": "Trophées de Grand Prédateur",
                "en": "Great Predator Trophies",
                "es": "Trofeos del Gran Depredador"
            },
            "items": [
                {
                    "id": 1091,
                    "rarity": 2,
                    "value": 12000,
                    "name": {
                        "fr": "Jambon de Bayonne",
                        "en": "Raw Ham",
                        "es": "Jamón"
                    },
                    "img": "/assets/items/1091.svg"
                },
                {
                    "id": 1092,
                    "rarity": 2,
                    "value": 6000,
                    "name": {
                        "fr": "Saucisson entamé",
                        "en": "Sliced Sausage",
                        "es": "Lomo embuchado de jabugo"
                    },
                    "img": "/assets/items/1092.svg"
                },
                {
                    "id": 1093,
                    "rarity": 1,
                    "value": 800,
                    "name": {
                        "fr": "Raide red reste",
                        "en": "Red Remains",
                        "es": "Mortadela torcida"
                    },
                    "img": "/assets/items/1093.svg"
                },
                {
                    "id": 1094,
                    "rarity": 1,
                    "value": 500,
                    "name": {
                        "fr": "Torchon madrangeais au sirop d'érable",
                        "en": "Maple Syrup Roll",
                        "es": "Bacon al sirope de fresa"
                    },
                    "img": "/assets/items/1094.svg"
                },
                {
                    "id": 1095,
                    "rarity": 2,
                    "value": 4000,
                    "name": {
                        "fr": "Saucisson de marcassin sauvage",
                        "en": "Blood Sausage",
                        "es": "Salchichón de jabato salvaje"
                    },
                    "img": "/assets/items/1095.svg"
                },
                {
                    "id": 1096,
                    "rarity": 1,
                    "value": 1200,
                    "name": {
                        "fr": "Tranches de Jaret de Kangourou",
                        "en": "Kangaroo Liver",
                        "es": "Rebanada de Canguro"
                    },
                    "img": "/assets/items/1096.svg"
                },
                {
                    "id": 1097,
                    "rarity": 1,
                    "value": 1600,
                    "name": {
                        "fr": "Saucissaille de St-Morgelet",
                        "en": "Sausage of St.Morgelet",
                        "es": "Salchichón de la Sierra de Aracena"
                    },
                    "img": "/assets/items/1097.svg"
                },
                {
                    "id": 1098,
                    "rarity": 3,
                    "value": 400,
                    "name": {
                        "fr": "Paté d'ongles au truffes",
                        "en": "Head Cheese",
                        "es": "Pavo trufado sin cabello de ángel"
                    },
                    "img": "/assets/items/1098.svg"
                },
                {
                    "id": 1099,
                    "rarity": 4,
                    "value": 16000,
                    "name": {
                        "fr": "Saucisson maudit scellé",
                        "en": "Squished Sausage",
                        "es": "Salchichón maldito"
                    },
                    "img": "/assets/items/1099.svg"
                }
            ]
        },
        {
            "id": 1009,
            "name": {
                "fr": "Délices de Harry 'le beau'",
                "en": "Good-looking Harry's delights",
                "es": "Delicias de Harry 'el guapo'"
            },
            "items": [
                {
                    "id": 1063,
                    "rarity": 1,
                    "value": 1000,
                    "name": {
                        "fr": "Fraise Tagada",
                        "en": "Tagada-Strawberry",
                        "es": "Chuchería de fresa"
                    },
                    "img": "/assets/items/1063.svg"
                },
                {
                    "id": 1064,
                    "rarity": 1,
                    "value": 1400,
                    "name": {
                        "fr": "Car-En-Sac",
                        "en": "Car-En-Sac",
                        "es": "Píldoras de mentira"
                    },
                    "img": "/assets/items/1064.svg"
                },
                {
                    "id": 1065,
                    "rarity": 1,
                    "value": 1600,
                    "name": {
                        "fr": "Dragibus",
                        "en": "Dragibus",
                        "es": "Bolitas de chuches"
                    },
                    "img": "/assets/items/1065.svg"
                },
                {
                    "id": 1066,
                    "rarity": 2,
                    "value": 12250,
                    "name": {
                        "fr": "Krokodile",
                        "en": "Crocodile",
                        "es": "Chuchería de cocodrilo"
                    },
                    "img": "/assets/items/1066.svg"
                },
                {
                    "id": 1067,
                    "rarity": 1,
                    "value": 1800,
                    "name": {
                        "fr": "Demi Cocobats",
                        "en": "Half Cocobats",
                        "es": "Chuche de infancia irlandesa"
                    },
                    "img": "/assets/items/1067.svg"
                },
                {
                    "id": 1068,
                    "rarity": 1,
                    "value": 1750,
                    "name": {
                        "fr": "Happy Cola",
                        "en": "Happy Cola",
                        "es": "Una botellita de cola"
                    },
                    "img": "/assets/items/1068.svg"
                }
            ]
        },
        {
            "id": 1010,
            "name": {
                "fr": "Chocolats",
                "en": "Chocolates",
                "es": "Bombones de chocolate"
            },
            "items": [
                {
                    "id": 1083,
                    "rarity": 3,
                    "value": 1650,
                    "name": {
                        "fr": "Sombrino aux amandes",
                        "en": "Chocolate with Almond",
                        "es": "Bombón de nueces"
                    },
                    "img": "/assets/items/1083.svg"
                },
                {
                    "id": 1084,
                    "rarity": 3,
                    "value": 1500,
                    "name": {
                        "fr": "Emi-Praline",
                        "en": "Dark Chocolate",
                        "es": "Emi-Praliné"
                    },
                    "img": "/assets/items/1084.svg"
                },
                {
                    "id": 1085,
                    "rarity": 3,
                    "value": 1600,
                    "name": {
                        "fr": "Frogmaliet aux pepites de chocolat",
                        "en": "Belgium Chocolate",
                        "es": "Bombón con pepitas de chocolate"
                    },
                    "img": "/assets/items/1085.svg"
                },
                {
                    "id": 1086,
                    "rarity": 3,
                    "value": 1800,
                    "name": {
                        "fr": "Yumi au café",
                        "en": "White Chocolate with Coffee Bean",
                        "es": "Yumi de café"
                    },
                    "img": "/assets/items/1086.svg"
                },
                {
                    "id": 1087,
                    "rarity": 3,
                    "value": 1700,
                    "name": {
                        "fr": "Bouchée mielleuse nappée au gel de Vodka",
                        "en": "Vodka Chocolate",
                        "es": "Esponjita de gel de Vodka"
                    },
                    "img": "/assets/items/1087.svg"
                },
                {
                    "id": 1088,
                    "rarity": 3,
                    "value": 1750,
                    "name": {
                        "fr": "Escargot au chocolat persillé",
                        "en": "Snail Chocolate",
                        "es": "Caracol de chocolate de pasta de queso"
                    },
                    "img": "/assets/items/1088.svg"
                },
                {
                    "id": 1089,
                    "rarity": 3,
                    "value": 1550,
                    "name": {
                        "fr": "Fossile de cacao marbré au fois gras",
                        "en": "Marble Chocolate",
                        "es": "Fósil de chocolate de márbol de foie gras"
                    },
                    "img": "/assets/items/1089.svg"
                },
                {
                    "id": 1090,
                    "rarity": 3,
                    "value": 3000,
                    "name": {
                        "fr": "Cerisot mariné a la bière",
                        "en": "Cerisot Marinated in Beer",
                        "es": "Caramelo marinado a la cerveza"
                    },
                    "img": "/assets/items/1090.svg"
                }
            ]
        },
        {
            "id": 1011,
            "name": {
                "fr": "Fromages",
                "en": "Cheese",
                "es": "Quesos"
            },
            "items": [
                {
                    "id": 1106,
                    "rarity": 1,
                    "value": 25,
                    "name": {
                        "fr": "Camembert",
                        "en": "Mozzarella Cheese",
                        "es": "Camembert"
                    },
                    "img": "/assets/items/1106.svg"
                },
                {
                    "id": 1107,
                    "rarity": 1,
                    "value": 75,
                    "name": {
                        "fr": "Emmental",
                        "en": "Emmental Cheese",
                        "es": "Emmental"
                    },
                    "img": "/assets/items/1107.svg"
                },
                {
                    "id": 1108,
                    "rarity": 2,
                    "value": 225,
                    "name": {
                        "fr": "Fromage verni",
                        "en": "Edam Cheese",
                        "es": "Queso barnizado"
                    },
                    "img": "/assets/items/1108.svg"
                },
                {
                    "id": 1109,
                    "rarity": 2,
                    "value": 250,
                    "name": {
                        "fr": "Roquefort",
                        "en": "Blue Cheese",
                        "es": "Roquefort"
                    },
                    "img": "/assets/items/1109.svg"
                },
                {
                    "id": 1110,
                    "rarity": 2,
                    "value": 400,
                    "name": {
                        "fr": "Fromage frais vigné",
                        "en": "Swiss Cheese",
                        "es": "Queso fresco"
                    },
                    "img": "/assets/items/1110.svg"
                },
                {
                    "id": 1111,
                    "rarity": 2,
                    "value": 325,
                    "name": {
                        "fr": "Pâte dessert fromagée",
                        "en": "Cheesecake",
                        "es": "Quesito"
                    },
                    "img": "/assets/items/1111.svg"
                }
            ]
        },
        {
            "id": 1012,
            "name": {
                "fr": "Conserves",
                "en": "Canned food",
                "es": "Conservas"
            },
            "items": [
                {
                    "id": 1100,
                    "rarity": 4,
                    "value": 2500,
                    "name": {
                        "fr": "Manquereau-sauce-au-citron",
                        "en": "Lemon Canned-Fish",
                        "es": "Lata de atún en aceite de oliva"
                    },
                    "img": "/assets/items/1100.svg"
                },
                {
                    "id": 1101,
                    "rarity": 2,
                    "value": 575,
                    "name": {
                        "fr": "Mini-saucisses en boite",
                        "en": "Canned Sliced Sausages",
                        "es": "Minisalchichas en conserva"
                    },
                    "img": "/assets/items/1101.svg"
                },
                {
                    "id": 1102,
                    "rarity": 2,
                    "value": 575,
                    "name": {
                        "fr": "Haricots blanc",
                        "en": "White Beans",
                        "es": "Judías blancas"
                    },
                    "img": "/assets/items/1102.svg"
                },
                {
                    "id": 1103,
                    "rarity": 2,
                    "value": 575,
                    "name": {
                        "fr": "Lychees premier prix",
                        "en": "Canned Lychees",
                        "es": "Litchis incaducables"
                    },
                    "img": "/assets/items/1103.svg"
                },
                {
                    "id": 1104,
                    "rarity": 1,
                    "value": 175,
                    "name": {
                        "fr": "Zion's Calamar",
                        "en": "Zion's Cheese",
                        "es": "Fabada asturiana en conserva"
                    },
                    "img": "/assets/items/1104.svg"
                },
                {
                    "id": 1105,
                    "rarity": 1,
                    "value": 175,
                    "name": {
                        "fr": "Aubergines au sirop",
                        "en": "Pâté",
                        "es": "Conserva de lentejas madrileñas con chorizo"
                    },
                    "img": "/assets/items/1105.svg"
                },
                {
                    "id": 1168,
                    "rarity": 3,
                    "value": 6000,
                    "name": {
                        "fr": "Sardines",
                        "en": "Sardines",
                        "es": "Sardinas en lata"
                    },
                    "img": "/assets/items/1168.svg"
                }
            ]
        },
        {
            "id": 1013,
            "name": {
                "fr": "Légumes",
                "en": "Vegetables",
                "es": "Legumbres"
            },
            "items": [
                {
                    "id": 1112,
                    "rarity": 2,
                    "value": 700,
                    "name": {
                        "fr": "Saladou",
                        "en": "Lettuce",
                        "es": "Ensalado"
                    },
                    "img": "/assets/items/1112.svg"
                },
                {
                    "id": 1113,
                    "rarity": 2,
                    "value": 800,
                    "name": {
                        "fr": "Poire d'eau",
                        "en": "Leek",
                        "es": "Puerro de agua"
                    },
                    "img": "/assets/items/1113.svg"
                },
                {
                    "id": 1114,
                    "rarity": 4,
                    "value": 3700,
                    "name": {
                        "fr": "Cacahuète mauve et juteuse",
                        "en": "Eggplant",
                        "es": "Cacahuete aceitoso de malva"
                    },
                    "img": "/assets/items/1114.svg"
                },
                {
                    "id": 1115,
                    "rarity": 2,
                    "value": 200,
                    "name": {
                        "fr": "Pommes de pierre",
                        "en": "Walnut",
                        "es": "Manzana de piedra"
                    },
                    "img": "/assets/items/1115.svg"
                },
                {
                    "id": 1116,
                    "rarity": 2,
                    "value": 400,
                    "name": {
                        "fr": "Patates douces",
                        "en": "Hazelnut",
                        "es": "Patatas dulces"
                    },
                    "img": "/assets/items/1116.svg"
                },
                {
                    "id": 1117,
                    "rarity": 3,
                    "value": 700,
                    "name": {
                        "fr": "Matraque bio",
                        "en": "Cucumber",
                        "es": "Porra bio"
                    },
                    "img": "/assets/items/1117.svg"
                },
                {
                    "id": 1118,
                    "rarity": 1,
                    "value": 500,
                    "name": {
                        "fr": "Tomate pacifique",
                        "en": "Tomato",
                        "es": "Tomate pacífico"
                    },
                    "img": "/assets/items/1118.svg"
                },
                {
                    "id": 1119,
                    "rarity": 1,
                    "value": 600,
                    "name": {
                        "fr": "Radix",
                        "en": "Radish",
                        "es": "Rabanox"
                    },
                    "img": "/assets/items/1119.svg"
                },
                {
                    "id": 1120,
                    "rarity": 3,
                    "value": 15,
                    "name": {
                        "fr": "Haricots verts",
                        "en": "Green Beans",
                        "es": "Judías verdes"
                    },
                    "img": "/assets/items/1120.svg"
                },
                {
                    "id": 1121,
                    "rarity": 3,
                    "value": 5500,
                    "name": {
                        "fr": "Pomme Sapik",
                        "en": "Onion",
                        "es": "Cebolla Sapik"
                    },
                    "img": "/assets/items/1121.svg"
                },
                {
                    "id": 1122,
                    "rarity": 4,
                    "value": 28000,
                    "name": {
                        "fr": "Pomme Sapu",
                        "en": "Garlic",
                        "es": "Ajos Sapu"
                    },
                    "img": "/assets/items/1122.svg"
                },
                {
                    "id": 1123,
                    "rarity": 5,
                    "value": 70000,
                    "name": {
                        "fr": "Echalottes",
                        "en": "Shallots",
                        "es": "Chalote"
                    },
                    "img": "/assets/items/1123.svg"
                },
                {
                    "id": 1124,
                    "rarity": 3,
                    "value": 2600,
                    "name": {
                        "fr": "Sel rieur",
                        "en": "River Salt",
                        "es": "Sal que ríe"
                    },
                    "img": "/assets/items/1124.svg"
                },
                {
                    "id": 1125,
                    "rarity": 3,
                    "value": 3500,
                    "name": {
                        "fr": "Poivron vert",
                        "en": "Green Pepper",
                        "es": "Pimiento verde"
                    },
                    "img": "/assets/items/1125.svg"
                },
                {
                    "id": 1126,
                    "rarity": 4,
                    "value": 15000,
                    "name": {
                        "fr": "Poivron jaune",
                        "en": "Yellow Pepper",
                        "es": "Pimiento amarillo"
                    },
                    "img": "/assets/items/1126.svg"
                },
                {
                    "id": 1127,
                    "rarity": 1,
                    "value": 700,
                    "name": {
                        "fr": "Poivron rouge",
                        "en": "Red Pepper",
                        "es": "Pimiento rojo"
                    },
                    "img": "/assets/items/1127.svg"
                },
                {
                    "id": 1128,
                    "rarity": 3,
                    "value": 3,
                    "name": {
                        "fr": "Brocolis digérés",
                        "en": "Digested Broccoli",
                        "es": "Brocolis digerido"
                    },
                    "img": "/assets/items/1128.svg"
                },
                {
                    "id": 1129,
                    "rarity": 3,
                    "value": 6000,
                    "name": {
                        "fr": "Grappe de Radix",
                        "en": "Bitten Radish",
                        "es": "Rácimo de Rabanox"
                    },
                    "img": "/assets/items/1129.svg"
                },
                {
                    "id": 1130,
                    "rarity": 3,
                    "value": 7000,
                    "name": {
                        "fr": "Lance-poix",
                        "en": "Snow Peas",
                        "es": "Lanza-guisantes"
                    },
                    "img": "/assets/items/1130.svg"
                },
                {
                    "id": 1131,
                    "rarity": 5,
                    "value": 35000,
                    "name": {
                        "fr": "Haricots blancs",
                        "en": "White Beans",
                        "es": "Judías blancas"
                    },
                    "img": "/assets/items/1131.svg"
                },
                {
                    "id": 1132,
                    "rarity": 4,
                    "value": 9000,
                    "name": {
                        "fr": "Poires d'eau en grappe",
                        "en": "Blended Pears",
                        "es": "Puñado de puerros de agua"
                    },
                    "img": "/assets/items/1132.svg"
                },
                {
                    "id": 1133,
                    "rarity": 3,
                    "value": 700,
                    "name": {
                        "fr": "Artifroid",
                        "en": "Artichoke",
                        "es": "Alcachofa"
                    },
                    "img": "/assets/items/1133.svg"
                },
                {
                    "id": 1134,
                    "rarity": 5,
                    "value": 90000,
                    "name": {
                        "fr": "Choux-flou",
                        "en": "Cauliflower",
                        "es": "Coliflor"
                    },
                    "img": "/assets/items/1134.svg"
                },
                {
                    "id": 1135,
                    "rarity": 3,
                    "value": 1400,
                    "name": {
                        "fr": "Chourou",
                        "en": "Purple Cabbage",
                        "es": "Lombarda"
                    },
                    "img": "/assets/items/1135.svg"
                },
                {
                    "id": 1136,
                    "rarity": 1,
                    "value": 350,
                    "name": {
                        "fr": "Pom pom pom..",
                        "en": "Corn",
                        "es": "Disparador de Maíz"
                    },
                    "img": "/assets/items/1136.svg"
                }
            ]
        },
        {
            "id": 1014,
            "name": {
                "fr": "Délices MT",
                "en": "MotionTwin Delights",
                "es": "Delicias MotionTwin"
            },
            "items": [
                {
                    "id": 1141,
                    "rarity": 6,
                    "value": 150000,
                    "name": {
                        "fr": "Brioche dorée",
                        "en": "Golden Pastry",
                        "es": "Magdalena brioche"
                    },
                    "img": "/assets/items/1141.svg"
                },
                {
                    "id": 1143,
                    "rarity": 5,
                    "value": 150000,
                    "name": {
                        "fr": "Laxatif aux amandes",
                        "en": "Almond Laxative",
                        "es": "Laxante de almendras"
                    },
                    "img": "/assets/items/1143.svg"
                }
            ]
        },
        {
            "id": 1015,
            "name": {
                "fr": "Kit de survie MT",
                "en": "MotionTwin survival kit",
                "es": "Kit de supervivencia MotionTwin"
            },
            "items": [
                {
                    "id": 1137,
                    "rarity": 2,
                    "value": 1250,
                    "name": {
                        "fr": "Hollandais",
                        "en": "Dutch Cake",
                        "es": "Sandwich tostado Holandés"
                    },
                    "img": "/assets/items/1137.svg"
                },
                {
                    "id": 1138,
                    "rarity": 2,
                    "value": 1250,
                    "name": {
                        "fr": "Fondant XXL au choco-beurre",
                        "en": "Chocolate Fudge Cake",
                        "es": "Pastel XXL de choco-mantequilla"
                    },
                    "img": "/assets/items/1138.svg"
                },
                {
                    "id": 1139,
                    "rarity": 3,
                    "value": 2500,
                    "name": {
                        "fr": "Pétillante",
                        "en": "Sparkling Soda",
                        "es": "Gaseosa"
                    },
                    "img": "/assets/items/1139.svg"
                },
                {
                    "id": 1140,
                    "rarity": 2,
                    "value": 1250,
                    "name": {
                        "fr": "Warpoquiche",
                        "en": "Baked Cracker",
                        "es": "Tarta salada de queso"
                    },
                    "img": "/assets/items/1140.svg"
                }
            ]
        },
        {
            "id": 1016,
            "name": {
                "fr": "L'heure du goûter",
                "en": "Time for tea",
                "es": "Hora de la merienda"
            },
            "items": [
                {
                    "id": 1144,
                    "rarity": 2,
                    "value": 900,
                    "name": {
                        "fr": "Smiley croquant",
                        "en": "Ghost Toast",
                        "es": "Smiley crujiente"
                    },
                    "img": "/assets/items/1144.svg"
                },
                {
                    "id": 1145,
                    "rarity": 1,
                    "value": 400,
                    "name": {
                        "fr": "Barquette de lave",
                        "en": "Flat Bread",
                        "es": "Barquillo de lava"
                    },
                    "img": "/assets/items/1145.svg"
                },
                {
                    "id": 1146,
                    "rarity": 3,
                    "value": 3000,
                    "name": {
                        "fr": "Nonoix",
                        "en": "Cracked Nut",
                        "es": "Nonuez"
                    },
                    "img": "/assets/items/1146.svg"
                },
                {
                    "id": 1147,
                    "rarity": 3,
                    "value": 3000,
                    "name": {
                        "fr": "Amande croquante",
                        "en": "Crunchy Almond",
                        "es": "Almendra crujiente"
                    },
                    "img": "/assets/items/1147.svg"
                },
                {
                    "id": 1148,
                    "rarity": 3,
                    "value": 3000,
                    "name": {
                        "fr": "Noisette",
                        "en": "Chestnut",
                        "es": "Nuez"
                    },
                    "img": "/assets/items/1148.svg"
                },
                {
                    "id": 1150,
                    "rarity": 4,
                    "value": 21000,
                    "name": {
                        "fr": "Brioche vapeur",
                        "en": "Steamed Brioche",
                        "es": "Brioche de vapor"
                    },
                    "img": "/assets/items/1150.svg"
                },
                {
                    "id": 1151,
                    "rarity": 1,
                    "value": 650,
                    "name": {
                        "fr": "Tartine chocolat-noisette",
                        "en": "Slice of Chocolate Hazelnut Bread",
                        "es": "Tostada con crema de chocolate"
                    },
                    "img": "/assets/items/1151.svg"
                },
                {
                    "id": 1152,
                    "rarity": 2,
                    "value": 1650,
                    "name": {
                        "fr": "Tartine hémoglobine",
                        "en": "Slice of Raspberry Hazelnut Bread",
                        "es": "Tostada con hemoglobina"
                    },
                    "img": "/assets/items/1152.svg"
                },
                {
                    "id": 1153,
                    "rarity": 3,
                    "value": 5800,
                    "name": {
                        "fr": "Tartine à l'orange collante",
                        "en": "Slice of Sticky Orange Bread",
                        "es": "Tostada de aceite"
                    },
                    "img": "/assets/items/1153.svg"
                },
                {
                    "id": 1154,
                    "rarity": 2,
                    "value": 1650,
                    "name": {
                        "fr": "Tartine au miel",
                        "en": "Slice of Hard Honey Bread",
                        "es": "Tostada de mermelada pegajosa"
                    },
                    "img": "/assets/items/1154.svg"
                },
                {
                    "id": 1155,
                    "rarity": 4,
                    "value": 15,
                    "name": {
                        "fr": "Lombric nature",
                        "en": "Earthworm",
                        "es": "Lombriz al natural"
                    },
                    "img": "/assets/items/1155.svg"
                },
                {
                    "id": 1159,
                    "rarity": 6,
                    "value": 250000,
                    "name": {
                        "fr": "Blob périmé",
                        "en": "Shiny Mushroom",
                        "es": "Champiñón podrido"
                    },
                    "img": "/assets/items/1159.svg"
                },
                {
                    "id": 1160,
                    "rarity": 3,
                    "value": 200,
                    "name": {
                        "fr": "Bonbon Hélène-fraiche",
                        "en": "Peppermint Candy",
                        "es": "Caramelo de reyes"
                    },
                    "img": "/assets/items/1160.svg"
                }
            ]
        },
        {
            "id": 1017,
            "name": {
                "fr": "Garçon patissier",
                "en": "The cake Boy",
                "es": "El Chico pastelero"
            },
            "items": [
                {
                    "id": 1058,
                    "rarity": 3,
                    "value": 15500,
                    "name": {
                        "fr": "Canelé du sud-ouest",
                        "en": "Hazelnut Chocolate",
                        "es": "Canelé de Burdeos"
                    },
                    "img": "/assets/items/1058.svg"
                },
                {
                    "id": 1059,
                    "rarity": 3,
                    "value": 18500,
                    "name": {
                        "fr": "Eclair noisette choco caramel et sucre",
                        "en": "Chocolate Eclair",
                        "es": "Rebanada de pan con crema de cacao y avellana"
                    },
                    "img": "/assets/items/1059.svg"
                },
                {
                    "id": 1072,
                    "rarity": 3,
                    "value": 8000,
                    "name": {
                        "fr": "Arbuche de noël",
                        "en": "Christmas Log",
                        "es": "Tronco de Navidad"
                    },
                    "img": "/assets/items/1072.svg"
                },
                {
                    "id": 1156,
                    "rarity": 2,
                    "value": 5000,
                    "name": {
                        "fr": "Grenade de chantilly",
                        "en": "Cream Grenade",
                        "es": "Granada de crema"
                    },
                    "img": "/assets/items/1156.svg"
                },
                {
                    "id": 1157,
                    "rarity": 4,
                    "value": 35000,
                    "name": {
                        "fr": "Profies très drôles",
                        "en": "Chocolate-Dipped Cream Puffs",
                        "es": "Profiteroles apetitosos"
                    },
                    "img": "/assets/items/1157.svg"
                },
                {
                    "id": 1158,
                    "rarity": 4,
                    "value": 35000,
                    "name": {
                        "fr": "Chouchocos",
                        "en": "Chocolate Delight",
                        "es": "Megaprofiterol"
                    },
                    "img": "/assets/items/1158.svg"
                },
                {
                    "id": 1165,
                    "rarity": 1,
                    "value": 2500,
                    "name": {
                        "fr": "Tartelette framboise",
                        "en": "Strawberry Tart",
                        "es": "Tarta de frambuesa"
                    },
                    "img": "/assets/items/1165.svg"
                }
            ]
        },
        {
            "id": 1018,
            "name": {
                "fr": "Les lois de la robotique",
                "en": "Laws of Robotics",
                "es": "Las leyes de la robótica"
            },
            "items": [
                {
                    "id": 1170,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Transformer en sucre",
                        "en": "Sugar Transformer",
                        "es": "Transformer de azúcar"
                    },
                    "img": "/assets/items/1170.svg"
                },
                {
                    "id": 1171,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Kitchissime",
                        "en": "Evil Robot",
                        "es": "Kitchisime"
                    },
                    "img": "/assets/items/1171.svg"
                },
                {
                    "id": 1172,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Igorocop",
                        "en": "RoboCop",
                        "es": "Igorocop"
                    },
                    "img": "/assets/items/1172.svg"
                },
                {
                    "id": 1173,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Tidouli didi",
                        "en": "R2D2",
                        "es": "Arto deedo"
                    },
                    "img": "/assets/items/1173.svg"
                },
                {
                    "id": 1174,
                    "rarity": 3,
                    "value": 35000,
                    "name": {
                        "fr": "Dalek ! Exterminate !",
                        "en": "Dalek ! Exterminate !",
                        "es": "¡Dalek, el Exterminador!"
                    },
                    "img": "/assets/items/1174.svg"
                },
                {
                    "id": 1175,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Robo-malin",
                        "en": "Robo-Malin",
                        "es": "Robo-loco"
                    },
                    "img": "/assets/items/1175.svg"
                },
                {
                    "id": 1176,
                    "rarity": 3,
                    "value": 25000,
                    "name": {
                        "fr": "Johnny 6",
                        "en": "Johnny 6",
                        "es": "Johnny 6"
                    },
                    "img": "/assets/items/1176.svg"
                },
                {
                    "id": 1177,
                    "rarity": 4,
                    "value": 50000,
                    "name": {
                        "fr": "Biscuit Transformer",
                        "en": "Biscuit Transformer",
                        "es": "Galleta de Transformer"
                    },
                    "img": "/assets/items/1177.svg"
                }
            ]
        },
        {
            "id": 1019,
            "name": {
                "fr": "Trophées de baroudeur",
                "en": "Fighter's trophies",
                "es": "Trofeos del peleón"
            },
            "items": [
                {
                    "id": 1178,
                    "rarity": 0,
                    "value": 1000,
                    "name": {
                        "fr": "Statue: Citron Sorbex",
                        "en": "Statue: Sorbex Lemon",
                        "es": "Estatua: Sorbetex de Limón"
                    },
                    "img": "/assets/items/1178.svg"
                },
                {
                    "id": 1179,
                    "rarity": 0,
                    "value": 4000,
                    "name": {
                        "fr": "Statue: Bombino",
                        "en": "Statue: Bombino",
                        "es": "Estatua: Bombino"
                    },
                    "img": "/assets/items/1179.svg"
                },
                {
                    "id": 1180,
                    "rarity": 0,
                    "value": 8000,
                    "name": {
                        "fr": "Statue: Poire Melbombe",
                        "en": "Statue: Baddybomb Pear",
                        "es": "Estatua: Pera Malabomba"
                    },
                    "img": "/assets/items/1180.svg"
                },
                {
                    "id": 1181,
                    "rarity": 0,
                    "value": 10000,
                    "name": {
                        "fr": "Statue: Tagada",
                        "en": "Statue: Tagada-Strawberry",
                        "es": "Estatua: Tagada"
                    },
                    "img": "/assets/items/1181.svg"
                },
                {
                    "id": 1182,
                    "rarity": 0,
                    "value": 15000,
                    "name": {
                        "fr": "Statue: Sapeur-kiwi",
                        "en": "Statue: Kiwi Sapper",
                        "es": "Estatua: Kiwi-Zapador"
                    },
                    "img": "/assets/items/1182.svg"
                },
                {
                    "id": 1183,
                    "rarity": 0,
                    "value": 20000,
                    "name": {
                        "fr": "Statue: Bondissante",
                        "en": "Statue: Leaper",
                        "es": "Estatua: Sandina"
                    },
                    "img": "/assets/items/1183.svg"
                },
                {
                    "id": 1184,
                    "rarity": 0,
                    "value": 25000,
                    "name": {
                        "fr": "Statue: Ananargeddon",
                        "en": "Statue: Armaggedon-Pineapple",
                        "es": "Estatua: Piñaguedón"
                    },
                    "img": "/assets/items/1184.svg"
                }
            ]
        },
        {
            "id": 1020,
            "name": {
                "fr": "Clés des Glaces",
                "en": "Ice-cream Keys",
                "es": "Llaves de Helados"
            },
            "items": [
                {
                    "id": 1190,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Passe-partout en bois",
                        "en": "Wooden Master Key",
                        "es": "Llave Maestra de Madera"
                    },
                    "img": "/assets/items/1190.svg"
                },
                {
                    "id": 1191,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Clé de Rigor Dangerous",
                        "en": "Rigor Dangerous Key",
                        "es": "Llave de Rigor Dangerous"
                    },
                    "img": "/assets/items/1191.svg"
                },
                {
                    "id": 1194,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Furtok Glaciale",
                        "en": "Frozen Key",
                        "es": "Furtok Glacial"
                    },
                    "img": "/assets/items/1194.svg"
                },
                {
                    "id": 1197,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Clé des Mondes Ardus",
                        "en": "Key of Difficult Worlds ",
                        "es": "Llave de los Mundos Arduos"
                    },
                    "img": "/assets/items/1197.svg"
                },
                {
                    "id": 1198,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Clé piquante",
                        "en": "Pungent Key",
                        "es": "Llave Que Rasca"
                    },
                    "img": "/assets/items/1198.svg"
                },
                {
                    "id": 1199,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Passe-partout de Tuberculoz",
                        "en": "Tuber's Key",
                        "es": "Llave Maestra de Tubérculo"
                    },
                    "img": "/assets/items/1199.svg"
                },
                {
                    "id": 1200,
                    "rarity": 6,
                    "value": 10000,
                    "name": {
                        "fr": "Clé des cauchemars",
                        "en": "Nightmare Key",
                        "es": "Llave de las Pesadillas"
                    },
                    "img": "/assets/items/1200.svg"
                }
            ]
        },
        {
            "id": 1021,
            "name": {
                "fr": "Clé avinée",
                "en": "Wine Key",
                "es": "Llave Borracha"
            },
            "items": [
                {
                    "id": 1193,
                    "rarity": 1,
                    "value": 10000,
                    "name": {
                        "fr": "Clé du Bourru",
                        "en": "Wine Key",
                        "es": "Llave del Bruto"
                    },
                    "img": "/assets/items/1193.svg"
                }
            ]
        },
        {
            "id": 1022,
            "name": {
                "fr": "Paperasse",
                "en": "Paperwork",
                "es": "Papeleo"
            },
            "items": [
                {
                    "id": 1196,
                    "rarity": 1,
                    "value": 10000,
                    "name": {
                        "fr": "Autorisation du Bois-Joli",
                        "en": "Authorization of Bois-Joli",
                        "es": "Autorización Madera-Bonita"
                    },
                    "img": "/assets/items/1196.svg"
                }
            ]
        },
        {
            "id": 1023,
            "name": {
                "fr": "Clés perdues",
                "en": "Lost Keys",
                "es": "Llaves Perdidas"
            },
            "items": [
                {
                    "id": 1192,
                    "rarity": 5,
                    "value": 10000,
                    "name": {
                        "fr": "Méluzzine",
                        "en": "Méluzzine Key",
                        "es": "Meluzina"
                    },
                    "img": "/assets/items/1192.svg"
                },
                {
                    "id": 1195,
                    "rarity": 4,
                    "value": 10000,
                    "name": {
                        "fr": "Vieille clé rouillée",
                        "en": "Old Rusty Key",
                        "es": "Vieja Llave Oxidada"
                    },
                    "img": "/assets/items/1195.svg"
                }
            ]
        },
        {
            "id": 1024,
            "name": {
                "fr": "Batons de joie",
                "en": "Glad sticks",
                "es": "Bastones de alegría"
            },
            "items": [
                {
                    "id": 1201,
                    "rarity": 6,
                    "value": 17000,
                    "name": {
                        "fr": "Pad Sounie",
                        "en": "Sony Controller",
                        "es": "Mando Suni"
                    },
                    "img": "/assets/items/1201.svg"
                },
                {
                    "id": 1202,
                    "rarity": 6,
                    "value": 100000,
                    "name": {
                        "fr": "Pad Frusion 64",
                        "en": "Nintendo 64 Controller",
                        "es": "Mando Nientiendo 64"
                    },
                    "img": "/assets/items/1202.svg"
                },
                {
                    "id": 1203,
                    "rarity": 6,
                    "value": 17000,
                    "name": {
                        "fr": "Pad Game-Pyramid",
                        "en": "Game-Cube Controller",
                        "es": "Mando Game-Pirámide"
                    },
                    "img": "/assets/items/1203.svg"
                },
                {
                    "id": 1204,
                    "rarity": 6,
                    "value": 17000,
                    "name": {
                        "fr": "Pad Sey-Ga",
                        "en": "Genesis Controller",
                        "es": "Mando Sey-Ga"
                    },
                    "img": "/assets/items/1204.svg"
                },
                {
                    "id": 1205,
                    "rarity": 6,
                    "value": 17000,
                    "name": {
                        "fr": "Pad Super Frusion",
                        "en": "SNES Controller",
                        "es": "Mando Super Nientiendo"
                    },
                    "img": "/assets/items/1205.svg"
                },
                {
                    "id": 1206,
                    "rarity": 6,
                    "value": 17000,
                    "name": {
                        "fr": "Pad du Système Maitre",
                        "en": "NES Controller",
                        "es": "Mando del Sistema Maestro"
                    },
                    "img": "/assets/items/1206.svg"
                },
                {
                    "id": 1207,
                    "rarity": 6,
                    "value": 17000,
                    "name": {
                        "fr": "Pad Frusion Entertainment System",
                        "en": "Master System Controller",
                        "es": "Mando Nientiendo Entertainment System"
                    },
                    "img": "/assets/items/1207.svg"
                },
                {
                    "id": 1208,
                    "rarity": 6,
                    "value": 20000,
                    "name": {
                        "fr": "Manette S-Téhéf",
                        "en": "Joystick",
                        "es": "Mando S-Tehef"
                    },
                    "img": "/assets/items/1208.svg"
                }
            ]
        },
        {
            "id": 1025,
            "name": {
                "fr": "Boissons rigolotes",
                "en": "Funny drinks",
                "es": "Bebidas divertidas"
            },
            "items": [
                {
                    "id": 1209,
                    "rarity": 1,
                    "value": 500,
                    "name": {
                        "fr": "Canette Express",
                        "en": "Can Beer",
                        "es": "Lata Express"
                    },
                    "img": "/assets/items/1209.svg"
                },
                {
                    "id": 1210,
                    "rarity": 2,
                    "value": 1200,
                    "name": {
                        "fr": "Bouteille aux 2064 bulles",
                        "en": "Sealed Beer",
                        "es": "Cerveza de las fiestas en Bordeaux"
                    },
                    "img": "/assets/items/1210.svg"
                },
                {
                    "id": 1211,
                    "rarity": 3,
                    "value": 3000,
                    "name": {
                        "fr": "Mousse volante",
                        "en": "Draft Beer",
                        "es": "Espuma voladora"
                    },
                    "img": "/assets/items/1211.svg"
                },
                {
                    "id": 1212,
                    "rarity": 4,
                    "value": 20000,
                    "name": {
                        "fr": "Vin Merveilleux",
                        "en": "Wonderful Wine",
                        "es": "Vino burgués del Gran Teatro"
                    },
                    "img": "/assets/items/1212.svg"
                },
                {
                    "id": 1213,
                    "rarity": 5,
                    "value": 100000,
                    "name": {
                        "fr": "Liqueur maléfique",
                        "en": "Evil Liquor",
                        "es": "Licor maléfico"
                    },
                    "img": "/assets/items/1213.svg"
                }
            ]
        },
        {
            "id": 1026,
            "name": {
                "fr": "Matériel administratif d'El Papah",
                "en": "Daddy's administratrive material",
                "es": "Material administrativo del Papá"
            },
            "items": [
                {
                    "id": 1214,
                    "rarity": 1,
                    "value": 2000,
                    "name": {
                        "fr": "Tampon MT",
                        "en": "MT Stamp",
                        "es": "Sello MotionTwin"
                    },
                    "img": "/assets/items/1214.svg"
                },
                {
                    "id": 1215,
                    "rarity": 2,
                    "value": 10,
                    "name": {
                        "fr": "Facture gratuite",
                        "en": "Free Bill",
                        "es": "Factura gratis"
                    },
                    "img": "/assets/items/1215.svg"
                },
                {
                    "id": 1216,
                    "rarity": 3,
                    "value": 3000,
                    "name": {
                        "fr": "Post-It de François",
                        "en": "Sticky Notes",
                        "es": "Post-It de François, no entiendo tu letra"
                    },
                    "img": "/assets/items/1216.svg"
                },
                {
                    "id": 1217,
                    "rarity": 4,
                    "value": 5500,
                    "name": {
                        "fr": "Pot à crayon solitaire",
                        "en": "Pencil Container",
                        "es": "Lapicero solitario sin pegatina de perro"
                    },
                    "img": "/assets/items/1217.svg"
                },
                {
                    "id": 1218,
                    "rarity": 5,
                    "value": 100000,
                    "name": {
                        "fr": "Agrafeuse du Chaos",
                        "en": "Chaos Stapler",
                        "es": "Grapadora del Caos"
                    },
                    "img": "/assets/items/1218.svg"
                }
            ]
        },
        {
            "id": 1027,
            "name": {
                "fr": "Artefacts mythiques",
                "en": "Mythical artifacts",
                "es": "Artefactos míticos"
            },
            "items": [
                {
                    "id": 1219,
                    "rarity": 0,
                    "value": 69,
                    "name": {
                        "fr": "Miroir bancal",
                        "en": "Mirror of Bancal",
                        "es": "Espejo de la cábala"
                    },
                    "img": "/assets/items/1219.svg"
                },
                {
                    "id": 1220,
                    "rarity": 0,
                    "value": 666,
                    "name": {
                        "fr": "Etoile du Diable",
                        "en": "Star of the Devil",
                        "es": "Estrella demoníaca"
                    },
                    "img": "/assets/items/1220.svg"
                },
                {
                    "id": 1225,
                    "rarity": 0,
                    "value": 69,
                    "name": {
                        "fr": "Miroir des Sables",
                        "en": "Mirror of Sables",
                        "es": "Espejo de las Arenas"
                    },
                    "img": "/assets/items/1225.svg"
                },
                {
                    "id": 1226,
                    "rarity": 0,
                    "value": 666,
                    "name": {
                        "fr": "Etoile des Diables Jumeaux",
                        "en": "Star of the Devil's Twins",
                        "es": "Estrella de los Diablos Gemelos"
                    },
                    "img": "/assets/items/1226.svg"
                },
                {
                    "id": 1227,
                    "rarity": 0,
                    "value": 100,
                    "name": {
                        "fr": "Sceau d'amitié",
                        "en": "Mixed Heart",
                        "es": "Sello de amistad"
                    },
                    "img": "/assets/items/1227.svg"
                },
                {
                    "id": 1237,
                    "rarity": 0,
                    "value": 150000,
                    "name": {
                        "fr": "Neige-o-glycérine",
                        "en": "Snow Bomb",
                        "es": "Nieve a la glicerina"
                    },
                    "img": "/assets/items/1237.svg"
                },
                {
                    "id": 1238,
                    "rarity": 0,
                    "value": 30000,
                    "name": {
                        "fr": "Pass-Pyramide",
                        "en": "Pyramid Pass",
                        "es": "Pase VIP Pirámide"
                    },
                    "img": "/assets/items/1238.svg"
                }
            ]
        },
        {
            "id": 1028,
            "name": {
                "fr": "Sandy en kit",
                "en": "Sandy kit",
                "es": "Sandy en kit"
            },
            "items": [
                {
                    "id": 1221,
                    "rarity": 1,
                    "value": 700,
                    "name": {
                        "fr": "Poudre de plage magique",
                        "en": "Shovel",
                        "es": "Polvo de playa mágica"
                    },
                    "img": "/assets/items/1221.svg"
                },
                {
                    "id": 1222,
                    "rarity": 1,
                    "value": 600,
                    "name": {
                        "fr": "Matériel d'architecte",
                        "en": "Bucket of Sand",
                        "es": "Material de arquitecto"
                    },
                    "img": "/assets/items/1222.svg"
                },
                {
                    "id": 1223,
                    "rarity": 2,
                    "value": 1200,
                    "name": {
                        "fr": "Maquette en sable",
                        "en": "Sand Castle",
                        "es": "Maqueta de arena"
                    },
                    "img": "/assets/items/1223.svg"
                },
                {
                    "id": 1224,
                    "rarity": 2,
                    "value": 900,
                    "name": {
                        "fr": "Winkel",
                        "en": "Winkel O'Riely",
                        "es": "Wink"
                    },
                    "img": "/assets/items/1224.svg"
                }
            ]
        },
        {
            "id": 1029,
            "name": {
                "fr": "Récompenses du Ninjutsu",
                "en": "Ninjutsu awards",
                "es": "Recompensas del Ninjutsu"
            },
            "items": [
                {
                    "id": 1228,
                    "rarity": 0,
                    "value": 900,
                    "name": {
                        "fr": "Insigne de l'ordre des Ninjas",
                        "en": "Symbol of the Ninjas",
                        "es": "Insignia de la orden de los Ninjas"
                    },
                    "img": "/assets/items/1228.svg"
                },
                {
                    "id": 1236,
                    "rarity": 0,
                    "value": 10000,
                    "name": {
                        "fr": "Insigne du Mérite",
                        "en": "Award of Merit",
                        "es": "Insignia del Mérito"
                    },
                    "img": "/assets/items/1236.svg"
                }
            ]
        },
        {
            "id": 1030,
            "name": {
                "fr": "Artefacts du Ninjutsu",
                "en": "Ninjutsu artefact",
                "es": "Artefactos del Ninjutsu"
            },
            "items": [
                {
                    "id": 1229,
                    "rarity": 2,
                    "value": 1200,
                    "name": {
                        "fr": "Couteau suisse japonais",
                        "en": "Magic Katana",
                        "es": "Cuchillo suizo japonés"
                    },
                    "img": "/assets/items/1229.svg"
                },
                {
                    "id": 1230,
                    "rarity": 3,
                    "value": 1600,
                    "name": {
                        "fr": "Shuriken de second rang",
                        "en": "Fūma Shuriken",
                        "es": "Shuriken de segundo rango"
                    },
                    "img": "/assets/items/1230.svg"
                },
                {
                    "id": 1231,
                    "rarity": 1,
                    "value": 250,
                    "name": {
                        "fr": "Shuriken d'entraînement",
                        "en": "Shuriken",
                        "es": "Shuriken de entrenamiento"
                    },
                    "img": "/assets/items/1231.svg"
                },
                {
                    "id": 1232,
                    "rarity": 2,
                    "value": 600,
                    "name": {
                        "fr": "Najinata",
                        "en": "Kunai",
                        "es": "Najinata"
                    },
                    "img": "/assets/items/1232.svg"
                },
                {
                    "id": 1233,
                    "rarity": 4,
                    "value": 5000,
                    "name": {
                        "fr": "Lance-boulettes de Précision",
                        "en": "Blowgun of Precision",
                        "es": "Lanza-bolitas de Precisión"
                    },
                    "img": "/assets/items/1233.svg"
                },
                {
                    "id": 1234,
                    "rarity": 4,
                    "value": 6500,
                    "name": {
                        "fr": "Ocarina chantant",
                        "en": "Singing Ocarina",
                        "es": "Ocarina cantante"
                    },
                    "img": "/assets/items/1234.svg"
                },
                {
                    "id": 1235,
                    "rarity": 4,
                    "value": 10000,
                    "name": {
                        "fr": "Armure de la nuit",
                        "en": "Armor of the Night",
                        "es": "Casco samurai"
                    },
                    "img": "/assets/items/1235.svg"
                }
            ]
        }
    ];

    public async findAll() {
        return this.families;
    }
    
}