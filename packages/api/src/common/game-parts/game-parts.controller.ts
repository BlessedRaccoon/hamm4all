import { Body, Controller, Get, HttpException, Param, Post, Put, Req, UseGuards } from "@nestjs/common";
import { GamePartsService } from "./game-parts.service";
import { H4A_API, GamePartParam, GamePartsAddBody, GamePart, GameParam } from "@hamm4all/shared";
import { AuthGuard } from "../../shared/auth/auth.guard";
import { Request } from "express";


@Controller()
export class GamePartsController {

    constructor(
        private readonly gamePartsService: GamePartsService
    ) {}


    @Get(H4A_API.v1.gameParts.list.getFullRawUri())
    public async list(
        @Param() params: GameParam
    ): Promise<GamePart[]> {
        const gameParts = await this.gamePartsService.findAll(params.gameID)
        const gamePartsList = gameParts.map((gamePart) => this.gamePartsService.toGamePart(gamePart))
        return Promise.all(gamePartsList)
    }

    @Get(H4A_API.v1.gameParts.get.getFullRawUri())
    public async get(
        @Param() params: GamePartParam
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        const gamePart = await this.gamePartsService.findOne(gamePartID)

        if (!gamePart) {
            throw new HttpException(`Impossible de trouver la partie de jeu avec l'ID "${gamePartID}"`, 404)
        }

        return this.gamePartsService.toGamePart(gamePart)
    }

    @Post(H4A_API.v1.gameParts.add.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.gameParts.add.getRole()))
    public async add(
        @Param() params: GameParam,
        @Body() body: GamePartsAddBody,
        @Req() request: Request
    ): Promise<GamePart> {
        const gamePart = await this.gamePartsService.create(body, request.user.id, params.gameID)

        return this.gamePartsService.toGamePart(gamePart)
    }

    @Put(H4A_API.v1.gameParts.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.gameParts.update.getRole()))
    public async update(
        @Param() params: GamePartParam,
        @Body() body: GamePartsAddBody,
        @Req() request: Request
    ): Promise<void> {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        await this.gamePartsService.update(gamePartID, body, request.user.id)
    }

}