import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { GamePartEntity } from "../../database/entities/game/game-part.entity";
import { GamePartsController } from "./game-parts.controller";
import { GamePartsService } from "./game-parts.service";
import { ThumbnailSequenceEntity } from "../../database/entities/thumbnails/thumbnail-sequence.entity";


@Module({
    "imports": [TypeOrmModule.forFeature([GamePartEntity, AuthTokenEntity, ThumbnailSequenceEntity])],
    "controllers": [GamePartsController],
    "providers": [GamePartsService],
    "exports": [GamePartsService],
})
export class GamePartsModule {}