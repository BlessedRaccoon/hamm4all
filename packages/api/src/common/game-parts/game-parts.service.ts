import { HttpException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { GamePartEntity } from "../../database/entities/game/game-part.entity";
import { FindOptionsRelations, Repository } from "typeorm";
import { GamePart, GamePartsAddBody, GamePartsUpdateBody } from "@hamm4all/shared";
import { APILogger } from "../../shared/logger/api-logger.service";
import { ThumbnailSequenceEntity } from "../../database/entities/thumbnails/thumbnail-sequence.entity";

@Injectable()
export class GamePartsService {
    private readonly logger = new APILogger(GamePartsService.name)

    constructor(
        @InjectRepository(GamePartEntity) private readonly gamePartRepository: Repository<GamePartEntity>,
        @InjectRepository(ThumbnailSequenceEntity) private readonly thumbnailSequenceRepository: Repository<ThumbnailSequenceEntity>
    ) {}

    private getClassicQueryRelationOptions(): FindOptionsRelations<GamePartEntity> {
        return {
            "game": true,
            "updated_by": true
        }
    }

    public async findAll(gameId?: number): Promise<GamePartEntity[]> {
        return this.gamePartRepository.find({
            "relations": this.getClassicQueryRelationOptions(),
            "where": {
                "game_id": gameId
            },
            "order": {
                "id": "ASC"
            }
        })
    }

    public async findOne(id: number): Promise<GamePartEntity | null> {
        return this.gamePartRepository.findOne({
            "relations": this.getClassicQueryRelationOptions(),
            "where": {
                "id": id
            }
        })
    }

    async getGamePartID(gameID: number, partIndex: number): Promise<number> {
        const gamePartsID = await this.gamePartRepository.find({
            "select": {
                "id": true
            },
            "where": {
                "game_id": gameID,
            },
            "order": {
                "id": "ASC"
            }
        })

        if ((!gamePartsID) || gamePartsID.length === 0 || !gamePartsID[partIndex - 1]) {
            throw new HttpException(`Impossible de trouver la partie de jeu avec l'index "${partIndex}"`, 404)
        }

        return gamePartsID[partIndex - 1].id
    }

    public async create(gamePart: GamePartsAddBody, userID: number, gameID: number): Promise<GamePartEntity> {
        const gamePartToInsert = new GamePartEntity()
        gamePartToInsert.name = gamePart.name
        gamePartToInsert.description = gamePart.description
        gamePartToInsert.game_id = gameID
        gamePartToInsert.updated_by_id = userID
        const savedGamePart = await this.gamePartRepository.save(gamePartToInsert)
            .then(gamePart => gamePart)
            .catch((err) => {
                this.logger.error(err)
                throw new HttpException(`Impossible de créer la partie de jeu. Vérifie qu'aucune partie de jeu avec le même nom n'existe déjà`, 500)
            })

        const insertedFullGamePart = await this.findOne(savedGamePart.id)

        if (!insertedFullGamePart) {
            throw new HttpException("Impossible de récupérer la partie de jeu créée", 500)
        }

        return insertedFullGamePart
    }

    public async update(gamePartID: number, newGamePart: GamePartsUpdateBody, userID: number): Promise<void> {
        const gamePartToUpdate = await this.findOne(gamePartID)

        if (!gamePartToUpdate) {
            throw new HttpException(`Impossible de trouver la partie de jeu avec l'ID "${gamePartID}"`, 404)
        }

        gamePartToUpdate.name = newGamePart.name
        gamePartToUpdate.description = newGamePart.description
        gamePartToUpdate.updated_by_id = userID
        await this.gamePartRepository.save(gamePartToUpdate)
            .catch((err) => {
                this.logger.error(err)
                throw new HttpException(`Impossible de mettre à jour la partie de jeu. Vérifie qu'aucune partie de jeu avec le même nom n'existe déjà`, 500)
            })
    }

    async countLevels(gamePartID: number): Promise<number> {
        const allLinksOfGamePart = await this.thumbnailSequenceRepository.find({
            "where": {
                "game_part_id": gamePartID
            },
            "relations": {
                "links": true
            },
        })

        return allLinksOfGamePart
            .flatMap((sequence) => {
                if (sequence.is_dimension) {
                    return sequence.links.slice(1)
                }
                return sequence.links
            })
            .length
            
    }

    public async toGamePart(gamePart: GamePartEntity): Promise<GamePart> {
        return {
            "id": gamePart.id,
            "name": gamePart.name,
            "description": gamePart.description,
            "game": {
                "id": gamePart.game.id,
                "name": gamePart.game.name,
                "thumbnail_id": gamePart.game.thumbnail_id,
            },
            "updated_at": gamePart.updated_at.toString(),
            "created_at": gamePart.created_at.toString(),
            "updated_by": {
                "id": gamePart.updated_by.id,
                "name": gamePart.updated_by.name
            },
            "level_count": await this.countLevels(gamePart.id)
        }
    }

}