import { GameGradeAddBody, H4A_API, GamesAddBody, GameParam, GamesUpdateBody, GAMES_THUMBNAIL_FILE_FIELD_NAME } from "@hamm4all/shared";
import { Body, Controller, FileTypeValidator, Get, HttpException, MaxFileSizeValidator, Param, ParseFilePipe, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from "@nestjs/common";
import { GamesService } from "./games.service";
import { LevelsService } from "../levels/levels.service";
import { AuthGuard } from "../../shared/auth/auth.guard";
import { FileInterceptor } from "@nestjs/platform-express";
import { Request } from "express";


@Controller()
export class GamesController {

    constructor(
        private readonly gamesService: GamesService,
        private readonly levelsService: LevelsService,
    ) {}

    @Get(H4A_API.v1.games.list.getFullRawUri())
    async list() {
        const games = await this.gamesService.findAllGames()
        const gamesPromises = games.map(game => this.gamesService.toGameDto(game))
        return await Promise.all(gamesPromises)
    }

    @Post(H4A_API.v1.games.add.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.games.add.getRole()))
    async add(
        @Body() body: GamesAddBody,
        @Req() req: Request
    ) {
        await this.gamesService.createGame(body, req.user.id)
    }

    @Get(H4A_API.v1.games.get.getFullRawUri())
    async get(
        @Param() params: GameParam
    ) {
        const game = await this.gamesService.findGameById(params.gameID)
        if (!game) {
            throw new HttpException("Game not found", 404)
        }

        return await this.gamesService.toGameDto(game)
    }

    @Get(H4A_API.v1.games.levels.getFullRawUri())
    async getLevels(
        @Param() params: GameParam
    ) {
        const levels = await this.levelsService.findAll(params.gameID)

        return levels.map(level => this.levelsService.toLevel(level))
    }

    @Put(H4A_API.v1.games.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.games.update.getRole()))
    async update(
        @Param() params: GameParam,
        @Body() body: GamesUpdateBody,
        @Req() req: Request
    ) {
        await this.gamesService.updateGame(params.gameID, body, req.user.id)
    }

    @Put(H4A_API.v1.games.thumbnail.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.games.thumbnail.update.getRole()))
    @UseInterceptors(FileInterceptor(GAMES_THUMBNAIL_FILE_FIELD_NAME))
    async updateThumbnail(
        @Param() params: GameParam,
        @UploadedFile(
            new ParseFilePipe({
                "validators": [
                    new MaxFileSizeValidator({
                        "maxSize": 1024 * 1024,
                        "message": "Une miniature de jeu ne doit pas dépasser 1Mo"
                    }),
                    new FileTypeValidator({
                        "fileType": "image/png",
                    })
                ]
            })
        ) thumbnail: Express.Multer.File,
        @Req() req: Request
    ) {
        await this.gamesService.updateThumbnail(params.gameID, thumbnail, req.user.id)
    }

    @Post(H4A_API.v1.games.grades.add.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.games.grades.add.getRole()))
    async addGrade(
        @Param() params: GameParam,
        @Body() body: GameGradeAddBody,
        @Req() req: Request
    ) {
        await this.gamesService.addOrUpdateGrade(params.gameID, body.grade, req.user.id)
    }

}