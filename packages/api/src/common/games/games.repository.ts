import { Injectable } from "@nestjs/common";
import { DataSource, Repository } from "typeorm";
import { GameEntity } from "../../database/entities/game/games.entity";
import { LevelEntity } from "../../database/entities/level/level.entity";

@Injectable()
export class GamesRepository extends Repository<GameEntity> {
    constructor(
        private dataSource: DataSource
    ) {
        super(GameEntity, dataSource.createEntityManager())
    }

    async countLevels(gameID: number) {
        return await this.dataSource.getRepository(LevelEntity).count({
            "relations": {
                "land": true
            },
            "where": {
                "land": {
                    "game_id": gameID
                }
            }
        })
    }
}