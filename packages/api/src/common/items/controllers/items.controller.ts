import { Body, Controller, Delete, FileTypeValidator, Get, MaxFileSizeValidator, Param, ParseFilePipe, Post, Put, Query, Req, UploadedFile, UseGuards, UseInterceptors } from "@nestjs/common";
import { LanguagesGuard } from "../../../shared/languages/languages.guard";
import { ItemsService } from "../services/items.service";
import { H4A_API, Item, ItemsListQuery, ItemsAddBody, ItemParam, ITEMS_PICTURE_FILE_FIELD_NAME } from "@hamm4all/shared";
import { AuthGuard } from "../../../shared/auth/auth.guard";
import { FileInterceptor } from "@nestjs/platform-express";
import { ThrottlerGuard } from "@nestjs/throttler";
import { Request } from "express";

@Controller()
export class ItemsController {

    constructor(
        private readonly itemsService: ItemsService
    ) {}

    @Get(H4A_API.v1.items.list.getFullRawUri())
    @UseGuards(LanguagesGuard)
    async getAllItems(
        @Query() query: ItemsListQuery
    ): Promise<Array<Item>> {
        const games = !query.games ? [] : Array.isArray(query.games) ? query.games : [query.games];
        const items = await this.itemsService.getAllItems(games)
        return items;
    }

    @Post(H4A_API.v1.items.add.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.items.add.getRole()), ThrottlerGuard)
    async add(
        @Body() item: ItemsAddBody,
        @Req() request: Request
    ) {
        return this.itemsService.add(item, request.user.id);
    }

    @Put(H4A_API.v1.items.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.items.update.getRole()), ThrottlerGuard)
    async update(
        @Body() item: ItemsAddBody,
        @Param() params: ItemParam,
        @Req() request: Request
    ) {
        return this.itemsService.update(item, params.itemID, request.user.id);
    }

    @Put(H4A_API.v1.items.picture.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.items.picture.update.getRole()), ThrottlerGuard)
    @UseInterceptors(FileInterceptor(ITEMS_PICTURE_FILE_FIELD_NAME))
    async updatePicture(
        @Param() params: ItemParam,
        @UploadedFile(
            new ParseFilePipe({
                "validators": [
                    new MaxFileSizeValidator({
                        "maxSize": 500_000,
                        "message": "Une image d'objet ne doit pas dépasser 500ko"
                    }),
                    new FileTypeValidator({
                        "fileType": "svg",
                    })
                ]
            })
        ) picture: Express.Multer.File,
        @Req() request: Request
    ) {
        return this.itemsService.updatePicture(params.itemID, picture, request.user.id);
    }

    @Delete(H4A_API.v1.items.delete.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.items.delete.getRole()))
    async delete(
        @Param() params: ItemParam
    ) {
        return this.itemsService.delete(params.itemID);
    }


}