import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ItemsService } from "../services/items.service";
import { ItemsController } from "../controllers/items.controller";
import { ItemEntity } from "../../../database/entities/items/items.entity";
import { GameEntity } from "../../../database/entities/game/games.entity";
import { GameGradeEntity } from "../../../database/entities/game/game-grade.entity";
import { GameLandEntity } from "../../../database/entities/game/game-lands.entity";
import { LevelEntity } from "../../../database/entities/level/level.entity";
import { LevelTransitionEntity } from "../../../database/entities/level/level-transitions.entity";
import { AuthTokenEntity } from "../../../database/entities/auth-token.entity";
import { BlobModule } from "../../../shared/blobs/blobs.module";
import { ItemNameEntity } from "../../../database/entities/items/item-names.entity";
import { ExperienceModule } from "../../users/experience/experience.module";

@Module({
    "imports": [
        TypeOrmModule.forFeature([ItemEntity, GameEntity, GameGradeEntity, GameLandEntity, LevelEntity, LevelTransitionEntity, AuthTokenEntity, ItemNameEntity]),
        BlobModule, ExperienceModule
    ],
    "providers": [ItemsService],
    "controllers": [ItemsController]
})
export class ItemsModule {}