import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LandsController } from "./lands.controller";
import { LandsService } from "./lands.service";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { GameLandsRepository } from "./lands.repository";


@Module({
    "imports": [TypeOrmModule.forFeature([AuthTokenEntity])],
    "controllers": [LandsController],
    "providers": [LandsService, GameLandsRepository]
})
export class LandsModule {}