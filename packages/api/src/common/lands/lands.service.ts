import { HttpException, Injectable } from "@nestjs/common";
import { FindOptionsOrder, FindOptionsRelations } from "typeorm";
import { GameLandEntity } from "../../database/entities/game/game-lands.entity";
import { LandsAddBody, Land } from "@hamm4all/shared";
import { GameLandsRepository } from "./lands.repository";


@Injectable()
export class LandsService {

    constructor(
        private readonly gameLandRepository: GameLandsRepository,
    ) {}

    public async findAll(gameID?: number) {
        const lands = await this.gameLandRepository.find({
            relations: this.getClassicRelations(),
            where: {
                game: {
                    id: gameID
                }
            },
            order: this.getClassicOrder()
        });

        return lands
    }

    getClassicOrder(): FindOptionsOrder<GameLandEntity> {
        return {
            "levels": {
                "screenshots": {
                    "order_num": "ASC"
                }
            }
        }
    }

    toLand(land: GameLandEntity): Land {
        return {
            "id": land.id,
            "name": land.name,
            "gameID": land.game_id,
            "levels": land.levels.map(level => ({
                "id": level.id,
                "name": level.name,
                "mainScreenshotID": level.screenshots.find(screenshot => screenshot.order_num === 1)?.id
            }))
        }
    }

    private getClassicRelations(): FindOptionsRelations<GameLandEntity> {
        return {
            "game": true,
            "levels": {
                "screenshots": true
            }
        }
    }

    private async getNthLand(landID: number) {
        const lands = await this.gameLandRepository.findOne({
            relations: this.getClassicRelations(),
            where: {
                id: landID
            },
            order: this.getClassicOrder()
        });

        return lands;
    }

    public async findOne(landID: number) {
        const land = await this.getNthLand(landID);

        if (!land) {
            throw new HttpException(`La contrée avec l'ID "${landID}" n'a pas été trouvée`, 404);
        }

        return this.toLand(land);
    }

    public async create(land: LandsAddBody, gameID: number) {
        const landToRegister = this.gameLandRepository.createMinimal({
            "name": land.name,
            "description": land.description,
            "game_id": gameID
        });
        await this.gameLandRepository.save(landToRegister)
    }

}