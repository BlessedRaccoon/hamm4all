import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from "@nestjs/common";
import { LevelDescriptionsService } from "./level-descriptions.service";
import { H4A_API, LevelDescription, LevelsDescriptionAddBody, LevelsDescriptionListQuery, LevelsParam } from "@hamm4all/shared";
import { Request } from "express";
import { AuthGuard } from "../../../shared/auth/auth.guard";
import { LevelsService } from "../levels.service";
import { ThrottlerGuard } from "@nestjs/throttler";

@Controller()
export class LevelDescriptionsController {
    constructor(
        private readonly levelDescriptionsService: LevelDescriptionsService,
        private readonly levelsService: LevelsService
    ) {}

    @Get(H4A_API.v1.levels.description.list.getFullRawUri())
    async getDescriptions(
        @Param() params: LevelsParam,
        @Query() query: LevelsDescriptionListQuery
    ): Promise<LevelDescription[]> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)
        const descriptionEntities = await this.levelDescriptionsService.getDescriptions(levelID, query.limit, query.offset)
        return descriptionEntities.map(this.levelDescriptionsService.toDescription)
    }

    @Post(H4A_API.v1.levels.description.add.getFullRawUri())
    @UseGuards(ThrottlerGuard, AuthGuard(1))
    async addDescription(
        @Param() params: LevelsParam,
        @Body() body: LevelsDescriptionAddBody,
        @Req() request: Request
    ) {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelDescriptionsService.addDescription(levelID, body.content, request.user.id)
    }
}