import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { LevelDescriptionEntity } from "../../../database/entities/level/level-description.entity";
import { FindOptionsRelations, Repository } from "typeorm";
import { LevelDescription } from "@hamm4all/shared";
import { ExperienceService } from "../../users/experience/experience.service";


@Injectable()
export class LevelDescriptionsService {

    constructor(
        @InjectRepository(LevelDescriptionEntity) private readonly levelDescriptionRepository: Repository<LevelDescriptionEntity>,
        private readonly experienceService: ExperienceService
    ) {}

    getDescriptions(levelID: number, limit?: number, offset?: number) {
        return this.levelDescriptionRepository.find({
            relations: this.getClassicRelations(),
            where: {
                level_id: levelID
            },
            order: {
                "updated_at": "DESC"
            },
            take: limit,
            skip: offset
        })
    }

    private getClassicRelations(): FindOptionsRelations<LevelDescriptionEntity> {
        return {
            "updated_by": true
        }
    }

    getLatestDescription(levelID: number) {
        return this.getDescriptions(levelID, 1, 0)
    }

    async addDescription(levelID: number, content: string, updatedBy: number) {
        const description = new LevelDescriptionEntity()
        description.content = content
        description.level_id = levelID
        description.updated_by_id = updatedBy
        this.experienceService.addExperience(updatedBy, 10)
        return this.levelDescriptionRepository.save(description)
    }

    toDescription(descriptionEntity: LevelDescriptionEntity): LevelDescription {
        return {
            "id": descriptionEntity.id,
            "content": descriptionEntity.content,
            "updated_at": descriptionEntity.updated_at.toISOString(),
            "updated_by": {
                "id": descriptionEntity.updated_by_id,
                "name": descriptionEntity.updated_by.name
            }
        }
    }

}