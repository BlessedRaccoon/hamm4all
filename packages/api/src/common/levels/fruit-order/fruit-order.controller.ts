import { Body, Controller, Get, Param, Put, Req, UseGuards } from "@nestjs/common";
import { FruitOrderService } from "./fruit-order.service";
import { H4A_API, ScreenshotParam } from "@hamm4all/shared";
import { LevelScreenshotsService } from "../screenshots/level-screenshots.service";
import { AuthGuard } from "../../../shared/auth/auth.guard";
import { FruitOrderUpdateBody } from "@hamm4all/shared/dist/calls/routes/levels/screenshots/fruit-order/update/fruit-order.update.body";
import { Request } from "express";
import { ThrottlerGuard } from "@nestjs/throttler";


@Controller()
export class FruitOrderController {
    constructor(
        private readonly fruitOrderService: FruitOrderService,
        private readonly screenshotService: LevelScreenshotsService
    ) {}

    @Get(H4A_API.v1.levels.screenshots.fruitOrder.list.getFullRawUri())
    async getFruitOrder(
        @Param() {gameID, levelName, screenshotOrderNumber}: ScreenshotParam,
    ) {
        const screenshotID = await this.screenshotService.getScreenshotIDFromGameAndLevel(gameID, levelName, screenshotOrderNumber);
        return this.fruitOrderService.getFruitOrder(screenshotID);
    }

    @Put(H4A_API.v1.levels.screenshots.fruitOrder.update.getFullRawUri())
    @UseGuards(AuthGuard(1), ThrottlerGuard)
    async updateFruitOrder(
        @Param() {gameID, levelName, screenshotOrderNumber}: ScreenshotParam,
        @Body() newOrder: FruitOrderUpdateBody,
        @Req() request: Request
    ) {
        const screenshotID = await this.screenshotService.getScreenshotIDFromGameAndLevel(gameID, levelName, screenshotOrderNumber);

        return this.fruitOrderService.updateFruitOrder(screenshotID, newOrder.order, request.user.id);
    }

}
