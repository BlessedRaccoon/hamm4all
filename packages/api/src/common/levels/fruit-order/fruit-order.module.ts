import { Module } from "@nestjs/common";
import { FruitOrderController } from "./fruit-order.controller";
import { FruitOrderService } from "./fruit-order.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LevelOrderEntity } from "../../../database/entities/level/level-order.entity";
import { AuthTokenEntity } from "../../../database/entities/auth-token.entity";
import { LevelScreenshotsModule } from "../screenshots/level-screenshots.module";
import { ExperienceModule } from "../../users/experience/experience.module";

@Module({
    "controllers": [FruitOrderController],
    "providers": [FruitOrderService],
    "imports": [
        TypeOrmModule.forFeature([LevelOrderEntity, AuthTokenEntity]),
        LevelScreenshotsModule,
        ExperienceModule
    ]
})
export class FruitOrderModule {}