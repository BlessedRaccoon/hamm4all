import { Body, Controller, Get, HttpException, Param, Post, Put, Req, UseGuards } from "@nestjs/common";
import { LevelsService } from "./levels.service";
import { AuthGuard } from "../../shared/auth/auth.guard";
import { LevelsAddBody, Level, LevelsParam, LevelsUpdateBody, H4A_API } from "@hamm4all/shared";
import { Request } from "express";
import { RandomLevel } from "@hamm4all/shared/dist/calls/routes/levels/random/levels.random.types";


@Controller()
export class LevelsController {

    constructor(
        private readonly levelsService: LevelsService
    ) {}

    @Post(H4A_API.v1.levels.random.getFullRawUri())
    async random(): Promise<RandomLevel> {
        const randomLevel = await this.levelsService.getRandom();
        return randomLevel;
    }

    @Post(H4A_API.v1.levels.add.getFullRawUri())
    @UseGuards(AuthGuard(1))
    public async add(
        @Body() body: LevelsAddBody,
        @Req() request: Request
    ): Promise<Level> {
        const rawLevel = await this.levelsService.create(body, request.user.id);

        return this.levelsService.toLevel(rawLevel);
    }

    @Get(H4A_API.v1.levels.get.getFullRawUri())
    public async getFromGame(
        @Param() params: LevelsParam,
    ) {
        const rawLevel = await this.levelsService.findOneFromGame(params.gameID, params.levelName);

        if (!rawLevel) {
            throw new HttpException(`Le niveau ${params.levelName} n'a pas été trouvé dans le jeu ${params.gameID}`, 404);
        }

        return this.levelsService.toLevel(rawLevel);
    }

    @Put(H4A_API.v1.levels.update.getFullRawUri())
    @UseGuards(AuthGuard(1))
    public async update(
        @Param() params: LevelsParam,
        @Body() body: LevelsUpdateBody,
        @Req() request: Request
    ) {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        await this.levelsService.update(levelID, body, request.user.id);
    }
}