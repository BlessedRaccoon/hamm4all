import { HttpException, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { FindOptionsRelations, Repository } from "typeorm";
import { LevelEntity } from "../../database/entities/level/level.entity";
import { Level, LevelsAddBody, LevelsUpdateBody } from "@hamm4all/shared";
import { GameLandEntity } from "../../database/entities/game/game-lands.entity";
import { RandomLevel } from "@hamm4all/shared/dist/calls/routes/levels/random/levels.random.types";


@Injectable()
export class LevelsService {

    private readonly logger = new Logger(LevelsService.name);

    constructor(
        @InjectRepository(LevelEntity) private readonly levelsRepository: Repository<LevelEntity>,
        @InjectRepository(GameLandEntity) private readonly landsRepository: Repository<GameLandEntity>
    ) {}

    public async findAll(gameID?: number): Promise<LevelEntity[]> {
        return this.levelsRepository.find({
            "relations": this.getClassicQueryRelationOptions(),
            "where": {
                "land": {
                    game_id: gameID
                }
            }
        });
    }

    private getClassicQueryRelationOptions(): FindOptionsRelations<LevelEntity> {
        return {
            "land": {
                "game": true
            },
            "updated_by": true,
            "screenshots": true
        }
    }

    toLevel(level: LevelEntity): Level {
        return {
            "id": level.id,
            "name": level.name,
            "land": {
                "id": level.land.id,
                "name": level.land.name
            },
            "created_at": level.created_at.toString(),
            "updated_at": level.updated_at.toString(),
            "updated_by": {
                "id": level.updated_by.id,
                "name": level.updated_by.name
            },
            "mainScreenshotID": level.screenshots.find(screenshot => screenshot.order_num === 1)?.id
        }
    }

    public async findLevelIDFromGame(gameID: number, levelName: string): Promise<number> {
        const level = await this.findOneFromGame(gameID, levelName);

        if (!level) {
            throw new HttpException(`Le niveau ${levelName} n'existe pas dans le jeu ${gameID}`, 404);
        }

        return level.id;
    }

    public async findOne(id: number): Promise<LevelEntity | null> {
        const level = await this.levelsRepository.findOne({
            "relations": this.getClassicQueryRelationOptions(),
            "where": {
                "id": id
            }
        });

        return level
    }

    public async findOneFromGame(gameID: number, levelName: string): Promise<LevelEntity | null> {
        const level = await this.levelsRepository.findOne({
            "relations": this.getClassicQueryRelationOptions(),
            "where": {
                "name": levelName,
                "land": {
                    "game_id": gameID
                }
            }
        });

        return level
    }

    public async isLevelExisting(level: LevelsAddBody): Promise<boolean> {

        const land = await this.landsRepository.findOne({
            "select": ["game_id"],
            "where": {
                "id": level.landID
            }
        });

        if (! land) {
            throw new HttpException(`La contrée ${level.landID} n'existe pas`, 404);
        }

        const existingLevel = await this.findOneFromGame(land.game_id, level.name);

        return !!existingLevel
    }

    public async create(level: LevelsAddBody, userID: number): Promise<LevelEntity> {

        if (await this.isLevelExisting(level)) {
            throw new HttpException(`Le niveau ${level.name} existe déjà dans ce jeu`, 400);
        }

        const registeredLevel = await this.levelsRepository.save([{
            "name": level.name,
            "land_id": level.landID,
            "updated_by_id": userID
        }]);

        const addedLevel = await this.findOne(registeredLevel[0].id);

        if (!addedLevel) {
            throw new HttpException("Une erreur s'est produite lors de l'insertion du niveau en base de données", 500);
        }

        return addedLevel;
    }

    public async update(id: number, level: LevelsUpdateBody, userID: number): Promise<void> {
        const existingLevel = await this.levelsRepository.findOneBy({id});

        if (!existingLevel) {
            throw new HttpException(`Le niveau ${id} n'existe pas`, 404);
        }

        if (existingLevel.name !== level.name) {
            const existingLevelWithNewName = await this.isLevelExisting({
                "name": level.name,
                "landID": existingLevel.land_id
            });

            if (existingLevelWithNewName) {
                throw new HttpException(`Impossible de renommer le niveau, car le niveau ${level.name} existe déjà dans ce jeu`, 400);
            }
        }

        existingLevel.name = level.name;
        existingLevel.land_id = level.landID;
        existingLevel.updated_by_id = userID;

        await this.levelsRepository.save(existingLevel);
    }

    public async getRandom(): Promise<RandomLevel> {
        const levels = await this.findAll();

        if (levels.length === 0) {
            throw new HttpException("Aucun niveau n'a été trouvé", 404);
        }

        const randomIndex = Math.floor(Math.random() * levels.length);
        const chosenLevel = levels[randomIndex];

        const land = await this.landsRepository.findOne({
            "where": {
                "id": chosenLevel.land_id
            }
        });

        if (!land) {
            throw new HttpException(`La contrée ${chosenLevel.land_id} n'existe pas`, 404);
        }

        return {
            "level": {
                "id": chosenLevel.id,
                "name": chosenLevel.name
            },
            "gameID": land.game_id
        }
    }


}