import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthTokenEntity } from "../../../database/entities/auth-token.entity";
import { LevelScreenshotEntity } from "../../../database/entities/level/level-screenshot.entity";
import { BlobModule } from "../../../shared/blobs/blobs.module";
import { LevelScreenshotsController } from "./level-screenshots.controller";
import { LevelScreenshotsService } from "./level-screenshots.service";
import { LevelsModule } from "../levels.module";
import { ExperienceModule } from "../../users/experience/experience.module";
import { LevelOrderEntity } from "../../../database/entities/level/level-order.entity";

@Module({
    "imports": [
        TypeOrmModule.forFeature([AuthTokenEntity, LevelScreenshotEntity, LevelOrderEntity]),
        BlobModule, LevelsModule, ExperienceModule
    ],
    "controllers": [LevelScreenshotsController],
    "providers": [LevelScreenshotsService],
    "exports": [LevelScreenshotsService]
})
export class LevelScreenshotsModule {}