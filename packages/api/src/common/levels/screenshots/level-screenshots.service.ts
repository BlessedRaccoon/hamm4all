import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { LevelScreenshotEntity } from "../../../database/entities/level/level-screenshot.entity";
import { DataSource, FindOptionsRelations, Repository } from "typeorm";
import { LevelScreenshot, LevelsScreenshotOrderUpdateBody } from "@hamm4all/shared";
import { BlobService } from "../../../shared/blobs/blobs.service";
import { LevelsService } from "../levels.service";
import { APILogger } from "../../../shared/logger/api-logger.service";
import { ExperienceService } from "../../users/experience/experience.service";
import { LevelOrderEntity } from "../../../database/entities/level/level-order.entity";

@Injectable()
export class LevelScreenshotsService {
    private readonly logger = new APILogger("LevelScreenshotsService")

    constructor(
        @InjectRepository(LevelScreenshotEntity) private readonly levelScreenshotRepository: Repository<LevelScreenshotEntity>,
        @InjectRepository(LevelOrderEntity) private readonly fruitOrderRepository: Repository<LevelOrderEntity>,
        private readonly blobService: BlobService,
        private readonly levelService: LevelsService,
        private readonly experienceService: ExperienceService,
        private readonly dataSource: DataSource
    ) {}

    async getScreenshotIDFromGameAndLevel(gameID: number, levelName: string, screenshotOrderNumber: number): Promise<string> {
        const levelID = await this.levelService.findLevelIDFromGame(gameID, levelName);
        const screenshot = await this.getNthScreenshot(levelID, screenshotOrderNumber)
        return screenshot.id;
    }


    toLevelScreenshot(screenshot: LevelScreenshotEntity): LevelScreenshot {
        return {
            "id": screenshot.id,
            "created_at": screenshot.created_at.toISOString(),
            "created_by": {
                "id": screenshot.created_by.id,
                "name": screenshot.created_by.name
            }
        }
    }

    getClassicRelations(): FindOptionsRelations<LevelScreenshotEntity> {
        return {
            "created_by": true
        }
    }

    async getScreenshots(levelID: number): Promise<LevelScreenshotEntity[]> {
        const screenshots = await this.levelScreenshotRepository.find({
            "where": {
                "level_id": levelID
            },
            "relations": this.getClassicRelations(),
            "order": {
                "order_num": "ASC"
            }
        });

        return screenshots;
    }

    async getNthScreenshot(levelID: number, screenshotOrderNumber: number): Promise<LevelScreenshotEntity> {
        const screenshot = await this.levelScreenshotRepository.findOne({
            "where": {
                "level_id": levelID,
                "order_num": screenshotOrderNumber
            }
        });

        if (!screenshot) {
            throw new Error(`Screenshot ${screenshotOrderNumber} not found for level ${levelID}`);
        }

        return screenshot;
    }

    async getScreenshot(levelID: number, screenshotOrderNumber: number): Promise<LevelScreenshot> {
        const screenshot = await this.getNthScreenshot(levelID, screenshotOrderNumber);
        return this.toLevelScreenshot(screenshot);
    }

    async addScreenshot(levelID: number, screenshot: Express.Multer.File, userID: number): Promise<void> {
        const savedScreenshot = await this.blobService.add({
            "data": screenshot.buffer,
            "mimetype": screenshot.mimetype,
            "size": screenshot.size,
        }, ["image/png"])

        const otherScreenshots = await this.getScreenshots(levelID);
        const orderNumToAdd = otherScreenshots.length === 0 ?
            1 :
            otherScreenshots[otherScreenshots.length - 1].order_num + 1;

        const newScreenshot = new LevelScreenshotEntity();
        newScreenshot.level_id = levelID;
        newScreenshot.id = savedScreenshot.id;
        newScreenshot.order_num = orderNumToAdd;
        newScreenshot.created_by_id = userID;
        this.logger.verbose(`Adding screenshot to level ${levelID}: ${JSON.stringify(newScreenshot)}`)

        await this.levelScreenshotRepository.save(newScreenshot);

        await this.experienceService.addExperience(userID, 15);
    }

    async updateScreenshot(levelID: number, screenshotOrderNumber: number, screenshot: Express.Multer.File, userID: number): Promise<void> {
        const savedScreenshot = await this.blobService.add({
            "data": screenshot.buffer,
            "mimetype": screenshot.mimetype,
            "size": screenshot.size,
        }, ["image/png"])

        // Get the old screenshot
        const oldScreenshot = await this.getNthScreenshot(levelID, screenshotOrderNumber);

        // Insert with a temporary order number, to avoid conflicts
        const newScreenshot = new LevelScreenshotEntity();
        newScreenshot.level_id = levelID;
        newScreenshot.id = savedScreenshot.id;
        newScreenshot.order_num = 10000 + Math.floor(Date.now() / 1000);
        newScreenshot.created_by_id = userID;
        this.logger.verbose(`Updating screenshot ${screenshotOrderNumber} of level ${levelID}: ${JSON.stringify(newScreenshot)}`)
        console.log("Inserting new screenshot")
        await this.levelScreenshotRepository.save(newScreenshot);
        console.log("New screenshot inserted")

        // Reassign orders to follow the new inserted screenshot
        this.fruitOrderRepository.update({
            "screenshot_id": oldScreenshot.id
        }, {
            "screenshot_id": newScreenshot.id
        });

        // Delete the old screenshot
        await this.levelScreenshotRepository.delete({
            "id": oldScreenshot.id
        });

        // Update the new screenshot order
        newScreenshot.order_num = screenshotOrderNumber;
        await this.levelScreenshotRepository.save(newScreenshot);

        // Add experience
        await this.experienceService.addExperience(userID, 15);
    }

    async updateScreenshotOrder(levelID: number, body: LevelsScreenshotOrderUpdateBody): Promise<void> {
        const screenshots = await this.levelScreenshotRepository.find({
            "where": {
                "level_id": levelID
            }
        });

        // Temporary update order index to unreachable values, to avoid conflicts
        const CONFLICT_OFFSET = 1000000;
        for (const screenshot of screenshots) {
            screenshot.order_num = screenshot.order_num + CONFLICT_OFFSET;
        }
        await this.levelScreenshotRepository.save(screenshots);

        for (const screenshot of screenshots) {
            const newIndex = body.newOrderIDs.indexOf(screenshot.id);
            screenshot.order_num = newIndex + 1;
        }

        await this.levelScreenshotRepository.save(screenshots);
    }
}