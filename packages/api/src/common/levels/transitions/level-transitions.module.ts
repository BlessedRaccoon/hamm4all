import { Module } from "@nestjs/common";
import { LevelTransitionsController } from "./level-transitions.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthTokenEntity } from "../../../database/entities/auth-token.entity";
import { LevelTransitionEntity, LevelTransitionLabyEntity } from "../../../database/entities/level/level-transitions.entity";
import { LevelTransitionsService } from "./level-transitions.service";
import { LevelEntity } from "../../../database/entities/level/level.entity";
import { LevelsModule } from "../levels.module";


@Module({
    "controllers": [LevelTransitionsController],
    "imports": [
        TypeOrmModule.forFeature([LevelTransitionEntity, AuthTokenEntity, LevelTransitionLabyEntity, LevelEntity]),
        LevelsModule
    ],
    "providers": [LevelTransitionsService],
})
export class LevelTransitionsModule {}
