import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { LevelTransitionEntity, LevelTransitionLabyEntity } from "../../../database/entities/level/level-transitions.entity";
import { Repository } from "typeorm";
import { LevelEntity } from "../../../database/entities/level/level.entity";
import { LevelTransitionClassic, LevelTransitionsClassic, LevelTransitionsUpdateBody, LevelTransitionsLaby, LevelTransitionsLabyUpdateBody, LevelTransitionLabyType } from "@hamm4all/shared";


@Injectable()
export class LevelTransitionsService {

    private readonly logger = new Logger(LevelTransitionsService.name);

    constructor(
        @InjectRepository(LevelTransitionEntity) private readonly levelTransitionsRepository: Repository<LevelTransitionEntity>,
        @InjectRepository(LevelTransitionLabyEntity) private readonly levelTransitionsLabyRepository: Repository<LevelTransitionLabyEntity>,
        @InjectRepository(LevelEntity) private readonly levelsRepository: Repository<LevelEntity>
    ) {}

    async getClassicTransitions(levelID: number): Promise<LevelTransitionsClassic> {
        const level = await this.levelsRepository.findOneOrFail({
            "relations": {
                "next_levels": {
                    "source": true,
                    "target": true
                },
                "previous_levels": {
                    "source": true,
                    "target": true
                }
            },
            "where": {
                "id": levelID
            }
        })

        const previousLevels: LevelTransitionClassic[] = level.previous_levels.map(transition => ({
            "level": {
                "id": transition.source.id,
                "name": transition.source.name
            },
            "type": transition.type
        }));

        const nextLevels: LevelTransitionClassic[] = level.next_levels.map(transition => ({
            "level": {
                "id": transition.target.id,
                "name": transition.target.name
            },
            "type": transition.type
        }));

        return {
            "previous": previousLevels,
            "next": nextLevels
        };
    }

    async updateClassicTransitions(levelID: number, {transitions}: LevelTransitionsUpdateBody): Promise<void> {
        const level = await this.levelsRepository.findOneOrFail({
            "where": {
                "id": levelID
            }
        });

        level.next_levels = [];
        for (const transition of transitions) {
            const transitionEntity = new LevelTransitionEntity();
            transitionEntity.source_level_id = levelID;
            transitionEntity.target_level_id = transition.targetID;
            transitionEntity.type = transition.type;
            level.next_levels.push(transitionEntity)
        }

        await this.levelsRepository.save(level);
    }


    async getLabyTransitions(levelID: number): Promise<LevelTransitionsLaby> {
        const level = await this.levelsRepository.findOneOrFail({
            "relations": {
                "next_laby_levels": {
                    "source": true,
                    "target": true
                },
                "previous_laby_levels": {
                    "source": true,
                    "target": true
                }
            },
            "where": {
                "id": levelID
            }
        });

        function getTransition(transition: LevelTransitionLabyEntity | undefined, type: LevelTransitionLabyType) {
            if (!transition) return undefined;

            const id = type === "up" || type === "right" ? transition.target.id : transition.source.id
            const name = type === "up" || type === "right" ? transition.target.name : transition.source.name

            return {
                "level": {
                    "id": id,
                    "name": name
                },
                "type": type
            }
        }

        const upTransition = level.next_laby_levels.find(transition => transition.type === "up");
        const rightTransition = level.next_laby_levels.find(transition => transition.type === "right");
        const leftTransition = level.previous_laby_levels.find(transition => transition.type === "right");
        const downTransition = level.previous_laby_levels.find(transition => transition.type === "up");

        return {
            "up": getTransition(upTransition, "up"),
            "right": getTransition(rightTransition, "right"),
            "left": getTransition(leftTransition, "left"),
            "down": getTransition(downTransition, "down")
        }
    }

    async updateLabyTransitions(levelID: number, transitions: LevelTransitionsLabyUpdateBody): Promise<void> {
        const level = await this.levelsRepository.findOneOrFail({
            "where": {
                "id": levelID
            }
        });
    
        level.next_laby_levels = [];
        level.previous_laby_levels = [];
        await this.levelsRepository.save(level);

        const possibleLabyLinks: LevelTransitionLabyType[] = ["left", "right", "up", "down"]
        for (const type of possibleLabyLinks) {
            
            const currentTransition = transitions[type]
            if (currentTransition && !isNaN(currentTransition.targetID)) {
                if (type === "right" || type === "up") {
                    const transitionEntity = new LevelTransitionLabyEntity();
                    transitionEntity.source_level_id = levelID;
                    transitionEntity.target_level_id = currentTransition.targetID
                    transitionEntity.type = type;

                    level.next_laby_levels.push(transitionEntity);
                }
                else {
                    const transitionEntity = new LevelTransitionLabyEntity();
                    transitionEntity.source_level_id = currentTransition.targetID;
                    transitionEntity.target_level_id = levelID
                    transitionEntity.type = type === "left" ? "right" : "up";

                    level.previous_laby_levels.push(transitionEntity);
                }
            }
        }

        await this.levelsRepository.save(level);
    }

    private async clearLabyTransitions(level: LevelEntity): Promise<void> {
        await this.levelTransitionsLabyRepository.delete({ source_level_id: level.id });
        await this.levelTransitionsLabyRepository.delete({ target_level_id: level.id });
    }
}