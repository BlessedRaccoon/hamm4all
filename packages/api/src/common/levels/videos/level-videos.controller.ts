import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from "@nestjs/common";
import { LevelVideosService } from "./level-videos.service";
import { H4A_API, LevelsParam, LevelsVideo, LevelsVideoAddBody, LevelsVideoParam, LevelsVideoUpdateBody } from "@hamm4all/shared";
import { AuthGuard } from "../../../shared/auth/auth.guard";
import { Request } from "express";
import { LevelsService } from "../levels.service";
import { ThrottlerGuard } from "@nestjs/throttler";

@Controller()
export class LevelVideosController {
    constructor(
        private readonly levelVideosService: LevelVideosService,
        private readonly levelsService: LevelsService
    ) {}

    @Get(H4A_API.v1.levels.videos.list.getFullRawUri())
    async getVideos(
        @Param() params: LevelsParam
    ): Promise<LevelsVideo[]> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelVideosService.getVideos(levelID);
    }

    @Get(H4A_API.v1.levels.videos.get.getFullRawUri())
    async getVideo(
        @Param() params: LevelsVideoParam
    ): Promise<LevelsVideo> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelVideosService.getVideo(levelID, params.videoIndex);
    }

    @Post(H4A_API.v1.levels.videos.add.getFullRawUri())
    @UseGuards(AuthGuard(1), ThrottlerGuard)
    async addVideo(
        @Param() params: LevelsParam,
        @Body() body: LevelsVideoAddBody,
        @Req() req: Request
    ) {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelVideosService.addVideo(levelID, body, req.user.id);
    }

    @Put(H4A_API.v1.levels.videos.update.getFullRawUri())
    @UseGuards(AuthGuard(1), ThrottlerGuard)
    async updateVideo(
        @Param() params: LevelsVideoParam,
        @Body() body: LevelsVideoUpdateBody
    ) {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelVideosService.updateVideo(levelID, params.videoIndex, body);
    }

    @Delete(H4A_API.v1.levels.videos.delete.getFullRawUri())
    @UseGuards(AuthGuard(3))
    async deleteVideo(
        @Param() params: LevelsVideoParam
    ): Promise<void> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelVideosService.deleteVideo(levelID, params.videoIndex);
    }

}