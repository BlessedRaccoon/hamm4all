import { HttpException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { LevelVideoEntity } from "../../../database/entities/level/level-video.entity";
import { FindOptionsOrder, FindOptionsRelations, Repository } from "typeorm";
import { LevelsVideo, LevelsVideoAddBody } from "@hamm4all/shared";
import { ExperienceService } from "../../users/experience/experience.service";

@Injectable()
export class LevelVideosService {

    constructor(
        @InjectRepository(LevelVideoEntity) private readonly levelVideoRepository: Repository<LevelVideoEntity>,
        private readonly experienceService: ExperienceService
    ) { }

    toVideo(video: LevelVideoEntity): LevelsVideo {
        return {
            "id": video.id,
            "levelID": video.level_id,
            "createdAt": video.createdAt.toISOString(),
            "updatedAt": video.updatedAt.toISOString(),
            "hasOrder": video.has_order,
            "link": video.link,
            "livesLost": video.livesLost,
            "user": {
                "id": video.updated_by.id,
                "name": video.updated_by.name,
            }
        };
    }

    private getClassicRelations(): FindOptionsRelations<LevelVideoEntity> {
        return {
            "updated_by": true
        };
    }

    private getClassicOrder(): FindOptionsOrder<LevelVideoEntity> {
        return {
            "has_order": "DESC",
            "livesLost": "ASC"
        };
    }

    async addVideo(levelID: number, video: LevelsVideoAddBody, userID: number) {
        const newVideo = new LevelVideoEntity()
        newVideo.level_id = levelID;
        newVideo.has_order = video.hasOrder;
        newVideo.link = this.normalizeVideoLink(video.link);
        newVideo.livesLost = video.livesLost;
        newVideo.updatedByID = userID;

        await this.levelVideoRepository.save(newVideo);

        await this.experienceService.addExperience(userID, 25)
    }

    async updateVideo(levelID: number, videoIndex: number, video: LevelsVideoAddBody) {
        const videoToUpdate = await this.getNthVideo(levelID, videoIndex);

        videoToUpdate.has_order = video.hasOrder;
        videoToUpdate.link = this.normalizeVideoLink(video.link);
        videoToUpdate.livesLost = video.livesLost;

        await this.levelVideoRepository.save(videoToUpdate);
    }

    private normalizeVideoLink(link: string): string {
        if (link.includes("embed")) {
            return link;
        }

        const videoID = link.split("v=")[1].split("&")[0];
        return `https://www.youtube.com/embed/${videoID}`;
    }

    async deleteVideo(levelID: number, videoIndex: number): Promise<void> {
        const videoToDelete = await this.getNthVideo(levelID, videoIndex);

        await this.levelVideoRepository.remove(videoToDelete);
    }

    async getNthVideo(levelID: number, videoIndex: number): Promise<LevelVideoEntity> {
        const video = await this.levelVideoRepository.find({
            "where": {
                "level_id": levelID
            },
            "order": this.getClassicOrder(),
            "take": 1,
            "skip": videoIndex
        });

        if (! video || video.length === 0) {
            throw new HttpException("Video not found", 404)
        }

        return video[0];
    }

    async getVideos(levelID: number): Promise<LevelsVideo[]> {
        const videos = await this.levelVideoRepository.find({
            "relations": this.getClassicRelations(),
            "where": {
                "level_id": levelID
            },
            "order": this.getClassicOrder()
        });

        return videos.map(this.toVideo);
    }

    async getVideo(levelID: number, videoIndex: number): Promise<LevelsVideo> {
        const video = await this.getNthVideo(levelID, videoIndex);

        return this.toVideo(video);
    }
}