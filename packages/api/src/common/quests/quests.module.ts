import { Module } from "@nestjs/common";
import { QuestsController } from "./quests.controller";
import { QuestsService } from "./quests.service";


@Module({
    imports: [],
    controllers: [QuestsController],
    providers: [QuestsService]
})
export class QuestsModule {}