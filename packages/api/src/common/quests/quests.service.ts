import { Injectable } from "@nestjs/common";

@Injectable()
export class QuestsService {

    private readonly quests = [
        {
            "id": 0,
            "name": {
                "fr": "Les constellations",
                "en": "Constellations",
                "es": "Las constelaciones"
            },
            "desc": {
                "fr": "Igor peut maintenant poser 2 bombes au lieu d'une seule à la fois, et ce, de manière permanente !",
                "en": "From now on and forever, Igor can place 2 bombs!",
                "es": "A partir de ahora y para siempre, ¡Igor puede poner 2 bombas!"
            },
            "items": [
                {
                    "id": 40,
                    "quantity": 1,
                    "name": {
                        "fr": "Sagittaire",
                        "en": "Sagittarius",
                        "es": "Sagitario"
                    },
                    "img": "/assets/items/40.svg"
                },
                {
                    "id": 41,
                    "quantity": 1,
                    "name": {
                        "fr": "Capricorne",
                        "en": "Capricorn",
                        "es": "Capricornio"
                    },
                    "img": "/assets/items/41.svg"
                },
                {
                    "id": 42,
                    "quantity": 1,
                    "name": {
                        "fr": "Lion",
                        "en": "Leo",
                        "es": "Leo"
                    },
                    "img": "/assets/items/42.svg"
                },
                {
                    "id": 43,
                    "quantity": 1,
                    "name": {
                        "fr": "Taureau",
                        "en": "Taurus",
                        "es": "Tauro"
                    },
                    "img": "/assets/items/43.svg"
                },
                {
                    "id": 44,
                    "quantity": 1,
                    "name": {
                        "fr": "Balance",
                        "en": "Libra",
                        "es": "Libra"
                    },
                    "img": "/assets/items/44.svg"
                },
                {
                    "id": 45,
                    "quantity": 1,
                    "name": {
                        "fr": "Bélier",
                        "en": "Aries",
                        "es": "Aries"
                    },
                    "img": "/assets/items/45.svg"
                },
                {
                    "id": 46,
                    "quantity": 1,
                    "name": {
                        "fr": "Scorpion",
                        "en": "Scorpio",
                        "es": "Escorpión"
                    },
                    "img": "/assets/items/46.svg"
                },
                {
                    "id": 47,
                    "quantity": 1,
                    "name": {
                        "fr": "Cancer",
                        "en": "Cancer",
                        "es": "Cáncer"
                    },
                    "img": "/assets/items/47.svg"
                },
                {
                    "id": 48,
                    "quantity": 1,
                    "name": {
                        "fr": "Verseau",
                        "en": "Aquarius",
                        "es": "Acuario"
                    },
                    "img": "/assets/items/48.svg"
                },
                {
                    "id": 49,
                    "quantity": 1,
                    "name": {
                        "fr": "Gémeaux",
                        "en": "Gemini",
                        "es": "Géminis"
                    },
                    "img": "/assets/items/49.svg"
                },
                {
                    "id": 50,
                    "quantity": 1,
                    "name": {
                        "fr": "Poisson",
                        "en": "Pisces",
                        "es": "Piscis"
                    },
                    "img": "/assets/items/50.svg"
                },
                {
                    "id": 51,
                    "quantity": 1,
                    "name": {
                        "fr": "Vierge",
                        "en": "Virgo",
                        "es": "Virgo"
                    },
                    "img": "/assets/items/51.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 100
                    },
                    {
                        "id": 6,
                        "name": {
                            "fr": "Potions du zodiaque",
                            "en": "Potions du zodiaque",
                            "es": "Potions du zodiaque"
                        }
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 5,
                        "name": {
                            "fr": "Signes du zodiaque",
                            "en": "Signes du zodiaque",
                            "es": "Signes du zodiaque"
                        }
                    }
                ]
            }
        },
        {
            "id": 1,
            "name": {
                "fr": "Mixtures du zodiaque",
                "en": "Zodiac mixture",
                "es": "Influencias del zodíaco"
            },
            "desc": {
                "fr": "Igor sait désormais envoyer ses bombes à l'étage supérieur ! Maintenez BAS enfoncé pendant que vous tapez dans une bombe posée.",
                "en": "Igor knows how to place his bombs vertically now and he can place them on the upper platform! Press DOWN arrow key beside a placed bomb.",
                "es": "Igor ya sabe lanzar verticalmente sus bombas y colocarlas en la plataforma superior. Pulsa ABAJO junto a una bomba ya lanzada."
            },
            "items": [
                {
                    "id": 52,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Sagittaire",
                        "en": "Potion of Sagitarius",
                        "es": "Elixir de Sagitario"
                    },
                    "img": "/assets/items/52.svg"
                },
                {
                    "id": 53,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Capricorne",
                        "en": "Potion of Capricorn",
                        "es": "Elixir de Capricornio"
                    },
                    "img": "/assets/items/53.svg"
                },
                {
                    "id": 54,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Lion",
                        "en": "Potion of Leo",
                        "es": "Elixir de Leo"
                    },
                    "img": "/assets/items/54.svg"
                },
                {
                    "id": 55,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Taureau",
                        "en": "Potion of Taurus",
                        "es": "Elixir de Tauro"
                    },
                    "img": "/assets/items/55.svg"
                },
                {
                    "id": 56,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir de la Balance",
                        "en": "Potion of Libra",
                        "es": "Elixir de Libra"
                    },
                    "img": "/assets/items/56.svg"
                },
                {
                    "id": 57,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Bélier",
                        "en": "Potion of Aries",
                        "es": "Elixir de Aries"
                    },
                    "img": "/assets/items/57.svg"
                },
                {
                    "id": 58,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Scorpion",
                        "en": "Potion of Scorpio",
                        "es": "Elixir de Escorpión"
                    },
                    "img": "/assets/items/58.svg"
                },
                {
                    "id": 59,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Cancer",
                        "en": "Potion of Cancer",
                        "es": "Elixir de Cáncer"
                    },
                    "img": "/assets/items/59.svg"
                },
                {
                    "id": 60,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Verseau",
                        "en": "Potion of Aquarius",
                        "es": "Elixir de Acuario"
                    },
                    "img": "/assets/items/60.svg"
                },
                {
                    "id": 61,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir des Gémeaux",
                        "en": "Potion of Gemini",
                        "es": "Elixir de Géminis"
                    },
                    "img": "/assets/items/61.svg"
                },
                {
                    "id": 62,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir du Poisson",
                        "en": "Potion of Pisces",
                        "es": "Elixir de Acuario"
                    },
                    "img": "/assets/items/62.svg"
                },
                {
                    "id": 63,
                    "quantity": 1,
                    "name": {
                        "fr": "Elixir de la Vierge",
                        "en": "Potion of Virgo",
                        "es": "Elixir de Virgo"
                    },
                    "img": "/assets/items/63.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 101
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 6,
                        "name": {
                            "fr": "Potions du zodiaque",
                            "en": "Potions du zodiaque",
                            "es": "Potions du zodiaque"
                        }
                    }
                ]
            }
        },
        {
            "id": 2,
            "name": {
                "fr": "Premiers pas",
                "en": "First steps",
                "es": "Primeros pasos"
            },
            "desc": {
                "fr": "Igor débutera désormais la partie avec une vie supplémentaire.",
                "en": "From now on Igor will always start the game with an extra life.",
                "es": "A partir de ahora Igor empezará siempre la partida con una vida extra."
            },
            "items": [
                {
                    "id": 103,
                    "quantity": 1,
                    "name": {
                        "fr": "Vie palpitante",
                        "en": "Wonderful Life!",
                        "es": "Vida palpitante"
                    },
                    "img": "/assets/items/103.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 102
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 7,
                        "name": {
                            "fr": "Coeur 1",
                            "en": "Coeur 1",
                            "es": "Coeur 1"
                        }
                    }
                ]
            }
        },
        {
            "id": 3,
            "name": {
                "fr": "L'aventure commence",
                "en": "The adventure begins",
                "es": "La aventura comienza"
            },
            "desc": {
                "fr": "Igor débutera désormais la partie avec une autre vie supplémentaire.",
                "en": "Igor will start from now on the game with another extra life.",
                "es": "Igor empezará para siempre la partida con otra vida extra."
            },
            "items": [
                {
                    "id": 104,
                    "quantity": 1,
                    "name": {
                        "fr": "Vie aventureuse",
                        "en": "Heart of an Adventurer",
                        "es": "Vida aventurosa"
                    },
                    "img": "/assets/items/104.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 103
                    },
                    {
                        "id": 9,
                        "name": {
                            "fr": "Coeur 3",
                            "en": "Coeur 3",
                            "es": "Coeur 3"
                        }
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 8,
                        "name": {
                            "fr": "Coeur 2",
                            "en": "Coeur 2",
                            "es": "Coeur 2"
                        }
                    }
                ]
            }
        },
        {
            "id": 4,
            "name": {
                "fr": "Une destinée épique",
                "en": "An epic fate",
                "es": "Un destino épico"
            },
            "desc": {
                "fr": "Igor débutera désormais la partie avec encore une vie supplémentaire !",
                "en": "Igor will start the game with another life!",
                "es": "¡Igor empezará la partida con otra vida extra más!"
            },
            "items": [
                {
                    "id": 105,
                    "quantity": 1,
                    "name": {
                        "fr": "Vie épique",
                        "en": "Final Destiny",
                        "es": "Vida épica"
                    },
                    "img": "/assets/items/105.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 104
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 9,
                        "name": {
                            "fr": "Coeur 3",
                            "en": "Coeur 3",
                            "es": "Coeur 3"
                        }
                    }
                ]
            }
        },
        {
            "id": 5,
            "name": {
                "fr": "Persévérance",
                "en": "Perseverance",
                "es": "Perseverancia"
            },
            "desc": {
                "fr": "Igor débutera désormais la partie avec une vie supplémentaire.",
                "en": "Igor will start the game with an extra life.",
                "es": "Igor empezará la partida con una vida extra."
            },
            "items": [
                {
                    "id": 36,
                    "quantity": 5,
                    "name": {
                        "fr": "Ig'or",
                        "en": "Golden Igor",
                        "es": "Igoro"
                    },
                    "img": "/assets/items/36.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 105
                    }
                ]
            }
        },
        {
            "id": 6,
            "name": {
                "fr": "Gourmandise",
                "en": "Delicacies",
                "es": "Gourmand"
            },
            "desc": {
                "fr": "De nouveaux aliments (plus sains) apparaîtront désormais en jeu !",
                "en": "New food, healthier and nicer, will appear now in the game.",
                "es": "Nuevos alimentos, más sanos y apetitosos, aparecerán ahora en el juego."
            },
            "items": [
                {
                    "id": 1000,
                    "quantity": 5,
                    "name": {
                        "fr": "Cristaux d'Hammerfest",
                        "en": "Hammerfest Crystal",
                        "es": "Cristales de Hammerfest"
                    },
                    "img": "/assets/items/1000.svg"
                },
                {
                    "id": 1003,
                    "quantity": 1,
                    "name": {
                        "fr": "Bonbon Berlinmauve",
                        "en": "Seed Candy",
                        "es": "Caramelo Berlinmalva"
                    },
                    "img": "/assets/items/1003.svg"
                },
                {
                    "id": 1013,
                    "quantity": 1,
                    "name": {
                        "fr": "Muffin aux cailloux",
                        "en": "Muffin",
                        "es": "Magdalena de piedras"
                    },
                    "img": "/assets/items/1013.svg"
                },
                {
                    "id": 1027,
                    "quantity": 1,
                    "name": {
                        "fr": "Réglisse rouge",
                        "en": "Red Licorice",
                        "es": "Chuchería"
                    },
                    "img": "/assets/items/1027.svg"
                },
                {
                    "id": 1041,
                    "quantity": 1,
                    "name": {
                        "fr": "Sucette acidulée",
                        "en": "Lollipop",
                        "es": "Piruleta"
                    },
                    "img": "/assets/items/1041.svg"
                },
                {
                    "id": 1043,
                    "quantity": 1,
                    "name": {
                        "fr": "Sorbet au plastique",
                        "en": "Plastic Sorbet",
                        "es": "Polo de plástico"
                    },
                    "img": "/assets/items/1043.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1001,
                        "name": {
                            "fr": "Aliments classiques",
                            "en": "Aliments classiques",
                            "es": "Aliments classiques"
                        }
                    }
                ]
            }
        },
        {
            "id": 7,
            "name": {
                "fr": "Du sucre !",
                "en": "Some sugar!",
                "es": "¡Quiero algo dulce!"
            },
            "desc": {
                "fr": "Igor veut encore plus de sucre ! Des délicieuses friandises apparaîtront désormais en jeu.",
                "en": "Igor wants more sweet things! From now on sweets will appear in the game.",
                "es": "¡Igor quiere azúcar! A partir de ahora aparecerán chucherías en el juego."
            },
            "items": [
                {
                    "id": 1003,
                    "quantity": 10,
                    "name": {
                        "fr": "Bonbon Berlinmauve",
                        "en": "Seed Candy",
                        "es": "Caramelo Berlinmalva"
                    },
                    "img": "/assets/items/1003.svg"
                },
                {
                    "id": 1027,
                    "quantity": 10,
                    "name": {
                        "fr": "Réglisse rouge",
                        "en": "Red Licorice",
                        "es": "Chuchería"
                    },
                    "img": "/assets/items/1027.svg"
                },
                {
                    "id": 1041,
                    "quantity": 10,
                    "name": {
                        "fr": "Sucette acidulée",
                        "en": "Lollipop",
                        "es": "Piruleta"
                    },
                    "img": "/assets/items/1041.svg"
                },
                {
                    "id": 1070,
                    "quantity": 2,
                    "name": {
                        "fr": "P'tit fantome",
                        "en": "Small Ghost",
                        "es": "Fantasmita"
                    },
                    "img": "/assets/items/1070.svg"
                },
                {
                    "id": 1074,
                    "quantity": 2,
                    "name": {
                        "fr": "Soja Max IceCream",
                        "en": "Bread",
                        "es": "Helado de Soja Max"
                    },
                    "img": "/assets/items/1074.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1002,
                        "name": {
                            "fr": "Friandises",
                            "en": "Friandises",
                            "es": "Friandises"
                        }
                    }
                ]
            }
        },
        {
            "id": 8,
            "name": {
                "fr": "Malnutrition",
                "en": "Malnutrition",
                "es": "Malnutrición"
            },
            "desc": {
                "fr": "Le goût d'Igor pour les aliments peu équilibrés lui permettra de trouver des aliments encore plus... \"douteux\" en jeu.",
                "en": "Igor's taste for badly-balanced food will lead him to find even more food of questionable quality.",
                "es": "El gusto de Igor por los alimentos poco equilibrados le permitirá encontrar aún más alimentos de dudosa capacidad nutritiva."
            },
            "items": [
                {
                    "id": 1022,
                    "quantity": 5,
                    "name": {
                        "fr": "Liquide bizarre",
                        "en": "Bizarre Liquid",
                        "es": "Líquido raro"
                    },
                    "img": "/assets/items/1022.svg"
                },
                {
                    "id": 1024,
                    "quantity": 5,
                    "name": {
                        "fr": "Liquide étrange",
                        "en": "Strange Liquid",
                        "es": "Líquido extraño"
                    },
                    "img": "/assets/items/1024.svg"
                },
                {
                    "id": 1028,
                    "quantity": 20,
                    "name": {
                        "fr": "Oeuf Au Plat",
                        "en": "Plain Egg",
                        "es": "Huevo frito"
                    },
                    "img": "/assets/items/1028.svg"
                },
                {
                    "id": 1025,
                    "quantity": 10,
                    "name": {
                        "fr": "Oeuf cru",
                        "en": "Raw Egg",
                        "es": "Huevo crudo"
                    },
                    "img": "/assets/items/1025.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1004,
                        "name": {
                            "fr": "Junk food",
                            "en": "Junk food",
                            "es": "Junk food"
                        }
                    }
                ]
            }
        },
        {
            "id": 9,
            "name": {
                "fr": "Goût raffiné",
                "en": "Good taste",
                "es": "Gusto refinado"
            },
            "desc": {
                "fr": "Le régime bizarre d'Igor a amené de nouveaux aliments exotiques en jeu !",
                "en": "Igor's weird diet brings exotic new food to the game!",
                "es": "¡El régimen extraño de Igor trae nuevos y exóticos alimentos en el juego!"
            },
            "items": [
                {
                    "id": 1002,
                    "quantity": 1,
                    "name": {
                        "fr": "Canne de Bobble",
                        "en": "Candy Cane",
                        "es": "Bastón de Bobble"
                    },
                    "img": "/assets/items/1002.svg"
                },
                {
                    "id": 1046,
                    "quantity": 2,
                    "name": {
                        "fr": "Gump",
                        "en": "Gump",
                        "es": "Gump"
                    },
                    "img": "/assets/items/1046.svg"
                },
                {
                    "id": 1040,
                    "quantity": 10,
                    "name": {
                        "fr": "Sushi thon",
                        "en": "Fish Sushi",
                        "es": "Sushi de atún"
                    },
                    "img": "/assets/items/1040.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1005,
                        "name": {
                            "fr": "Exotic !",
                            "en": "Exotic !",
                            "es": "Exotic !"
                        }
                    }
                ]
            }
        },
        {
            "id": 10,
            "name": {
                "fr": "Avancée technologique",
                "en": "Technological advance",
                "es": "Avance tecnológico"
            },
            "desc": {
                "fr": "Pour vous aider dans votre aventure, tout un tas d'objets aux effets bizarres font leur apparition en jeu !",
                "en": "A lot of items of weird effects appear now in the game to help you in the adventure!",
                "es": "¡Un montón de objetos con efectos raros aparecen ahora en el juego para ayudarte en tu aventura!"
            },
            "items": [
                {
                    "id": 4,
                    "quantity": 1,
                    "name": {
                        "fr": "Lampe Fétvoveu",
                        "en": "Magic Lamp",
                        "es": "Lámpara Pidedeseo"
                    },
                    "img": "/assets/items/4.svg"
                },
                {
                    "id": 7,
                    "quantity": 1,
                    "name": {
                        "fr": "Basket IcePump",
                        "en": "Running Shoes",
                        "es": "Botín IcePump"
                    },
                    "img": "/assets/items/7.svg"
                },
                {
                    "id": 13,
                    "quantity": 1,
                    "name": {
                        "fr": "Cass-Tet",
                        "en": "Biker Helmet",
                        "es": "Casco"
                    },
                    "img": "/assets/items/13.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1,
                        "name": {
                            "fr": "Kit de premiers secours",
                            "en": "Kit de premiers secours",
                            "es": "Kit de premiers secours"
                        }
                    },
                    {
                        "id": 8,
                        "name": {
                            "fr": "Coeur 2",
                            "en": "Coeur 2",
                            "es": "Coeur 2"
                        }
                    },
                    {
                        "id": 13,
                        "name": {
                            "fr": "Eclairage antique",
                            "en": "Eclairage antique",
                            "es": "Eclairage antique"
                        }
                    },
                    {
                        "id": 15,
                        "name": {
                            "fr": "Flocon 1",
                            "en": "Flocon 1",
                            "es": "Flocon 1"
                        }
                    }
                ]
            }
        },
        {
            "id": 11,
            "name": {
                "fr": "Le petit guide des Champignons",
                "en": "Small guide to mushrooms",
                "es": "La pequeña guía de hongos"
            },
            "desc": {
                "fr": "Igor a découvert un étrange ouvrage, une sorte de livre de cuisine traitant des champignons hallucinogènes. Il pourra désormais en trouver lors de ses explorations.",
                "en": "Igor has discovered a rare text. It's a kind of cookbook about hallucinogenic mushrooms. Igor will be able to find them now.",
                "es": "Igor ha descubierto una obra extraña. Es una especie de libro de cocina sobre hongos alucinógenos. Ahora podrá encontrarlos en sus búsquedas."
            },
            "items": [
                {
                    "id": 106,
                    "quantity": 1,
                    "name": {
                        "fr": "Livre des champignons",
                        "en": "Book of Mushrooms",
                        "es": "Libro de los hongos"
                    },
                    "img": "/assets/items/106.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 3,
                        "name": {
                            "fr": "Champignons",
                            "en": "Champignons",
                            "es": "Champignons"
                        }
                    }
                ]
            }
        },
        {
            "id": 12,
            "name": {
                "fr": "Trouver les pièces d'or secrètes !",
                "en": "Find the secret golden coins!",
                "es": "¡Encuentra las monedas de oro secretas!"
            },
            "desc": {
                "fr": "Des richesses supplémentaires de très grande valeur apparaitront maintenant en jeu !",
                "en": "Great new richness will appear now in the game!",
                "es": "¡Nuevas riquezas de gran valor aparecerán ahora en el juego!"
            },
            "items": [
                {
                    "id": 1051,
                    "quantity": 150,
                    "name": {
                        "fr": "Pièce d'or secrète",
                        "en": "Secret Coin",
                        "es": "Moneda de oro secreta"
                    },
                    "img": "/assets/items/1051.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1007,
                        "name": {
                            "fr": "Kapital Risk",
                            "en": "Kapital Risk",
                            "es": "Kapital Risk"
                        }
                    }
                ]
            }
        },
        {
            "id": 13,
            "name": {
                "fr": "Le grimoire des Etoiles",
                "en": "The book of Magic Stars",
                "es": "El grimorio de las Estrellas"
            },
            "desc": {
                "fr": "Cet ouvrage mystérieux en dit long sur les 12 constellations du Zodiaque. Vous pourrez désormais les collectionner en jeu !",
                "en": "This mysterious in-depth book talks about the 12 Zodiac Constellations. They are now available in the game!",
                "es": "Esta obra misteriosa habla extensamente sobre las 12 constelaciones del Zodíaco. ¡Ya están disponibles en el juego!"
            },
            "items": [
                {
                    "id": 107,
                    "quantity": 1,
                    "name": {
                        "fr": "Livre des étoiles",
                        "en": "Book of Stars",
                        "es": "Libro de las estrellas"
                    },
                    "img": "/assets/items/107.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5,
                        "name": {
                            "fr": "Signes du zodiaque",
                            "en": "Signes du zodiaque",
                            "es": "Signes du zodiaque"
                        }
                    }
                ]
            }
        },
        {
            "id": 14,
            "name": {
                "fr": "Armageddon",
                "en": "Armageddon",
                "es": "Armageddon"
            },
            "desc": {
                "fr": "Des objets aux effets ravageurs vont maintenant apparaître en jeu !",
                "en": "Items with devastating powers will appear now in the game!",
                "es": "¡Objetos con efectos devastadores aparecerán ahora en el juego!"
            },
            "items": [
                {
                    "id": 1,
                    "quantity": 10,
                    "name": {
                        "fr": "Bouclidur en or",
                        "en": "Golden Shield",
                        "es": "Escudo de madera y oro"
                    },
                    "img": "/assets/items/1.svg"
                },
                {
                    "id": 5,
                    "quantity": 5,
                    "name": {
                        "fr": "Lampe Léveussonfé",
                        "en": "Black Magic Lam",
                        "es": "Lámpara Negra"
                    },
                    "img": "/assets/items/5.svg"
                },
                {
                    "id": 11,
                    "quantity": 5,
                    "name": {
                        "fr": "Parapluie rouge",
                        "en": "Red Umbrella",
                        "es": "Paraguas rojo"
                    },
                    "img": "/assets/items/11.svg"
                },
                {
                    "id": 21,
                    "quantity": 10,
                    "name": {
                        "fr": "Enceinte Bessel-Son",
                        "en": "Jukebox Igor",
                        "es": "Altavoz Bájaloya"
                    },
                    "img": "/assets/items/21.svg"
                },
                {
                    "id": 22,
                    "quantity": 5,
                    "name": {
                        "fr": "Vieille chaussure trouée",
                        "en": "Heavy Weight Boots",
                        "es": "Zapato viejo con agujeros"
                    },
                    "img": "/assets/items/22.svg"
                },
                {
                    "id": 24,
                    "quantity": 1,
                    "name": {
                        "fr": "Hippo-flocon",
                        "en": "Iceberg Shower",
                        "es": "Pedrusco de los montes"
                    },
                    "img": "/assets/items/24.svg"
                },
                {
                    "id": 25,
                    "quantity": 1,
                    "name": {
                        "fr": "Flamme froide",
                        "en": "Snow Flame",
                        "es": "Llama fría"
                    },
                    "img": "/assets/items/25.svg"
                },
                {
                    "id": 28,
                    "quantity": 3,
                    "name": {
                        "fr": "Bibelot en argent",
                        "en": "Silver Trophy",
                        "es": "Baratija de plata"
                    },
                    "img": "/assets/items/28.svg"
                },
                {
                    "id": 38,
                    "quantity": 1,
                    "name": {
                        "fr": "Totem des dinoz",
                        "en": "Dino Totem Pole",
                        "es": "Tótem de los Dinos"
                    },
                    "img": "/assets/items/38.svg"
                },
                {
                    "id": 82,
                    "quantity": 5,
                    "name": {
                        "fr": "Jugement avant-dernier",
                        "en": "Eye of Providence",
                        "es": "Juicio penúltimo"
                    },
                    "img": "/assets/items/82.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 2,
                        "name": {
                            "fr": "Destruction massive",
                            "en": "Destruction massive",
                            "es": "Destruction massive"
                        }
                    }
                ]
            }
        },
        {
            "id": 15,
            "name": {
                "fr": "Régime MotionTwin",
                "en": "MotionTwin Diet",
                "es": "Régimen MotionTwin"
            },
            "desc": {
                "fr": "A force de manger n'importe quoi, Igor a acquis la maîtrise des jeux ! Il pourra collectionner des objets rarissimes pendant son exploration et trouverdes aliments d'exception !",
                "en": "After eating every kind of food, no matter what, Igor has become a Master of the games! Now he will be able to collect rare items and exceptional food during his adventure!",
                "es": "A base de comer cualquier cosa, ¡Igor se ha vuelto un maestro de los juegos! Ahora podrá coleccionar objetos y alimentos muy raros durante su aventura."
            },
            "items": [
                {
                    "id": 1057,
                    "quantity": 11,
                    "name": {
                        "fr": "Pizza de Donatello",
                        "en": "Slice of Pizza",
                        "es": "Pizza de Donatello"
                    },
                    "img": "/assets/items/1057.svg"
                },
                {
                    "id": 1074,
                    "quantity": 11,
                    "name": {
                        "fr": "Soja Max IceCream",
                        "en": "Bread",
                        "es": "Helado de Soja Max"
                    },
                    "img": "/assets/items/1074.svg"
                },
                {
                    "id": 1137,
                    "quantity": 11,
                    "name": {
                        "fr": "Hollandais",
                        "en": "Dutch Cake",
                        "es": "Sandwich tostado Holandés"
                    },
                    "img": "/assets/items/1137.svg"
                },
                {
                    "id": 1138,
                    "quantity": 11,
                    "name": {
                        "fr": "Fondant XXL au choco-beurre",
                        "en": "Chocolate Fudge Cake",
                        "es": "Pastel XXL de choco-mantequilla"
                    },
                    "img": "/assets/items/1138.svg"
                },
                {
                    "id": 1139,
                    "quantity": 11,
                    "name": {
                        "fr": "Pétillante",
                        "en": "Sparkling Soda",
                        "es": "Gaseosa"
                    },
                    "img": "/assets/items/1139.svg"
                },
                {
                    "id": 1140,
                    "quantity": 11,
                    "name": {
                        "fr": "Warpoquiche",
                        "en": "Baked Cracker",
                        "es": "Tarta salada de queso"
                    },
                    "img": "/assets/items/1140.svg"
                },
                {
                    "id": 1149,
                    "quantity": 20,
                    "name": {
                        "fr": "Noodles crus",
                        "en": "Ramen Noodles",
                        "es": "Fideos chinos deshidratados"
                    },
                    "img": "/assets/items/1149.svg"
                },
                {
                    "id": 1045,
                    "quantity": 30,
                    "name": {
                        "fr": "Pétale mystérieuse",
                        "en": "Cashew",
                        "es": "Pétalo misterioso"
                    },
                    "img": "/assets/items/1045.svg"
                },
                {
                    "id": 1081,
                    "quantity": 11,
                    "name": {
                        "fr": "Biloo",
                        "en": "Beer",
                        "es": "Hidromiel de los Bosques"
                    },
                    "img": "/assets/items/1081.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 11,
                        "name": {
                            "fr": "Gadgets Motion-Twin",
                            "en": "Gadgets Motion-Twin",
                            "es": "Gadgets Motion-Twin"
                        }
                    },
                    {
                        "id": 1014,
                        "name": {
                            "fr": "Délices MT",
                            "en": "Délices MT",
                            "es": "Délices MT"
                        }
                    }
                ]
            }
        },
        {
            "id": 16,
            "name": {
                "fr": "Créateur de jeu en devenir",
                "en": "Creator of games",
                "es": "Creador de juegos innovadores"
            },
            "desc": {
                "fr": "Igor semble apprécier les aliments basiques, une belle carrière dans le monde du jeu vidéo pourrait s'offrir à lui ! Des aliments adaptés lui seront dorénavant proposés et quelques cartes à jouer pour se faire la main.",
                "en": "Igor appreciates basic food. He could be offered a nice career in videogames! Adapted food will be available in the game, and also a deck of cards!",
                "es": "Igor aprecia los alimentos básicos, así que puede que se le presente una bella carrera en el mundo de los videojuegos. Alimentos adaptados a él le serán a partir de ahora propuestos durante el juego, ¡así como una baraja de cartas!"
            },
            "items": [
                {
                    "id": 1142,
                    "quantity": 20,
                    "name": {
                        "fr": "Café de fin de projet",
                        "en": "Project Finisher",
                        "es": "Café de fin de proyecto"
                    },
                    "img": "/assets/items/1142.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 4,
                        "name": {
                            "fr": "Cartes à jouer",
                            "en": "Cartes à jouer",
                            "es": "Cartes à jouer"
                        }
                    },
                    {
                        "id": 1015,
                        "name": {
                            "fr": "Kit de survie MT",
                            "en": "Kit de survie MT",
                            "es": "Kit de survie MT"
                        }
                    }
                ]
            }
        },
        {
            "id": 17,
            "name": {
                "fr": "La vie est une boîte de chocolats",
                "en": "Life is like a box of chocolates...",
                "es": "La vida es como una caja de bombones..."
            },
            "desc": {
                "fr": "De délicieux chocolats sont maintenant distribués en Hammerfest !",
                "en": "Delicious chocolates will be now available in Hammerfest!",
                "es": "¡Deliciosos bombones tan sorprendentes como la vida misma son distribuidos ahora en Hammerfest!"
            },
            "items": [
                {
                    "id": 1046,
                    "quantity": 5,
                    "name": {
                        "fr": "Gump",
                        "en": "Gump",
                        "es": "Gump"
                    },
                    "img": "/assets/items/1046.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1010,
                        "name": {
                            "fr": "Chocolats",
                            "en": "Chocolats",
                            "es": "Chocolats"
                        }
                    }
                ]
            }
        },
        {
            "id": 18,
            "name": {
                "fr": "Le trésor Oune-difaïned",
                "en": "Difaïned treasure",
                "es": "El tesoro difaïned"
            },
            "desc": {
                "fr": "A force de ramasser des diamants apparus on ne sait comment, Igor a acquis la faculté de repérer des pierres précieuses rares ! Il en trouvera au cours de ses explorations.",
                "en": "After collecting so many diamonds of mysterious origin, Igor adquires the ability of finding precious stones! He'll be able now to find them throughout his quests.",
                "es": "A base de recoger tantos diamantes de misteriosa aparición, ¡Igor ha adquirido la facultad de localizar las piedras preciosas raras! Ahora podrá encontrarlas a lo largo de sus exploraciones."
            },
            "items": [
                {
                    "id": 1008,
                    "quantity": 30,
                    "name": {
                        "fr": "Diamant Oune-difaïned",
                        "en": "Difaïned Diamond",
                        "es": "Diamante difaïned"
                    },
                    "img": "/assets/items/1008.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1003,
                        "name": {
                            "fr": "Pierres précieuses",
                            "en": "Pierres précieuses",
                            "es": "Pierres précieuses"
                        }
                    }
                ]
            }
        },
        {
            "id": 19,
            "name": {
                "fr": "Super size me !",
                "en": "Super size me!",
                "es": "Super size me!"
            },
            "desc": {
                "fr": "Toujours plus loin dans la malnutrition, Igor a découvert qu'il pouvait aussi se nourrir de produits en boite.",
                "en": "Keeping far from good food, Igor discovers now that he can also eat canned food!",
                "es": "Como siempre, bien lejos de la buena alimentación, Igor descubre que puede también alimentarse de alimentos en conserva."
            },
            "items": [
                {
                    "id": 1001,
                    "quantity": 20,
                    "name": {
                        "fr": "Pain à la viande",
                        "en": "Hamburger",
                        "es": "Pan de carne"
                    },
                    "img": "/assets/items/1001.svg"
                },
                {
                    "id": 1055,
                    "quantity": 20,
                    "name": {
                        "fr": "Boisson kipik",
                        "en": "Soda",
                        "es": "Bebida kipik"
                    },
                    "img": "/assets/items/1055.svg"
                },
                {
                    "id": 1056,
                    "quantity": 20,
                    "name": {
                        "fr": "Doigts-de-tuberculoz",
                        "en": "French Fries",
                        "es": "Dedos-de-Tubérculo"
                    },
                    "img": "/assets/items/1056.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1012,
                        "name": {
                            "fr": "Conserves",
                            "en": "Conserves",
                            "es": "Conserves"
                        }
                    }
                ]
            }
        },
        {
            "id": 20,
            "name": {
                "fr": "Maître joaillier",
                "en": "Master jeweller",
                "es": "Maestro joyero"
            },
            "desc": {
                "fr": "Igor est devenu un véritable expert en pierres précieuses. Il pourra maintenant découvrir de puissants bijoux magiques au fil de ses explorations !",
                "en": "Igor has become an expert in precious stones. Now his knowledge permits him to discover precious stones which are even more powerful!",
                "es": "Igor se ha convertido en un auténtico experto en piedras preciosas. ¡Ahora sus conocimientos le permiten descubrir joyas preciosas aún más poderosas!"
            },
            "items": [
                {
                    "id": 1009,
                    "quantity": 10,
                    "name": {
                        "fr": "Oeil de tigre",
                        "en": "Eye of the Tiger",
                        "es": "Ojo del Tigre"
                    },
                    "img": "/assets/items/1009.svg"
                },
                {
                    "id": 1010,
                    "quantity": 5,
                    "name": {
                        "fr": "Jade de 12kg",
                        "en": "12kg Jade",
                        "es": "Jade de 12kg"
                    },
                    "img": "/assets/items/1010.svg"
                },
                {
                    "id": 1011,
                    "quantity": 1,
                    "name": {
                        "fr": "Reflet-de-lune",
                        "en": "Moon Glade",
                        "es": "Reflejo-de-luna"
                    },
                    "img": "/assets/items/1011.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 10,
                        "name": {
                            "fr": "Trésor des pirates",
                            "en": "Trésor des pirates",
                            "es": "Trésor des pirates"
                        }
                    },
                    {
                        "id": 12,
                        "name": {
                            "fr": "Armement norvégien expérimental",
                            "en": "Armement norvégien expérimental",
                            "es": "Armement norvégien expérimental"
                        }
                    }
                ]
            }
        },
        {
            "id": 21,
            "name": {
                "fr": "Grand prédateur",
                "en": "Great Predator",
                "es": "Gran Depredador"
            },
            "desc": {
                "fr": "Igor en a plus qu'assez de chasser des choses sucrées ! Désormais devenu un prédateur sans pitié, il pourra traquer et dévorer toute sorte de charcuteries.",
                "en": "Igor is fed up hunting sweets! As he has turned into an unconditional predator, now he will be able to devour any sort of delicacy.",
                "es": "¡Igor está bien harto cazar chucherías! Depredador incondicional, ahora podrá devorar toda clase de charcutería."
            },
            "items": [
                {
                    "id": 1001,
                    "quantity": 20,
                    "name": {
                        "fr": "Pain à la viande",
                        "en": "Hamburger",
                        "es": "Pan de carne"
                    },
                    "img": "/assets/items/1001.svg"
                },
                {
                    "id": 1021,
                    "quantity": 10,
                    "name": {
                        "fr": "Poulet Surgelé",
                        "en": "Frozen Chicken",
                        "es": "Pollo congelado"
                    },
                    "img": "/assets/items/1021.svg"
                },
                {
                    "id": 1025,
                    "quantity": 20,
                    "name": {
                        "fr": "Oeuf cru",
                        "en": "Raw Egg",
                        "es": "Huevo crudo"
                    },
                    "img": "/assets/items/1025.svg"
                },
                {
                    "id": 1061,
                    "quantity": 1,
                    "name": {
                        "fr": "Perroquet décapité en sauce",
                        "en": "Parrot Head",
                        "es": "Loro decapitado en salsa"
                    },
                    "img": "/assets/items/1061.svg"
                },
                {
                    "id": 1162,
                    "quantity": 10,
                    "name": {
                        "fr": "Surimi pamplemousse",
                        "en": "Grapefruit Sushi",
                        "es": "Surimi de pomelo"
                    },
                    "img": "/assets/items/1162.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1008,
                        "name": {
                            "fr": "Trophées de Grand Prédateur",
                            "en": "Trophées de Grand Prédateur",
                            "es": "Trophées de Grand Prédateur"
                        }
                    }
                ]
            }
        },
        {
            "id": 22,
            "name": {
                "fr": "Expert en salades et potages",
                "en": "Expert in salads and stews",
                "es": "Experto en ensaladas y potajes"
            },
            "desc": {
                "fr": "On raconte partout qu'Igor serait la réincarnation de Saladou, le maître mondialement reconnu de la salade. Fort de ce don, il pourra à l'avenir cueillir une très grande variété de légumes !",
                "en": "They say that Igor is the reencarnation of Saladou, the world famous master of salads for virtual rabbits. With this new talent, he will be able to collect a great variety of vegetables!",
                "es": "Se cuenta que Igor es la reencarnación de Ensalado, el reconocido maestro de la ensalada para conejos virtuales. ¡Con su don podrá recoger una gran variedad de verduras!"
            },
            "items": [
                {
                    "id": 1020,
                    "quantity": 15,
                    "name": {
                        "fr": "Noodles",
                        "en": "Noodles",
                        "es": "Fideos chinos"
                    },
                    "img": "/assets/items/1020.svg"
                },
                {
                    "id": 1039,
                    "quantity": 1,
                    "name": {
                        "fr": "Monsieur radis",
                        "en": "Mr. Radish",
                        "es": "Señor rábano"
                    },
                    "img": "/assets/items/1039.svg"
                },
                {
                    "id": 1060,
                    "quantity": 2,
                    "name": {
                        "fr": "Bleuette rouge",
                        "en": "Red Plant",
                        "es": "Nadie sabe lo que es"
                    },
                    "img": "/assets/items/1060.svg"
                },
                {
                    "id": 1104,
                    "quantity": 5,
                    "name": {
                        "fr": "Zion's Calamar",
                        "en": "Zion's Cheese",
                        "es": "Fabada asturiana en conserva"
                    },
                    "img": "/assets/items/1104.svg"
                },
                {
                    "id": 1103,
                    "quantity": 3,
                    "name": {
                        "fr": "Lychees premier prix",
                        "en": "Canned Lychees",
                        "es": "Litchis incaducables"
                    },
                    "img": "/assets/items/1103.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1013,
                        "name": {
                            "fr": "Légumes",
                            "en": "Légumes",
                            "es": "Légumes"
                        }
                    }
                ]
            }
        },
        {
            "id": 23,
            "name": {
                "fr": "Festin d'Hammerfest",
                "en": "Hammerfest Feast",
                "es": "Festín de Hammerfest"
            },
            "desc": {
                "fr": "Avec un repas aussi complet, Igor est fin prêt pour avoir accès aux patisseries les plus raffinées qui existent.",
                "en": "After a complete lunch, Igor will now have access to really delicious cakes.",
                "es": "Tras una comida tan completa, Igor está al fin listo para acceder a pasteles de exquisito gusto."
            },
            "items": [
                {
                    "id": 1091,
                    "quantity": 1,
                    "name": {
                        "fr": "Jambon de Bayonne",
                        "en": "Raw Ham",
                        "es": "Jamón"
                    },
                    "img": "/assets/items/1091.svg"
                },
                {
                    "id": 1112,
                    "quantity": 1,
                    "name": {
                        "fr": "Saladou",
                        "en": "Lettuce",
                        "es": "Ensalado"
                    },
                    "img": "/assets/items/1112.svg"
                },
                {
                    "id": 1126,
                    "quantity": 1,
                    "name": {
                        "fr": "Poivron jaune",
                        "en": "Yellow Pepper",
                        "es": "Pimiento amarillo"
                    },
                    "img": "/assets/items/1126.svg"
                },
                {
                    "id": 1163,
                    "quantity": 1,
                    "name": {
                        "fr": "Poulpi à l'encre",
                        "en": "Octopus in Ink",
                        "es": "Calamar en su tinta"
                    },
                    "img": "/assets/items/1163.svg"
                },
                {
                    "id": 1109,
                    "quantity": 1,
                    "name": {
                        "fr": "Roquefort",
                        "en": "Blue Cheese",
                        "es": "Roquefort"
                    },
                    "img": "/assets/items/1109.svg"
                },
                {
                    "id": 1138,
                    "quantity": 1,
                    "name": {
                        "fr": "Fondant XXL au choco-beurre",
                        "en": "Chocolate Fudge Cake",
                        "es": "Pastel XXL de choco-mantequilla"
                    },
                    "img": "/assets/items/1138.svg"
                },
                {
                    "id": 1142,
                    "quantity": 1,
                    "name": {
                        "fr": "Café de fin de projet",
                        "en": "Project Finisher",
                        "es": "Café de fin de proyecto"
                    },
                    "img": "/assets/items/1142.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1017,
                        "name": {
                            "fr": "Garçon patissier",
                            "en": "Garçon patissier",
                            "es": "Garçon patissier"
                        }
                    }
                ]
            }
        },
        {
            "id": 24,
            "name": {
                "fr": "Goûter d'anniversaire",
                "en": "Birthday party",
                "es": "Merienda de cumpleaños"
            },
            "desc": {
                "fr": "Igor a trouvé tous les éléments pour assurer à son prochain goûter d'anniversaire ! Il trouvera maintenant pleins de petits en-cas pour patienter jusque là.",
                "en": "Igor has found all the required elements for his next birthday party! From now on he will get even more until that time comes.",
                "es": "¡Igor ha encontrado todos los elementos para asegurar una buena merienda de cumpleaños! A partir de ahora podrá obtener aún más dulces para una segunda merienda."
            },
            "items": [
                {
                    "id": 68,
                    "quantity": 1,
                    "name": {
                        "fr": "Bougie",
                        "en": "Candle of Light",
                        "es": "Vela"
                    },
                    "img": "/assets/items/68.svg"
                },
                {
                    "id": 1004,
                    "quantity": 10,
                    "name": {
                        "fr": "Bonbon Chamagros",
                        "en": "Raindrop Candy",
                        "es": "Caramelo Chamagros"
                    },
                    "img": "/assets/items/1004.svg"
                },
                {
                    "id": 1005,
                    "quantity": 10,
                    "name": {
                        "fr": "Bonbon rosamelle-praline",
                        "en": "Cherry Candy",
                        "es": "Caramelo rosa-praliné"
                    },
                    "img": "/assets/items/1005.svg"
                },
                {
                    "id": 1007,
                    "quantity": 1,
                    "name": {
                        "fr": "Sucette chlorophylle",
                        "en": "Green Apple Lollipop",
                        "es": "Chupakups de clorofila"
                    },
                    "img": "/assets/items/1007.svg"
                },
                {
                    "id": 1015,
                    "quantity": 1,
                    "name": {
                        "fr": "Suprême aux framboises",
                        "en": "Wedding Cake",
                        "es": "Tarta suprema de frambuesas"
                    },
                    "img": "/assets/items/1015.svg"
                },
                {
                    "id": 1016,
                    "quantity": 3,
                    "name": {
                        "fr": "Petit pétillant",
                        "en": "Piece of Cake",
                        "es": "Tarta muy rica"
                    },
                    "img": "/assets/items/1016.svg"
                },
                {
                    "id": 1018,
                    "quantity": 5,
                    "name": {
                        "fr": "Glace Fraise",
                        "en": "Strawberry Ice Cream",
                        "es": "Helado de fresa"
                    },
                    "img": "/assets/items/1018.svg"
                },
                {
                    "id": 1019,
                    "quantity": 5,
                    "name": {
                        "fr": "Coupe Glacée",
                        "en": "Ice Cream Cup",
                        "es": "Copa helada"
                    },
                    "img": "/assets/items/1019.svg"
                },
                {
                    "id": 1023,
                    "quantity": 10,
                    "name": {
                        "fr": "Gros acidulé",
                        "en": "Taffy",
                        "es": "Gran azucarado"
                    },
                    "img": "/assets/items/1023.svg"
                },
                {
                    "id": 1070,
                    "quantity": 10,
                    "name": {
                        "fr": "P'tit fantome",
                        "en": "Small Ghost",
                        "es": "Fantasmita"
                    },
                    "img": "/assets/items/1070.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1016,
                        "name": {
                            "fr": "L'heure du goûter",
                            "en": "L'heure du goûter",
                            "es": "L'heure du goûter"
                        }
                    }
                ]
            }
        },
        {
            "id": 25,
            "name": {
                "fr": "Bon vivant",
                "en": "Bon vivant",
                "es": "Vividor"
            },
            "desc": {
                "fr": "Un bon repas ne peu se concevoir sans des petits trucs à grignotter à l'apéritif. Igor le sait, maintenant, et il pourra trouver ce qu'il faut en jeu.",
                "en": "We can't imagin a good lunch without a nice aperitive. Igor knows now what he needs.",
                "es": "Una buena comida no puede ser concebida sin tomarse antes unas tapas. Ahora Igor conoce bien lo que se necesita para un buen tapeo."
            },
            "items": [
                {
                    "id": 1069,
                    "quantity": 10,
                    "name": {
                        "fr": "Cacahuete secrète",
                        "en": "Peanut",
                        "es": "Cacahuete secreto"
                    },
                    "img": "/assets/items/1069.svg"
                },
                {
                    "id": 1081,
                    "quantity": 10,
                    "name": {
                        "fr": "Biloo",
                        "en": "Beer",
                        "es": "Hidromiel de los Bosques"
                    },
                    "img": "/assets/items/1081.svg"
                },
                {
                    "id": 1094,
                    "quantity": 10,
                    "name": {
                        "fr": "Torchon madrangeais au sirop d'érable",
                        "en": "Maple Syrup Roll",
                        "es": "Bacon al sirope de fresa"
                    },
                    "img": "/assets/items/1094.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1006,
                        "name": {
                            "fr": "Apéritifs",
                            "en": "Apéritifs",
                            "es": "Apéritifs"
                        }
                    }
                ]
            }
        },
        {
            "id": 26,
            "name": {
                "fr": "Fondue norvégienne",
                "en": "Norwegian fondue",
                "es": "Fondue noruega"
            },
            "desc": {
                "fr": "Les odeurs qui émanent de la tanière d'Igor ne laissent aucun doute là-dessus: il est devenu un grand amateur de fromages. De nouveaux produits laitiers apparaîtront dans les cavernes.",
                "en": "The smells that come from Igor's lair don't leave a doubt about it: he has become a great lover of cheese. New dairy products will appear now in the caverns.",
                "es": "Los olores que emanan de la guarida de Igor no dejan ninguna duda: ha caído en el lado pestoso de los quesos. Ahora aparecerán nuevos productos lácteos en las cavernas."
            },
            "items": [
                {
                    "id": 1031,
                    "quantity": 10,
                    "name": {
                        "fr": "Fromage piqué",
                        "en": "Cheese on a Stick",
                        "es": "Queso con palillo de dientes"
                    },
                    "img": "/assets/items/1031.svg"
                },
                {
                    "id": 1057,
                    "quantity": 10,
                    "name": {
                        "fr": "Pizza de Donatello",
                        "en": "Slice of Pizza",
                        "es": "Pizza de Donatello"
                    },
                    "img": "/assets/items/1057.svg"
                },
                {
                    "id": 1137,
                    "quantity": 1,
                    "name": {
                        "fr": "Hollandais",
                        "en": "Dutch Cake",
                        "es": "Sandwich tostado Holandés"
                    },
                    "img": "/assets/items/1137.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1011,
                        "name": {
                            "fr": "Fromages",
                            "en": "Fromages",
                            "es": "Fromages"
                        }
                    }
                ]
            }
        },
        {
            "id": 27,
            "name": {
                "fr": "Mystère de Guu",
                "en": "Guu's mistery",
                "es": "Misterio de Guu"
            },
            "desc": {
                "fr": "Cette quête n'a aucun intérêt, à part vous conseiller de découvrir au plus vite l'excellent dessin animé \"Haré + Guu\" disponible en DVD dans toutes les bonnes boutiques ! Banyaaaaaïï. ^^",
                "en": "This quest hasn't any practical use. It's only to suggest to you the great anime \"Jungle wa itsumo Hare nochi Guu\". Banyaaaaaïï ^__^",
                "es": "Esta exploración no tiene ningún interés, sólo el de aconsejarte el genial anime \"Jungle wa itsumo Hare nochi Guu\". Banyaaaaaïï ^__^"
            },
            "items": [
                {
                    "id": 88,
                    "quantity": 1,
                    "name": {
                        "fr": "Pokuté",
                        "en": "Hare Nochi Guuuuu!",
                        "es": "Pokuté"
                    },
                    "img": "/assets/items/88.svg"
                },
                {
                    "id": 99,
                    "quantity": 1,
                    "name": {
                        "fr": "Poils de Chourou",
                        "en": "Chourou's Hair",
                        "es": "Pelos de Chourou"
                    },
                    "img": "/assets/items/99.svg"
                },
                {
                    "id": 100,
                    "quantity": 1,
                    "name": {
                        "fr": "Pocket-Guu",
                        "en": "Pocket-Guu",
                        "es": "Guu de bolsillo"
                    },
                    "img": "/assets/items/100.svg"
                },
                {
                    "id": 1044,
                    "quantity": 1,
                    "name": {
                        "fr": "Manda",
                        "en": "Manda",
                        "es": "Manda"
                    },
                    "img": "/assets/items/1044.svg"
                }
            ]
        },
        {
            "id": 28,
            "name": {
                "fr": "Friandises divines",
                "en": "Divine sweets",
                "es": "Chucherías divinas"
            },
            "desc": {
                "fr": "Les sucreries n'ont plus aucun secret pour Igor. Il saura, à compter de ce jour, débusquer les délices légendaires de Harry \"le beau\" disséminés à travers tout Hammerfest.",
                "en": "Sweets are no longer a secret to Igor. Now he will be able to collect any of Good-looking Harry's legendary sweets that he finds in Hammerfest!",
                "es": "Las golosinas ya no son ningún secreto para Igor. ¡Ahora podrá arrasar con cualquier chuchería legendaria de Harry \"el guapo\" que se le presente!"
            },
            "items": [
                {
                    "id": 1059,
                    "quantity": 2,
                    "name": {
                        "fr": "Eclair noisette choco caramel et sucre",
                        "en": "Chocolate Eclair",
                        "es": "Rebanada de pan con crema de cacao y avellana"
                    },
                    "img": "/assets/items/1059.svg"
                },
                {
                    "id": 1084,
                    "quantity": 2,
                    "name": {
                        "fr": "Emi-Praline",
                        "en": "Dark Chocolate",
                        "es": "Emi-Praliné"
                    },
                    "img": "/assets/items/1084.svg"
                },
                {
                    "id": 1088,
                    "quantity": 2,
                    "name": {
                        "fr": "Escargot au chocolat persillé",
                        "en": "Snail Chocolate",
                        "es": "Caracol de chocolate de pasta de queso"
                    },
                    "img": "/assets/items/1088.svg"
                },
                {
                    "id": 1144,
                    "quantity": 2,
                    "name": {
                        "fr": "Smiley croquant",
                        "en": "Ghost Toast",
                        "es": "Smiley crujiente"
                    },
                    "img": "/assets/items/1144.svg"
                },
                {
                    "id": 1151,
                    "quantity": 2,
                    "name": {
                        "fr": "Tartine chocolat-noisette",
                        "en": "Slice of Chocolate Hazelnut Bread",
                        "es": "Tostada con crema de chocolate"
                    },
                    "img": "/assets/items/1151.svg"
                },
                {
                    "id": 1152,
                    "quantity": 2,
                    "name": {
                        "fr": "Tartine hémoglobine",
                        "en": "Slice of Raspberry Hazelnut Bread",
                        "es": "Tostada con hemoglobina"
                    },
                    "img": "/assets/items/1152.svg"
                },
                {
                    "id": 1153,
                    "quantity": 2,
                    "name": {
                        "fr": "Tartine à l'orange collante",
                        "en": "Slice of Sticky Orange Bread",
                        "es": "Tostada de aceite"
                    },
                    "img": "/assets/items/1153.svg"
                },
                {
                    "id": 1154,
                    "quantity": 2,
                    "name": {
                        "fr": "Tartine au miel",
                        "en": "Slice of Hard Honey Bread",
                        "es": "Tostada de mermelada pegajosa"
                    },
                    "img": "/assets/items/1154.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1009,
                        "name": {
                            "fr": "Délices de Harry 'le beau'",
                            "en": "Délices de Harry 'le beau'",
                            "es": "Délices de Harry 'le beau'"
                        }
                    }
                ]
            }
        },
        {
            "id": 29,
            "name": {
                "fr": "Igor et Cortex",
                "en": "Igor and Cortex",
                "es": "Igor y Cortex"
            },
            "desc": {
                "fr": "Igor a entrepris la fabrication de gadgets mystérieux... Attendez-vous à collectionner des machines étranges en jeu !",
                "en": "Igor has started to make mysterious gadgets... get ready to collect strange machines in the game!",
                "es": "Igor se ha puesto a fabricar cachibaches misteriosos... ¡prepárate para coleccionar máquinas extrañas en el juego!"
            },
            "items": [
                {
                    "id": 21,
                    "quantity": 10,
                    "name": {
                        "fr": "Enceinte Bessel-Son",
                        "en": "Jukebox Igor",
                        "es": "Altavoz Bájaloya"
                    },
                    "img": "/assets/items/21.svg"
                },
                {
                    "id": 26,
                    "quantity": 10,
                    "name": {
                        "fr": "Ampoule 30 watts",
                        "en": "Light Bulb",
                        "es": "Bombilla 30 watios"
                    },
                    "img": "/assets/items/26.svg"
                },
                {
                    "id": 31,
                    "quantity": 10,
                    "name": {
                        "fr": "Lunettes renversantes rouges",
                        "en": "Red Shades",
                        "es": "Gafas revolteadoras rojas"
                    },
                    "img": "/assets/items/31.svg"
                },
                {
                    "id": 85,
                    "quantity": 10,
                    "name": {
                        "fr": "Baton tonnerre",
                        "en": "Hammer Time 3000",
                        "es": "Martillo"
                    },
                    "img": "/assets/items/85.svg"
                },
                {
                    "id": 90,
                    "quantity": 10,
                    "name": {
                        "fr": "Fulguro pieds-en-mousse",
                        "en": "Slippery Bananas",
                        "es": "Fulguro pies-pesados"
                    },
                    "img": "/assets/items/90.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1018,
                        "name": {
                            "fr": "Les lois de la robotique",
                            "en": "Les lois de la robotique",
                            "es": "Les lois de la robotique"
                        }
                    }
                ]
            }
        },
        {
            "id": 30,
            "name": {
                "fr": "Affronter l'obscurité",
                "en": "Facing the darkness",
                "es": "Afrontar la oscuridad"
            },
            "desc": {
                "fr": "Igor sait maintenant s'éclairer ! Il pensera maintenant à apporter avec lui une torche pour ne pas trop se perdre dans l'obscurité des niveaux avancés !",
                "en": "Igor has learnt to light himself up! Thanks to this torch he won't get lost any more in the darkness of the advanced levels!",
                "es": "¡Igor ha aprendido a autoiluminarse! ¡Gracias a su antorcha incorporada ya no se perderá en la oscuridad de los niveles más avanzados!"
            },
            "items": [
                {
                    "id": 68,
                    "quantity": 10,
                    "name": {
                        "fr": "Bougie",
                        "en": "Candle of Light",
                        "es": "Vela"
                    },
                    "img": "/assets/items/68.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 14,
                        "name": {
                            "fr": "Igor-Newton",
                            "en": "Igor-Newton",
                            "es": "Igor-Newton"
                        }
                    },
                    {
                        "id": 106
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 13,
                        "name": {
                            "fr": "Eclairage antique",
                            "en": "Eclairage antique",
                            "es": "Eclairage antique"
                        }
                    }
                ]
            }
        },
        {
            "id": 31,
            "name": {
                "fr": "Et la lumière fût !",
                "en": "And light appeared!",
                "es": "¡Y se hizo la luz!"
            },
            "desc": {
                "fr": "Préparé à tous les dangers, le courageux Igor ne craint plus du tout l'obscurité dans les niveaux avancés !",
                "en": "Ready for every danger, Igor won't ever be afraid of the darkness of the advanced levels!",
                "es": "Preparado para los peligros de la vida moderna, ¡Igor no temerá nunca más a la oscuridad de los niveles avanzados!"
            },
            "items": [
                {
                    "id": 26,
                    "quantity": 10,
                    "name": {
                        "fr": "Ampoule 30 watts",
                        "en": "Light Bulb",
                        "es": "Bombilla 30 watios"
                    },
                    "img": "/assets/items/26.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 107
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 14,
                        "name": {
                            "fr": "Igor-Newton",
                            "en": "Igor-Newton",
                            "es": "Igor-Newton"
                        }
                    }
                ]
            }
        },
        {
            "id": 32,
            "name": {
                "fr": "Noël sur Hammerfest !",
                "en": "Christmas in Hammerfest!",
                "es": "¡Navidad en Hammerfest!"
            },
            "desc": {
                "fr": "Vous avez gagné 5 parties supplémentaires.",
                "en": "You won 5 extra lives!!",
                "es": "¡¡Has ganado 5 vidas extra!!"
            },
            "items": [
                {
                    "id": 109,
                    "quantity": 1,
                    "name": {
                        "fr": "Flocon simple",
                        "en": "Snowflake",
                        "es": "Copo de nieve simple"
                    },
                    "img": "/assets/items/109.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 16,
                        "name": {
                            "fr": "Flocon 2",
                            "en": "Flocon 2",
                            "es": "Flocon 2"
                        }
                    }
                ],
                "tokens": 5
            },
            "remove": {
                "families": [
                    {
                        "id": 15,
                        "name": {
                            "fr": "Flocon 1",
                            "en": "Flocon 1",
                            "es": "Flocon 1"
                        }
                    }
                ]
            }
        },
        {
            "id": 33,
            "name": {
                "fr": "Joyeux anniversaire Igor",
                "en": "Happy birthday Igor",
                "es": "Feliz cumpleaños Igor"
            },
            "desc": {
                "fr": "Vous avez gagné 10 parties supplémentaires !",
                "en": "You won 10 free games!!",
                "es": "¡¡Has ganado 10 partidas gratis!!"
            },
            "items": [
                {
                    "id": 110,
                    "quantity": 2,
                    "name": {
                        "fr": "Flocon bizarre",
                        "en": "Glacier",
                        "es": "Copo de nieve raro"
                    },
                    "img": "/assets/items/110.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 17,
                        "name": {
                            "fr": "Flocon 3",
                            "en": "Flocon 3",
                            "es": "Flocon 3"
                        }
                    }
                ],
                "tokens": 10
            },
            "remove": {
                "families": [
                    {
                        "id": 16,
                        "name": {
                            "fr": "Flocon 2",
                            "en": "Flocon 2",
                            "es": "Flocon 2"
                        }
                    }
                ]
            }
        },
        {
            "id": 34,
            "name": {
                "fr": "Cadeau céleste",
                "en": "Celestial present",
                "es": "Regalo celestial"
            },
            "desc": {
                "fr": "Vous avez gagné 20 parties supplémentaires !",
                "en": "You won 20 free games!!",
                "es": "¡¡Has ganado 20 partidas gratis!!"
            },
            "items": [
                {
                    "id": 111,
                    "quantity": 2,
                    "name": {
                        "fr": "Flocon ENORME !",
                        "en": "Gon Ga Jora",
                        "es": "Copo de nieve gigantesco"
                    },
                    "img": "/assets/items/111.svg"
                }
            ],
            "give": {
                "tokens": 20
            },
            "remove": {
                "families": [
                    {
                        "id": 17,
                        "name": {
                            "fr": "Flocon 3",
                            "en": "Flocon 3",
                            "es": "Flocon 3"
                        }
                    }
                ]
            }
        },
        {
            "id": 35,
            "name": {
                "fr": "Achat de parties amélioré",
                "en": "Game purchase enhanced",
                "es": "Compra de partidas mejorada"
            },
            "desc": {
                "fr": "Vos appels dans la boutique vous rapporteront dorénavant plus de flocons !",
                "en": "From now on, each time you buy games with our payment service, you'll obtain more games for the same price!",
                "es": "A partir de ahora cada vez que compres partidas con nuestro sistema de pago, ¡obtendrás más partidas que antes por el mismo precio!"
            },
            "items": [
                {
                    "id": 10,
                    "quantity": 5,
                    "name": {
                        "fr": "Téléphone-phone-phone",
                        "en": "Telephone",
                        "es": "Teléfono-no-no"
                    },
                    "img": "/assets/items/10.svg"
                }
            ],
            "give": {
                "bank": 25
            },
            "remove": {
                "families": [
                    {
                        "id": 18,
                        "name": {
                            "fr": "Kit d'appel main libre",
                            "en": "Kit d'appel main libre",
                            "es": "Kit d'appel main libre"
                        }
                    }
                ]
            }
        },
        {
            "id": 36,
            "name": {
                "fr": "Exterminateur de Sorbex",
                "en": "Sorbex exterminator",
                "es": "Exterminador de Sorbetex"
            },
            "desc": {
                "fr": "Vous avez pressé sans pitié les Citrons Sorbex !",
                "en": "You squeezed the Sorbex lemons without mercy!",
                "es": "¡Has presionado sin piedad a los limones Sorbetex!"
            },
            "items": [
                {
                    "id": 1178,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Citron Sorbex",
                        "en": "Statue: Sorbex Lemon",
                        "es": "Estatua: Sorbetex de Limón"
                    },
                    "img": "/assets/items/1178.svg"
                }
            ]
        },
        {
            "id": 37,
            "name": {
                "fr": "Désamorceur de Bombinos",
                "en": "Bombino disposal expert",
                "es": "Desactivador de Bombinos"
            },
            "desc": {
                "fr": "Vous avez survécu aux explosions des Bombinos !",
                "en": "You survived the Bombinos explosions!",
                "es": "¡Has sobrevivido a las explosiones de los Bombinos!"
            },
            "items": [
                {
                    "id": 1179,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Bombino",
                        "en": "Statue: Bombino",
                        "es": "Estatua: Bombino"
                    },
                    "img": "/assets/items/1179.svg"
                }
            ]
        },
        {
            "id": 38,
            "name": {
                "fr": "Tueur de poires",
                "en": "Pear killer",
                "es": "Asesino de peras"
            },
            "desc": {
                "fr": "Vous avez vaincu un nouveau boss: les Poires-Melbombes !",
                "en": "You killed a new monster: the Baddybombs Pears!",
                "es": "Has matado un nuevo monstruo: ¡las Peras Malabombas!"
            },
            "items": [
                {
                    "id": 1180,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Poire Melbombe",
                        "en": "Statue: Baddybomb Pear",
                        "es": "Estatua: Pera Malabomba"
                    },
                    "img": "/assets/items/1180.svg"
                }
            ]
        },
        {
            "id": 39,
            "name": {
                "fr": "Mixeur de Tagadas",
                "en": "Tagadas mixer",
                "es": "Triturador de Tagadas"
            },
            "desc": {
                "fr": "Vous n'avez fait qu'une bouchée des Fraises-Tagada !",
                "en": "You have only had a Tagada-Strawberry mouthful!",
                "es": "Receta de Batido de fresa: azúcar, leche, un huevo y un buen montón de Fresas Tagada. Poner nata al final para decorar."
            },
            "items": [
                {
                    "id": 1181,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Tagada",
                        "en": "Statue: Tagada-Strawberry",
                        "es": "Estatua: Tagada"
                    },
                    "img": "/assets/items/1181.svg"
                }
            ]
        },
        {
            "id": 40,
            "name": {
                "fr": "Kiwi frotte s'y pique",
                "en": "Kiwi itches! ouch, it scratches!",
                "es": "Kiwi rasca, vaya cómo pica..."
            },
            "desc": {
                "fr": "Vous avez su éviter tous les pièges des Sapeur-kiwis !",
                "en": "You knew how to avoid all the traps from the Kiwi Sappers!",
                "es": "¡Conseguiste evitar todas las trampas de los Kiwis Zapadores!"
            },
            "items": [
                {
                    "id": 1182,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Sapeur-kiwi",
                        "en": "Statue: Kiwi Sapper",
                        "es": "Estatua: Kiwi-Zapador"
                    },
                    "img": "/assets/items/1182.svg"
                }
            ]
        },
        {
            "id": 41,
            "name": {
                "fr": "Chasseur de Bondissantes",
                "en": "Leaping Hunter",
                "es": "Cazador de Sandinas"
            },
            "desc": {
                "fr": "Vous ne vous êtes pas laissé impressionner par la meute de Bondissantes !",
                "en": "Great, you haven't been impressed by the herd of Leapers!",
                "es": "Estupendo, ¡no te has dejado impresionar por la manada de Sandinas!"
            },
            "items": [
                {
                    "id": 1183,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Bondissante",
                        "en": "Statue: Leaper",
                        "es": "Estatua: Sandina"
                    },
                    "img": "/assets/items/1183.svg"
                }
            ]
        },
        {
            "id": 42,
            "name": {
                "fr": "Tronçonneur d'Ananargeddons",
                "en": "Armaggedon-Pineapple Killer",
                "es": "Aniquilador de Piñaguedones"
            },
            "desc": {
                "fr": "Vous êtes venu à bout d'un groupe d'Ananargeddons surentraînés !",
                "en": "You have gloriously come to the end of the fight against the Armageddon-Pineapples!",
                "es": "¡Llegaste gloriosamente hasta el final en la lucha contra los Piñaguedones!"
            },
            "items": [
                {
                    "id": 1184,
                    "quantity": 1,
                    "name": {
                        "fr": "Statue: Ananargeddon",
                        "en": "Statue: Armaggedon-Pineapple",
                        "es": "Estatua: Piñaguedón"
                    },
                    "img": "/assets/items/1184.svg"
                }
            ]
        },
        {
            "id": 43,
            "name": {
                "fr": "Roi de Hammerfest",
                "en": "Hammerfest King",
                "es": "Rey de Hammerfest"
            },
            "desc": {
                "fr": "Votre persévérance et votre ténacité ont eu raison du sorcier Tuberculoz: vous avez retrouvé la carotte de Igor ! Vous débuterez vos prochaines parties avec une vie supplémentaire et Igor portera fièrement sa carotte préférée.",
                "en": "You finished the game! Your perseverance and tenacity permitted you to defeat the evil Tuber and to recover Igor's nose. From now on you'll have an extra life and you'll play with the carrot!",
                "es": "¡Te has pasado el juego! Tu perseverancia y tu tenacidad te permitieron derrotar al malvado Tubérculo y recuperar la nariz de Igor. ¡A partir de ahora jugarás siempre con una vida extra y la zanahoria!"
            },
            "items": [
                {
                    "id": 102,
                    "quantity": 1,
                    "name": {
                        "fr": "La Carotte d'Igor",
                        "en": "Igor's Carrot",
                        "es": "La Zanahoria de Igor"
                    },
                    "img": "/assets/items/102.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 108
                    }
                ]
            }
        },
        {
            "id": 44,
            "name": {
                "fr": "Chapelier fou",
                "en": "Mad hatter",
                "es": "Sombrero loco"
            },
            "desc": {
                "fr": "Igor s'est découvert une passion nouvelle pour les coiffes. Vous pourrez maintenant appuyer sur la touche \"D\" pendant la partie pour changer de déguisement !",
                "en": "Igor has discovered a mad passion for hats. From now on, disguise Igor by pressing \"D\" key.",
                "es": "Igor ha descubierto una nueva pasión por los gorros. Podrás a partir de ahora darle a la tecla \"D\" durante la partida para cambiar de disfraz."
            },
            "items": [
                {
                    "id": 72,
                    "quantity": 5,
                    "name": {
                        "fr": "Chapeau de Mage-Gris",
                        "en": "Magician's Hat",
                        "es": "Sombrero de Mago Gris"
                    },
                    "img": "/assets/items/72.svg"
                },
                {
                    "id": 91,
                    "quantity": 5,
                    "name": {
                        "fr": "Couvre-chef de Luffy",
                        "en": "Luffy's Hat",
                        "es": "Sombrero de paja de Luffy"
                    },
                    "img": "/assets/items/91.svg"
                },
                {
                    "id": 92,
                    "quantity": 10,
                    "name": {
                        "fr": "Chapeau violin",
                        "en": "Chopper's Hat",
                        "es": "Sombrero de Tony Tony Chopper"
                    },
                    "img": "/assets/items/92.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 109
                    }
                ]
            }
        },
        {
            "id": 45,
            "name": {
                "fr": "Poney éco-terroriste",
                "en": "Eco-terrorist pony",
                "es": "Poni eco-terrorista"
            },
            "desc": {
                "fr": "Igor a accumulé suffisament de richesses pour financer ses activités louches en Hammerfest. Il peut maintenant se déguiser en appuyant sur la touche \"D\" pendant la partie ! Et sinon, si ca n'est pas déjà fait, avez-vous déjà visité www.dinoparc.com ?",
                "en": "Igor has accumulated enough wealth to finance his terrorist activities in Hammerfest. From now on, he will be able to disguise himself by pressing \"D\" key during the game! By the way, have you already played www.dinoparc.com?",
                "es": "Igor ha acumulado suficientes riquezas que le permiten financiar sus actividades terroristas en Hammerfest. ¡Ahora puede disfrazarse pulsando a la tecla \"D\" durante la partida! Por cierto, ¿has jugado ya a www.dinoparc.es?"
            },
            "items": [
                {
                    "id": 1053,
                    "quantity": 10,
                    "name": {
                        "fr": "Sou d'or",
                        "en": "Gold Coin",
                        "es": "Moneda de oro"
                    },
                    "img": "/assets/items/1053.svg"
                },
                {
                    "id": 1054,
                    "quantity": 10,
                    "name": {
                        "fr": "Gros tas de sous",
                        "en": "Big Unemployment",
                        "es": "Un montón de dinero"
                    },
                    "img": "/assets/items/1054.svg"
                },
                {
                    "id": 95,
                    "quantity": 3,
                    "name": {
                        "fr": "Cagnotte de Tuberculoz",
                        "en": "Tuber's Bag",
                        "es": "Saco de Tubérculo"
                    },
                    "img": "/assets/items/95.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 110
                    }
                ]
            }
        },
        {
            "id": 46,
            "name": {
                "fr": "Le Pioupiouz est en toi",
                "en": "The Pioupiou is in you",
                "es": "El Pioupiouz está en ti"
            },
            "desc": {
                "fr": "Il fallait s'y attendre: à force de ramasser n'importe quoi, Igor s'est fait gober par un Pioupiou ! Appuyez sur la touche \"D\" pendant la partie pour changer de déguisement (au fait, vous connaissiez le site www.pioupiouz.com ?)",
                "en": "We should have expected that: after collecting no matter what kind of item, Igor has finished by devouring a Pioupiou! Press \"D\" key during the game to change fancy dress.",
                "es": "Había que esperarlo: a base de recoger cualquier cosa, ¡Igor ha acabado devorando un Píopío! Pulsa la tecla \"D\" durante la partida para cambiar de disfraz."
            },
            "items": [
                {
                    "id": 112,
                    "quantity": 1,
                    "name": {
                        "fr": "Pioupiou carnivore",
                        "en": "Carnivorous PiouPiou",
                        "es": "Píopío carnívoro"
                    },
                    "img": "/assets/items/112.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 111
                    }
                ]
            }
        },
        {
            "id": 47,
            "name": {
                "fr": "Chasseur de champignons",
                "en": "Mushrooms hunter",
                "es": "Cazador de champiñones"
            },
            "desc": {
                "fr": "Comme son homologue italien (plombier de son état), Igor a une passion bizarre pour les champignons. Il pourra désormais se déguiser en appuyant sur la touche \"D\" pendant la partie !",
                "en": "Like his Italian counterpart (plumber in a red hat), Igor feels a great passion for mushrooms! You'll be able from now on to change Igor's dress during the game by pressing \"D\" key.",
                "es": "Como su homólogo italiano (fontanero con gorra roja), Igor siente una gran pasión por los champiñones. ¡Podrás a partir de ahora disfrazar a Igor pulsando la tecla \"D\" durante la partida!"
            },
            "items": [
                {
                    "id": 14,
                    "quantity": 1,
                    "name": {
                        "fr": "Délice hallucinogène bleu",
                        "en": "Blue Hallucinogenic Mushroom",
                        "es": "Delicia alucinógena azul"
                    },
                    "img": "/assets/items/14.svg"
                },
                {
                    "id": 15,
                    "quantity": 1,
                    "name": {
                        "fr": "Champignon rigolo rouge",
                        "en": "Red Delight Mushroom",
                        "es": "Seta chuli y roja"
                    },
                    "img": "/assets/items/15.svg"
                },
                {
                    "id": 16,
                    "quantity": 1,
                    "name": {
                        "fr": "Figonassée grimpante",
                        "en": "Green Beer Mushroom",
                        "es": "Pequeña Walalu de los bosques"
                    },
                    "img": "/assets/items/16.svg"
                },
                {
                    "id": 17,
                    "quantity": 1,
                    "name": {
                        "fr": "Petit Waoulalu des bois",
                        "en": "Golded Mushroom",
                        "es": "Seta dorada trepadora"
                    },
                    "img": "/assets/items/17.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 19,
                        "name": {
                            "fr": "Mario party",
                            "en": "Mario party",
                            "es": "Mario party"
                        }
                    },
                    {
                        "id": 112
                    }
                ]
            }
        },
        {
            "id": 48,
            "name": {
                "fr": "Successeur de Tuberculoz",
                "en": "Tuber's successor",
                "es": "Sucesor de Tubérculo"
            },
            "desc": {
                "fr": "Igor semble avoir... changé.. Son regard est maintenant plus froid. Il affiche une mine sombre et se cache maintenant sous une grande cape pourpre. Petit à petit, il devient ce qu'il a combattu... Vous pouvez maintenant revêtir l'apparence du sorcier Tuberculoz en appuyant sur \"D\" pendant la partie !",
                "en": "It seems like Igor has... changed. His look has become cool. He hides behind a great cloak and his face is dark. He's maybe changing little by little into the monster who was once his enemy. Disguise Igor as Tuber by pressing \"D\" key during the game!",
                "es": "Parece que Igor ha... cambiado. Su mirada se ha vuelto fría. Se esconde tras una gran capa marrón y ahora su semblante es oscuro. Parece que poco a poco se está transformando en quien fue su enemigo. ¡Disfrázate de Tubérculo pulsando la tecla \"D\" durante la partida!"
            },
            "items": [
                {
                    "id": 113,
                    "quantity": 1,
                    "name": {
                        "fr": "Cape de Tuberculoz",
                        "en": "The Robe of Tuber",
                        "es": "Capa de Tubérculo"
                    },
                    "img": "/assets/items/113.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 113
                    }
                ]
            }
        },
        {
            "id": 49,
            "name": {
                "fr": "La première clé !",
                "en": "The first Key!",
                "es": "¡La primera Llave!"
            },
            "desc": {
                "fr": "Igor a trouvé une sorte de Passe-partout en Bois. Nul doute qu'il ouvre une porte quelque part dans les cavernes...",
                "en": "Igor has found a Master Key. Undoubtedly, it will open a door somewhere in the caverns...",
                "es": "Igor ha encontrado una Llave Maestra de Madera. Sin duda debe de abrir una puerta en algún sitio de las cavernas..."
            },
            "items": [
                {
                    "id": 1190,
                    "quantity": 1,
                    "name": {
                        "fr": "Passe-partout en bois",
                        "en": "Wooden Master Key",
                        "es": "Llave Maestra de Madera"
                    },
                    "img": "/assets/items/1190.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5000
                    },
                    {
                        "id": 1023,
                        "name": {
                            "fr": "Clés perdues",
                            "en": "Clés perdues",
                            "es": "Clés perdues"
                        }
                    },
                    {
                        "id": 1025,
                        "name": {
                            "fr": "Boissons rigolotes",
                            "en": "Boissons rigolotes",
                            "es": "Boissons rigolotes"
                        }
                    },
                    {
                        "id": 1026,
                        "name": {
                            "fr": "Matériel administratif d'El Papah",
                            "en": "Matériel administratif d'El Papah",
                            "es": "Matériel administratif d'El Papah"
                        }
                    }
                ]
            }
        },
        {
            "id": 50,
            "name": {
                "fr": "Rigor Dangerous",
                "en": "Rigor Dangerous",
                "es": "Rigor Dangerous"
            },
            "desc": {
                "fr": "Vous avez découvert dans votre aventure une vieille clé rouillée mystérieuse ! Elle comporte une petite mention gravée: \"Rick\". Sans doute son ancien propriétaire...",
                "en": "You have discovered an old Rusty Key in your adventure! It has a small inscription: \"Rick\". Obviously, this was its previous owner.",
                "es": "¡Has descubierto en tu aventura una vieja Llave Oxidada! Contiene una pequeña inscripción: \"Rick\". Sin duda fue su antiguo propietario..."
            },
            "items": [
                {
                    "id": 1191,
                    "quantity": 1,
                    "name": {
                        "fr": "Clé de Rigor Dangerous",
                        "en": "Rigor Dangerous Key",
                        "es": "Llave de Rigor Dangerous"
                    },
                    "img": "/assets/items/1191.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5001
                    }
                ]
            }
        },
        {
            "id": 51,
            "name": {
                "fr": "La Méluzzine perdue",
                "en": "The lost Meluzin",
                "es": "La Meluzine perdida"
            },
            "desc": {
                "fr": "La Méluzzine, clé légendaire sortie des vieux contes hammerfestiens, ouvre à ce qu'on raconte la porte de grandes richesses. Reste à savoir où ?",
                "en": "They say that the Meluzin, the legendary key of the ancient Hammerfest tales, opens the door to the greatest wealth. But where is the door?",
                "es": "La Meluzine, llave legendaria salida de viejos cuentos hammerfestianos, abre según dicen la puerta a grandes riquezas. Queda por saber dónde está esa puerta..."
            },
            "items": [
                {
                    "id": 1192,
                    "quantity": 1,
                    "name": {
                        "fr": "Méluzzine",
                        "en": "Méluzzine Key",
                        "es": "Meluzina"
                    },
                    "img": "/assets/items/1192.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5002
                    }
                ]
            }
        },
        {
            "id": 52,
            "name": {
                "fr": "Enfin le Bourru !",
                "en": "At last, the drunk man!",
                "es": "¡Por fin el borracho!"
            },
            "desc": {
                "fr": "Cette étrange petite clé sent le vin.",
                "en": "This strange little key smells of wine: it's the Wine Key!",
                "es": "Esta extraña pequeña llave huele a vino: ¡es la Llave Borracha!"
            },
            "items": [
                {
                    "id": 1193,
                    "quantity": 1,
                    "name": {
                        "fr": "Clé du Bourru",
                        "en": "Wine Key",
                        "es": "Llave del Bruto"
                    },
                    "img": "/assets/items/1193.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5003
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 1021,
                        "name": {
                            "fr": "Clé avinée",
                            "en": "Clé avinée",
                            "es": "Clé avinée"
                        }
                    }
                ]
            }
        },
        {
            "id": 53,
            "name": {
                "fr": "Congélation",
                "en": "Frozen",
                "es": "Congelación"
            },
            "desc": {
                "fr": "Bien qu'étant un bonhomme de neige, Igor lui-même a du mal à garder cette clé en main, tant le froid qu'elle dégage est intense. La porte qu'elle ouvre donne sûrement sur les endroits les plus reculés de Hammerfest.",
                "en": "Like any other snowman, Igor can't keep a key like this one in his hand. The door that it opens gives access to the most hidden places of Hammerfest.",
                "es": "Como cualquier otro hombre de nieve, Igor tiene problemas para guardar esta llave en la mano. Esta puerta da acceso a los lugares más recónditos de Hammerfest."
            },
            "items": [
                {
                    "id": 1194,
                    "quantity": 1,
                    "name": {
                        "fr": "Furtok Glaciale",
                        "en": "Frozen Key",
                        "es": "Furtok Glacial"
                    },
                    "img": "/assets/items/1194.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5004
                    }
                ]
            }
        },
        {
            "id": 54,
            "name": {
                "fr": "Une clé rouillée",
                "en": "A rusty key",
                "es": "Una llave herrumbrosa"
            },
            "desc": {
                "fr": "Ce petit bout de ferraille difforme n'a pas l'air d'avoir une grande valeur. Mais il faut parfois ce méfier des apparences ! Qui peut savoir quel genre d'aventure se cache au delà de la porte qu'elle ouvre ?",
                "en": "This amorphous metallic thing does not seem to be worth a lot. Although, we should not trust appearances! Who knows what adventures hide beyond the door it opens?",
                "es": "Este pequeño trozo metálico amorfo no parece tener un gran valor. ¡Sin embargo hay que desconfiar de las apariencias! ¿Quién puede saber qué clase de aventura se esconde más allá de la puerta que abrirá?"
            },
            "items": [
                {
                    "id": 1195,
                    "quantity": 1,
                    "name": {
                        "fr": "Vieille clé rouillée",
                        "en": "Old Rusty Key",
                        "es": "Vieja Llave Oxidada"
                    },
                    "img": "/assets/items/1195.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5005
                    }
                ]
            }
        },
        {
            "id": 55,
            "name": {
                "fr": "Laissez passer !",
                "en": "Let it go!",
                "es": "¡Dejadlo libre!"
            },
            "desc": {
                "fr": "Votre toute puissance administrative sera dorénavant appuyée par le formulaire d'Autorisation du Bois-Joli BJ22a.",
                "en": "All your administrative power will be backed up by the LovelyWood BJ22a Authorization Form.",
                "es": "Todo tu poder administrativo contará con el apoyo del formulario de Autorización Madera-Bonita BJ22a."
            },
            "items": [
                {
                    "id": 1196,
                    "quantity": 1,
                    "name": {
                        "fr": "Autorisation du Bois-Joli",
                        "en": "Authorization of Bois-Joli",
                        "es": "Autorización Madera-Bonita"
                    },
                    "img": "/assets/items/1196.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5006
                    }
                ]
            },
            "remove": {
                "families": [
                    {
                        "id": 1022,
                        "name": {
                            "fr": "Paperasse",
                            "en": "Paperasse",
                            "es": "Paperasse"
                        }
                    }
                ]
            }
        },
        {
            "id": 56,
            "name": {
                "fr": "Les mondes ardus",
                "en": "The Arduous Worlds",
                "es": "Los Mundos Arduos"
            },
            "desc": {
                "fr": "Pour avoir atteint le niveau 50 en mode Cauchemar, vous avez gagné la Clé des Mondes Ardus.",
                "en": "You achieved level 50 in the Nightmare mode! You win the Arduous Worlds Key!",
                "es": "Debido a que has alcanzado el nivel 50 en el modo Pesadilla, ¡ganas la Llave de los Mundos Arduos!"
            },
            "items": [
                {
                    "id": 1197,
                    "quantity": 1,
                    "name": {
                        "fr": "Clé des Mondes Ardus",
                        "en": "Key of Difficult Worlds ",
                        "es": "Llave de los Mundos Arduos"
                    },
                    "img": "/assets/items/1197.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5007
                    }
                ]
            }
        },
        {
            "id": 57,
            "name": {
                "fr": "Viiiite !",
                "en": "Quickly!",
                "es": "¡Ráaapido!"
            },
            "desc": {
                "fr": "Sans trop savoir pourquoi, la Clé Piquante que vous avez trouvé vous donne une folle envie de courir partout et de vous rouler en boule.",
                "en": "You don't really know why, but the Itchy Key you've found, makes you run and roll everywhere!",
                "es": "Sin saber muy bien por qué, la Llave Que Rasca que has encontrado te da unas ganas tremendas de correr por todos lados y rodar como un ovillo."
            },
            "items": [
                {
                    "id": 1198,
                    "quantity": 1,
                    "name": {
                        "fr": "Clé piquante",
                        "en": "Pungent Key",
                        "es": "Llave Que Rasca"
                    },
                    "img": "/assets/items/1198.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5008
                    }
                ]
            }
        },
        {
            "id": 58,
            "name": {
                "fr": "Faire les poches à Tubz",
                "en": "Empty Tuber's pocket",
                "es": "Vacíarle los bolsillos a Tubérculo"
            },
            "desc": {
                "fr": "Tuberculoz, le vilain sorcier, portait sur lui une clé...",
                "en": "Tuber, the evil sorcerer, used to have a key...",
                "es": "Tubérculo, el hechicero malvado, llevaba una llave..."
            },
            "items": [
                {
                    "id": 1199,
                    "quantity": 1,
                    "name": {
                        "fr": "Passe-partout de Tuberculoz",
                        "en": "Tuber's Key",
                        "es": "Llave Maestra de Tubérculo"
                    },
                    "img": "/assets/items/1199.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5009
                    }
                ]
            }
        },
        {
            "id": 59,
            "name": {
                "fr": "Tuberculoz, seigneur des enfers",
                "en": "Tuber, Master of Hell",
                "es": "Tubérculo, señor de los infiernos"
            },
            "desc": {
                "fr": "Votre toute puissance et votre maîtrise absolue des techniques de combat de Hammerfest vous ont permi de gagner une clé unique en terrassant Tuberculoz en mode Cauchemar !",
                "en": "All your great power and skill in combat techniques gave you the chance to defeat Tuber in the Nightmare mode! You're now the privileged owner of the Unique Key.",
                "es": "¡Todo tu poder y maestría absoluta de las técnicas de combate de Hammerfest permitieron que derrotaras al mismísimo Tubérculo en el modo Pesadilla! Gracias a ello eres el privilegiado poseedor de la Llave Única."
            },
            "items": [
                {
                    "id": 1200,
                    "quantity": 1,
                    "name": {
                        "fr": "Clé des cauchemars",
                        "en": "Nightmare Key",
                        "es": "Llave de las Pesadillas"
                    },
                    "img": "/assets/items/1200.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5010
                    }
                ]
            }
        },
        {
            "id": 60,
            "name": {
                "fr": "L'eau ferrigineuneuse",
                "en": "Metallic water",
                "es": "El agua que sabe a perfume"
            },
            "desc": {
                "fr": "Votre excès dans la consommation de boissons alcoolisées vous a permis de débloquer la Clé du Bourru ! Vous aurez maintenant de bonnes chances de la trouver au cours de vos explorations.",
                "en": "Alcohol abuse is bad for your health, according to the health authorities. However, in Hammerfest it has enabled you to unblock the Wine Key! From now on, you have a lot of chances of finding it in the caverns.",
                "es": "El exceso del consumo de alcohol es perjudicial para la salud. Lo dicen las autoridades sanitarias. Sin embargo, ¡en Hammerfest te ha permitido desbloquear la Llave Borracha! A partir de ahora tienes muchas posibilidades de encontrarla en las cavernas."
            },
            "items": [
                {
                    "id": 1209,
                    "quantity": 20,
                    "name": {
                        "fr": "Canette Express",
                        "en": "Can Beer",
                        "es": "Lata Express"
                    },
                    "img": "/assets/items/1209.svg"
                },
                {
                    "id": 1210,
                    "quantity": 10,
                    "name": {
                        "fr": "Bouteille aux 2064 bulles",
                        "en": "Sealed Beer",
                        "es": "Cerveza de las fiestas en Bordeaux"
                    },
                    "img": "/assets/items/1210.svg"
                },
                {
                    "id": 1211,
                    "quantity": 5,
                    "name": {
                        "fr": "Mousse volante",
                        "en": "Draft Beer",
                        "es": "Espuma voladora"
                    },
                    "img": "/assets/items/1211.svg"
                },
                {
                    "id": 1212,
                    "quantity": 3,
                    "name": {
                        "fr": "Vin Merveilleux",
                        "en": "Wonderful Wine",
                        "es": "Vino burgués del Gran Teatro"
                    },
                    "img": "/assets/items/1212.svg"
                },
                {
                    "id": 1213,
                    "quantity": 2,
                    "name": {
                        "fr": "Liqueur maléfique",
                        "en": "Evil Liquor",
                        "es": "Licor maléfico"
                    },
                    "img": "/assets/items/1213.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1021,
                        "name": {
                            "fr": "Clé avinée",
                            "en": "Clé avinée",
                            "es": "Clé avinée"
                        }
                    }
                ]
            }
        },
        {
            "id": 61,
            "name": {
                "fr": "Paperasse administrative",
                "en": "Paperwork",
                "es": "Papeleo"
            },
            "desc": {
                "fr": "Igor est un maître dans l'art de remplir des formulaires administratifs. Il trouvera donc sans problème bientôt l'Autorisation du Bois-Joli dans les dédales des cavernes...",
                "en": "Igor is an expert in filling in administrative forms. You'll soon find the LovelyWood BJ22a Authorization Form in the caverns.",
                "es": "Igor es un maestro en el arte de rellenar formularios administrativos. Pronto podrás encontrar la Autorización Madera-bonita BJ22a en las cavernas."
            },
            "items": [
                {
                    "id": 1214,
                    "quantity": 10,
                    "name": {
                        "fr": "Tampon MT",
                        "en": "MT Stamp",
                        "es": "Sello MotionTwin"
                    },
                    "img": "/assets/items/1214.svg"
                },
                {
                    "id": 1215,
                    "quantity": 15,
                    "name": {
                        "fr": "Facture gratuite",
                        "en": "Free Bill",
                        "es": "Factura gratis"
                    },
                    "img": "/assets/items/1215.svg"
                },
                {
                    "id": 1216,
                    "quantity": 3,
                    "name": {
                        "fr": "Post-It de François",
                        "en": "Sticky Notes",
                        "es": "Post-It de François, no entiendo tu letra"
                    },
                    "img": "/assets/items/1216.svg"
                },
                {
                    "id": 1217,
                    "quantity": 2,
                    "name": {
                        "fr": "Pot à crayon solitaire",
                        "en": "Pencil Container",
                        "es": "Lapicero solitario sin pegatina de perro"
                    },
                    "img": "/assets/items/1217.svg"
                },
                {
                    "id": 1218,
                    "quantity": 1,
                    "name": {
                        "fr": "Agrafeuse du Chaos",
                        "en": "Chaos Stapler",
                        "es": "Grapadora del Caos"
                    },
                    "img": "/assets/items/1218.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1022,
                        "name": {
                            "fr": "Paperasse",
                            "en": "Paperasse",
                            "es": "Paperasse"
                        }
                    }
                ]
            }
        },
        {
            "id": 62,
            "name": {
                "fr": "Meilleur joueur",
                "en": "Best Player",
                "es": "Mejor jugador"
            },
            "desc": {
                "fr": "Le meilleur joueur du net, c'est vous, plus personne n'a de doute là dessus ! Votre collection de manettes de jeu fera sans nul doute des envieux ^^ Igor dispose maintenant de l'option Tornade qui procure un boost de vitesse au début de chaque partie, quelques soient les options choisies !",
                "en": "The best videogame player is you, there's no doubt about it! Your control pads collection is enviable! From now on, Igor has a speed bonus at the beginning of each game.",
                "es": "El mejor jugador de videojuegos eres tú, ¡sin ninguna duda! ¡Tu colección de mandos de consola es envidiable! A partir de ahora Igor posee desde el principio de cada partida un bonus en rapidez."
            },
            "items": [
                {
                    "id": 1201,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad Sounie",
                        "en": "Sony Controller",
                        "es": "Mando Suni"
                    },
                    "img": "/assets/items/1201.svg"
                },
                {
                    "id": 1202,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad Frusion 64",
                        "en": "Nintendo 64 Controller",
                        "es": "Mando Nientiendo 64"
                    },
                    "img": "/assets/items/1202.svg"
                },
                {
                    "id": 1203,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad Game-Pyramid",
                        "en": "Game-Cube Controller",
                        "es": "Mando Game-Pirámide"
                    },
                    "img": "/assets/items/1203.svg"
                },
                {
                    "id": 1204,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad Sey-Ga",
                        "en": "Genesis Controller",
                        "es": "Mando Sey-Ga"
                    },
                    "img": "/assets/items/1204.svg"
                },
                {
                    "id": 1205,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad Super Frusion",
                        "en": "SNES Controller",
                        "es": "Mando Super Nientiendo"
                    },
                    "img": "/assets/items/1205.svg"
                },
                {
                    "id": 1206,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad du Système Maitre",
                        "en": "NES Controller",
                        "es": "Mando del Sistema Maestro"
                    },
                    "img": "/assets/items/1206.svg"
                },
                {
                    "id": 1207,
                    "quantity": 1,
                    "name": {
                        "fr": "Pad Frusion Entertainment System",
                        "en": "Master System Controller",
                        "es": "Mando Nientiendo Entertainment System"
                    },
                    "img": "/assets/items/1207.svg"
                },
                {
                    "id": 1208,
                    "quantity": 1,
                    "name": {
                        "fr": "Manette S-Téhéf",
                        "en": "Joystick",
                        "es": "Mando S-Tehef"
                    },
                    "img": "/assets/items/1208.svg"
                }
            ],
            "give": {
                "option": "boost"
            }
        },
        {
            "id": 63,
            "name": {
                "fr": "Miroir, mon beau miroir",
                "en": "Mirror, my beautiful mirror",
                "es": "Espejo, mi bonito espejo"
            },
            "desc": {
                "fr": "Le Miroir Bancal que vous avez trouvé en jeu vous permet maintenant de voir les choses sous un angle nouveau. L'option de jeu \"Miroir\" a été débloquée !",
                "en": "The Bancal Mirror you have found in the game shows you things from another point of view. The extra option \"Mirror\" has been unblocked!",
                "es": "El Espejo Bancal que has encontrado en el juego te permite ahora ver las cosas desde un ángulo diferente. ¡La opción suplementaria de juego \"Espejo\" ha sido desbloqueada!"
            },
            "items": [
                {
                    "id": 1219,
                    "quantity": 1,
                    "name": {
                        "fr": "Miroir bancal",
                        "en": "Mirror of Bancal",
                        "es": "Espejo de la cábala"
                    },
                    "img": "/assets/items/1219.svg"
                }
            ],
            "give": {
                "option": "mirror"
            }
        },
        {
            "id": 64,
            "name": {
                "fr": "Mode cauchemar",
                "en": "Nightmare mode",
                "es": "Modo pesadilla"
            },
            "desc": {
                "fr": "Vous êtes doué. Surement très doué même... Mais saurez-vous aider Igor en mode Cauchemar ? Cette option a été débloquée !",
                "en": "You're talented. Very talented... but, are you able to help Igor in the Nightmare mode? This new option has been unblocked!",
                "es": "Tienes talento. Mucho talento... pero ¿serías capaz de ayudar a Igor en el modo Pesadilla? ¡Esta opción ha sido desbloqueada!"
            },
            "items": [
                {
                    "id": 1220,
                    "quantity": 1,
                    "name": {
                        "fr": "Etoile du Diable",
                        "en": "Star of the Devil",
                        "es": "Estrella demoníaca"
                    },
                    "img": "/assets/items/1220.svg"
                }
            ],
            "give": {
                "option": "nightmare"
            }
        },
        {
            "id": 65,
            "name": {
                "fr": "L'aventure continue !",
                "en": "The adventure continues!",
                "es": "¡La aventura continúa!"
            },
            "desc": {
                "fr": "Le conseil des Carotteux vous a choisi pour explorer plus en avant les cavernes de Hammerfest. La Clé de Gordon est une première étape dans cette nouvelle mission.",
                "en": "The Carrotian Council has chosen you to explore the Caverns of Hammerfest in-depth. The Gordon's Key is only the first step of a new great mission...",
                "es": "El consejo de los Zanahorios y Panteonianos te ha elegido para explorar con más atención las Cavernas de Hammerfest. La Llave de Gordon es tan sólo la primera etapa de una nueva misión."
            },
            "items": [
                {
                    "id": 117,
                    "quantity": 1,
                    "name": {
                        "fr": "Clé de Gordon",
                        "en": "Gordon's Key",
                        "es": "Llave de Gordon"
                    },
                    "img": "/assets/items/117.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5012
                    }
                ]
            }
        },
        {
            "id": 66,
            "name": {
                "fr": "Joyau d'Ankhel",
                "en": "Ankhel Jewel",
                "es": "Joya de Ankhel"
            },
            "desc": {
                "fr": "Vous avez fait preuve d'une dextérité et d'une perspicacité sans égal en retrouvant le Joyau d'Ankhel. L'option Controle du Ballon a été débloquée pour le mode SoccerFest !",
                "en": "You have shown your great ability and dexterity in finding the Ankhel Jewel. The Advanced Ball Control option has been unblocked in the Soccerfest mode game!",
                "es": "Has hecho prueba de una habilidad y perspicacia sin igual al encontrar la Joya de Ankhel. ¡La opción Control Avanzado del Balón ha sido desbloqueada en el modo Fútbolfest!"
            },
            "items": [
                {
                    "id": 116,
                    "quantity": 1,
                    "name": {
                        "fr": "Joyau d'Ankhel",
                        "en": "The Ankhel Jewel",
                        "es": "Joya de Ankhel"
                    },
                    "img": "/assets/items/116.svg"
                }
            ],
            "give": {
                "option": "kickcontrol"
            }
        },
        {
            "id": 67,
            "name": {
                "fr": "Sandy commence l'aventure !",
                "en": "Sandy's adventure begins!",
                "es": "¡Sandy comienza la aventura!"
            },
            "desc": {
                "fr": "Tous les éléments sont réunis pour donner naissance à Sandy, le bonhomme de sable ! Ce nouveau personnage pourra se joindre à vous dans le mode Multi Coopératif, jouable à deux sur le même ordinateur. Re-découvrez la grande aventure avec un ami !",
                "en": "            You’ve got all the necessary elements to bring Sandy the Sandman to life!             This new character can join you in the Adventure Mode (Two players mode             on the same computer).           ",
                "es": "¡Todos los elementos han sido reunidos para en fin dar vida a Sandy, el hombre de arena! Este nuevo personaje podrá unirse a ti en el modo Cooperativo, disponible para dos jugadores en el mismo ordenador. ¡Re-descubre la gran aventura con un amigo!"
            },
            "items": [
                {
                    "id": 1221,
                    "quantity": 5,
                    "name": {
                        "fr": "Poudre de plage magique",
                        "en": "Shovel",
                        "es": "Polvo de playa mágica"
                    },
                    "img": "/assets/items/1221.svg"
                },
                {
                    "id": 1222,
                    "quantity": 10,
                    "name": {
                        "fr": "Matériel d'architecte",
                        "en": "Bucket of Sand",
                        "es": "Material de arquitecto"
                    },
                    "img": "/assets/items/1222.svg"
                },
                {
                    "id": 1223,
                    "quantity": 5,
                    "name": {
                        "fr": "Maquette en sable",
                        "en": "Sand Castle",
                        "es": "Maqueta de arena"
                    },
                    "img": "/assets/items/1223.svg"
                },
                {
                    "id": 1224,
                    "quantity": 3,
                    "name": {
                        "fr": "Winkel",
                        "en": "Winkel O'Riely",
                        "es": "Wink"
                    },
                    "img": "/assets/items/1224.svg"
                }
            ],
            "give": {
                "mode": "multicoop"
            },
            "remove": {
                "families": [
                    {
                        "id": 1028,
                        "name": {
                            "fr": "Sandy en kit",
                            "en": "Sandy en kit",
                            "es": "Sandy en kit"
                        }
                    }
                ]
            }
        },
        {
            "id": 68,
            "name": {
                "fr": "Miroir, NOTRE beau miroir",
                "en": "Mirror, OUR beautiful mirror",
                "es": "Espejo, NUESTRO bonito espejo"
            },
            "desc": {
                "fr": "Avec le Miroir des Sables, vous pouvez maintenant voir les choses sous un angle nouveau mais à deux ! L'option de jeu \"Miroir\" a été débloquée pour le mode Multi Coopératif !",
                "en": "            You can now see things from a different angle with your friend. The game option \"Mirror\"             has been added to the Two Players game mode.",
                "es": "Con el Espejo de las Arenas, puedes ver la cosas desde nuevo ángulo... ¡y con un amigo! La opción de juego \"Espejo\" ha sido desbloqueada para el modo Cooperativo."
            },
            "items": [
                {
                    "id": 1225,
                    "quantity": 1,
                    "name": {
                        "fr": "Miroir des Sables",
                        "en": "Mirror of Sables",
                        "es": "Espejo de las Arenas"
                    },
                    "img": "/assets/items/1225.svg"
                }
            ],
            "give": {
                "option": "mirrormulti"
            }
        },
        {
            "id": 69,
            "name": {
                "fr": "Mode double cauchemar",
                "en": "Double-nightmare option",
                "es": "Modo Doble Pesadilla"
            },
            "desc": {
                "fr": "De toute évidence, vous êtes capables de grandes choses à deux ! L'option de jeu \"Cauchemar\" a été débloquée pour le mode Multi Coopératif !",
                "en": "            It is evident that you are able to do great things together with a friend! The game option \"Nightmare\"             has been added to the Two Players game mode.",
                "es": "¡Está claro que eres capaz de llegar lejos cuando estás acompañado! La opción de juego \"Pesadilla\" ha sido desbloqueada para el modo Cooperativoooooo."
            },
            "items": [
                {
                    "id": 1226,
                    "quantity": 1,
                    "name": {
                        "fr": "Etoile des Diables Jumeaux",
                        "en": "Star of the Devil's Twins",
                        "es": "Estrella de los Diablos Gemelos"
                    },
                    "img": "/assets/items/1226.svg"
                }
            ],
            "give": {
                "option": "nightmaremulti"
            }
        },
        {
            "id": 70,
            "name": {
                "fr": "Une grande Amitié",
                "en": "A great Friendship",
                "es": "Una gran Amistad"
            },
            "desc": {
                "fr": "Mieux vaut tard que jamais ! Igor et Sandy ont enfin compris qu'il valait mieux s'entraider s'ils voulaient survivre à deux dans les Cavernes de Hammerfest. L'option de jeu \"Partage de vies\" a été débloquée pour le mode Multi Coopératif ! Si cette option est activée, lorsqu'un joueur perd sa dernière vie, il en prend une au second joueur et peut ainsi continuer la partie !",
                "en": "            Better late than never! Igor and Sandy have finally understood the importance of staying together             in order to survive in the Caverns. The game option \"Lives sharing\" has been unblocked for             the Two Players mode! With this option enabled, if a player loses his latest life, he will             take a life from the second player and then he will be able to continue playing!",
                "es": "¡Más vale tarde que nunca! Igor y Sandy han comprendido que es mejor trabajar juntos para sobrevivir en las Cavernas. La opción de juego \"Compartir vidas\" ha sido desbloqueada para el modo Cooperativo. Si esta opción es activada, cuando un jugador pierde su última vida, utiliza una del segundo jugador para poder continuar la partida."
            },
            "items": [
                {
                    "id": 1227,
                    "quantity": 2,
                    "name": {
                        "fr": "Sceau d'amitié",
                        "en": "Mixed Heart",
                        "es": "Sello de amistad"
                    },
                    "img": "/assets/items/1227.svg"
                }
            ],
            "give": {
                "option": "lifesharing"
            }
        },
        {
            "id": 71,
            "name": {
                "fr": "Apprentissage des canifs volants",
                "en": "Learning how to launch Shurikens",
                "es": "Aprendizaje de lanzamiento shurikens"
            },
            "desc": {
                "fr": "A toujours sauter partout, Igor a fini par acquérir une souplesse digne d'un ninja et une dextérité hors du commun ! Mais pour prouver sa valeur, il doit maintenant collecter les artefacts ninjas dispersés en Hammerfest !",
                "en": "            Jumping and jumping around has improved Igor's agility and now he's turning into a ninja!             In order to prove his courage, he must collect all the ninja items hidden in Hammerfest!",
                "es": "De tanto saltar Igor ha desarrollado una habilidad digna de un gran ninja... Para probar su talento, deberá recoger artefactos ninja dispersados en las cavernas."
            },
            "items": [
                {
                    "id": 1228,
                    "quantity": 1,
                    "name": {
                        "fr": "Insigne de l'ordre des Ninjas",
                        "en": "Symbol of the Ninjas",
                        "es": "Insignia de la orden de los Ninjas"
                    },
                    "img": "/assets/items/1228.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 1030,
                        "name": {
                            "fr": "Artefacts du Ninjutsu",
                            "en": "Artefacts du Ninjutsu",
                            "es": "Artefacts du Ninjutsu"
                        }
                    }
                ]
            }
        },
        {
            "id": 72,
            "name": {
                "fr": "Shinobi do !",
                "en": "Shinobi do!",
                "es": "¡Shinobi do!"
            },
            "desc": {
                "fr": "Igor a rempli sa quêté initiatique et maîtrise maintenant à la perfection un grand nombre d'armes du Ninjutsu ! Mais comme son nouveau de l'honneur le lui interdit, il ne pourra pas s'en servir. Toutefois, il pourra mettre à l'épreuve ses compétences grâce à l'option de jeu \"Ninjutsu\" qu'il vient de débloquer pour le mode Aventure !",
                "en": "            Igor has completed his starting quest and now he's a grand master of Ninjutsu weapons!             Nevertheless, his Code of Honour prevents him from using them. Although now he will be able             to test his skills in the \"Ninjutsu\" game mode he has unblocked!",
                "es": "¡Igor ha terminado la exploración de iniciación en la vía del Ninjutsu! Gracias a ello la opción de juego \"Ninjutsu\" ha sido desbloqueada para el modo Aventura."
            },
            "items": [
                {
                    "id": 1229,
                    "quantity": 5,
                    "name": {
                        "fr": "Couteau suisse japonais",
                        "en": "Magic Katana",
                        "es": "Cuchillo suizo japonés"
                    },
                    "img": "/assets/items/1229.svg"
                },
                {
                    "id": 1230,
                    "quantity": 2,
                    "name": {
                        "fr": "Shuriken de second rang",
                        "en": "Fūma Shuriken",
                        "es": "Shuriken de segundo rango"
                    },
                    "img": "/assets/items/1230.svg"
                },
                {
                    "id": 1231,
                    "quantity": 7,
                    "name": {
                        "fr": "Shuriken d'entraînement",
                        "en": "Shuriken",
                        "es": "Shuriken de entrenamiento"
                    },
                    "img": "/assets/items/1231.svg"
                },
                {
                    "id": 1232,
                    "quantity": 7,
                    "name": {
                        "fr": "Najinata",
                        "en": "Kunai",
                        "es": "Najinata"
                    },
                    "img": "/assets/items/1232.svg"
                },
                {
                    "id": 1233,
                    "quantity": 1,
                    "name": {
                        "fr": "Lance-boulettes de Précision",
                        "en": "Blowgun of Precision",
                        "es": "Lanza-bolitas de Precisión"
                    },
                    "img": "/assets/items/1233.svg"
                },
                {
                    "id": 1234,
                    "quantity": 1,
                    "name": {
                        "fr": "Ocarina chantant",
                        "en": "Singing Ocarina",
                        "es": "Ocarina cantante"
                    },
                    "img": "/assets/items/1234.svg"
                },
                {
                    "id": 1235,
                    "quantity": 1,
                    "name": {
                        "fr": "Armure de la nuit",
                        "en": "Armor of the Night",
                        "es": "Casco samurai"
                    },
                    "img": "/assets/items/1235.svg"
                }
            ],
            "give": {
                "option": "ninja"
            }
        },
        {
            "id": 73,
            "name": {
                "fr": "Rapide comme l'éclair !",
                "en": "As quick as the lightning!",
                "es": "Rápido como el rayo..."
            },
            "desc": {
                "fr": "Igor est imbattable dès lors qu'il s'agit de viser juste et se déplacer avec précision... Il faut maintenant le prouver aux autres ! Le mode TIME ATTACK est débloqué et vous ouvre en plus l'accès à un nouveau classement sur le site. Soyez le bonhomme de neige le plus rapide de tout Hammerfest !",
                "en": "            Igor is unbeatable when he needs to move fast. He now has to prove it to the others!             The TIME ATTACK mode and its ranking have been unblocked. Be the fastest snowman in all Hammerfest!",
                "es": "Igor es imbatible para apuntar y lanzar con precisión... ¡Ahora hay que probárselo a todo el mundo! El modo CONTRARRELOJ ha sido desbloqueado y te ofrece la posibilidad de acceder a la nueva clasificación CONTRARRELOJ. ¡Ya puedes ser el muñeco de nieve más rápido de Hammerfest!"
            },
            "items": [
                {
                    "id": 1236,
                    "quantity": 100,
                    "name": {
                        "fr": "Insigne du Mérite",
                        "en": "Award of Merit",
                        "es": "Insignia del Mérito"
                    },
                    "img": "/assets/items/1236.svg"
                }
            ],
            "give": {
                "mode": "timeattack",
                "option": "set_ta_0"
            }
        },
        {
            "id": 74,
            "name": {
                "fr": "Maître des Bombes",
                "en": "Bomb Master",
                "es": "Maestro de Bombas"
            },
            "desc": {
                "fr": "Vous êtes l'expert réputé dans tout Hammerfest en matière d'explosifs. Et pour montrer qu'on ne vous ne la fait pas, à vous, l'option \"Explosifs instables\" a été débloquée pour le mode Aventure ! Gare à TOUT ce qui explose en jeu ! De plus, vous pouvez pousser toutes vos bombes plus loin en avançant en même temps que vous la frappez.",
                "en": "            You are a well-known expert in explosive material. To demonstrate this to everybody, you can             now enable the \"Unstable explosives\" option in the Adventure mode. Pay attention to everything             that explodes in the game! You can also push the bombs farther and move while you hit them.",
                "es": "Eres el mayor experto en materia explosiva de la región de Hammerfest.  La opción \"Explosivos inestables\" ha sido desbloqueada para el modo Aventura. Además, también podrás empujar las bombas más lejos al mismo tiempo que avanzas y golpeas."
            },
            "items": [
                {
                    "id": 1237,
                    "quantity": 1,
                    "name": {
                        "fr": "Neige-o-glycérine",
                        "en": "Snow Bomb",
                        "es": "Nieve a la glicerina"
                    },
                    "img": "/assets/items/1237.svg"
                }
            ],
            "give": {
                "option": "bombexpert",
                "log": true
            }
        },
        {
            "id": 75,
            "name": {
                "fr": "Tombeau de Tuberculoz",
                "en": "Tuber's tomb",
                "es": "Tumba de Tubérculo"
            },
            "desc": {
                "fr": "Vous avez découvert une étrange pyramide de poche dans le tombeau de Tuberculoz.",
                "en": "            You have discovered a bizarre pocket-size pyramid in Tuber's tomb.",
                "es": "Has descubierto una extraña pirámide de bolsillo en la tumba de Tubérculo."
            },
            "items": [
                {
                    "id": 1238,
                    "quantity": 1,
                    "name": {
                        "fr": "Pass-Pyramide",
                        "en": "Pyramid Pass",
                        "es": "Pase VIP Pirámide"
                    },
                    "img": "/assets/items/1238.svg"
                }
            ],
            "give": {
                "families": [
                    {
                        "id": 5014
                    }
                ]
            }
        }
    ]

    async findAll() {
        return this.quests;
    }

}