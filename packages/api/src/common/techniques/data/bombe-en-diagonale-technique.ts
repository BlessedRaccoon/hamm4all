import { Technique } from "@hamm4all/shared";
import paths from "../../../shared/paths/path.const"

const bombeEnDiagonaleTechnique: Technique = {
    "name": "Bombe en Diagonale",
    "presentation": "La bombe en diagonale est une technique d'une extrême importance, qui nécessite d'avoir débloqué le pouvoir de lever les bombes.",
    "description": "Cette technique consiste à poser une bombe, taper dedans et très rapidement appuyer sur la touche bas pour la lever. Cela aura comme effet d'envoyer la bombe en diagonale (aussi appelé \"tir en lob\").",
    "videos": [
        [
            {
                "name": "Bombe en Diagonale",
                "type": "webm",
                "source": "/assets/videos/bombediagonale.webm"
            }
        ]
    ],
    "utility": `Cette technique est aussi utilisée dans des niveaux tels que le [niveau 53](${paths.getLevelPath(31, "53")}), car la bombe en diagonale bien timée permet d'atteindre un fruit qui se situerait 6 blocs au dessus d'Igor.`
}; 

export default bombeEnDiagonaleTechnique;