import { Technique } from "@hamm4all/shared"

const doubleImpulsionTechnique: Technique = {
    "name": "Double Impulsion",
    "presentation": "La technique de la double impulsion est probablement la technique la plus difficile à maîtriser sur Hammerfest. Elle nécessite d'avoir l'option \"Explosifs Instables\".\nCette technique permet, quand elle est exécutée à la perfection, d'élever Igor de 9 cases complètes à partir de l'endroit où il était. Les applications de cette techniques sont extrêmement rares cependant, difficile de frimer en l'utilisant de manière concrète.",
    "description": "Pour réaliser cette technique, il faut poser une bombe, la jeter en l'air pour poser une autre bombe sur Igor un instant après. Ensuite, il faut lever les deux bombes en l'air en même temps qu'Igor, puis, juste avant que la première bombe explose, lever la deuxième bombe une deuxième fois pendant qu'elle est en l'air.\nIgor sera donc propulsé par la puissance des deux bombes et de l'option \"explosifs instables\", d'où l'appélation de \"Double Impulsion\".",
    "videos": [
        [
            {
                "name": "Double Impulsion",
                "type": "webm",
                "source": "/assets/videos/doubleimpulsion.webm"
            }
        ]
    ],
    "utility": "Comme dit plus haut, les cas concrets d'application de cette technique sont minimes, peut-être dans une prochaine contrée d'Eternalfest ?"
}

export default doubleImpulsionTechnique