import { Technique } from "@hamm4all/shared";

const sauteMoutonTechnique: Technique = {
    "name": "Saute Mouton",
    "presentation": "La technique du saute-mouton est une technique simple ayant plein de variantes, que l'on doit apprendre rapidement en début de jeu pour pouvoir parfois se sortir de quelques impasses.\nContrairement à beaucoup de techniques, celle-ci peut se maîtriser très facilement, et peut être exécutée systématiquement sans erreur une fois apprise.",
    "description": "Cette technique permet de franchir un espace de 2, 3 ou 4 (en cauchemar ou tornade) blocs entre deux plateformes. Il suffit d'avancer en direction du fossé entre les plateformes, puis de se laisser tomber tout en continuant d'avancer. En arrivant au milieu de l'espace entre les deux plateformes, lâchez une bombe tout en continuant d'avancer, et voilà ! Vous êtes maintenant de l'autre côté.",
    "videos": [
        [
            {
                "name": "2 cases",
                "type": "webm",
                "source": "/assets/videos/sautemouton2.webm"
            }
        ],
        [
            {
                "name": "3 cases",
                "type": "webm",
                "source": "/assets/videos/sautemouton3.webm"
            }
        ],
        [
            {
                "name": "4 cases",
                "type": "webm",
                "source": "/assets/videos/sautemouton4.webm"
            }
        ],
        [
            {
                "name": "7 cases",
                "type": "webm",
                "source": "/assets/videos/sautemouton7.webm"
            }
        ]
    ],
    "utility": "Notons également qu'il est possible de combiner cette technique avec un fruit qui passe en sens opposé. Il faut dans ce cas attendre que le fruit commence à saute pour passer à votre tour : cela s'appelle le sous-mouton.\n Cette technique peut aussi s'étendre à une infinité de cases si Igor possède un minimum de 4 bombes."
}

export default sauteMoutonTechnique;