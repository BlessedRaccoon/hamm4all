import { Technique } from "@hamm4all/shared";
import pathConst from "../../../shared/paths/path.const";

const tiersEtageTechnique : Technique = {
    "name": "Le Tiers-Etage",
    "presentation": "La technique du Tiers-étage est une technique qui existe depuis les débuts du jeu. Elle nécessite d'avoir deux bombes ou plus pour être exécutée.",
    "description": "La technique consiste à se lever en l'air, tirer une bombe au sommet du saut, et rapidement se retourner pour tirer une bombe dans l'autre sens. Les deux impulsions permettent de se sur-élever de deux cases supplémentaires par rapport à un saut normal.",
    "videos": [
        [
            {
                "name": "Tiers-étage",
                "type": "webm",
                "source": "/assets/videos/tiers-etage.webm"
            }
        ]
    ],
    "utility": `Cette technique est particulièrement utile au [8 de la crypte](${pathConst.getLevelPath(31, "54.12.1G.8")}) par exemple, où on peut l'utiliser pour passer d'un côté à l'autre du niveau.<br> Si vous avez une maîtrise de la technique, elle peut être également utilisée au niveau [niveau 33.0.2](${pathConst.getLevelPath(31, "33.0.2")}) avec trois bombes pour prendre les ![Canne](${pathConst.getItemPath(1002)}) cannes de Bobble. Une maîtrise parfaite de la technique peut même permettre de récupérer les cannes avec 2 bombes, comme le montre [maxdefolsch](${pathConst.getEFUser("e6cd6f38-217d-4c0b-90a6-d442a1e6fa2f")}) dans [cette vidéo](https://www.youtube.com/watch?v=v8GXudARQsI).`
}

export default tiersEtageTechnique;