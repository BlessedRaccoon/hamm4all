import { Controller, Get } from "@nestjs/common";
import { TechniquesService } from "./techniques.service";
import { H4A_API } from "@hamm4all/shared";

@Controller({
    version: ["1"]
})
export class TechniquesController {

    constructor(private readonly techniquesService: TechniquesService) {}

    @Get(H4A_API.v1.techniques.getFullRawUri())
    public async getTechnics() {
        return this.techniquesService.findAll();
    }
}