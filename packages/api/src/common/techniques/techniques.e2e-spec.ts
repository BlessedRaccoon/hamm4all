import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Technique } from '@hamm4all/shared/calls/routes/techniques/technique.types';
import { validateObject } from "@hamm4all/shared/tests/tests.helper"
import { ApiModule } from '../../api.module';

describe('Techniques', () => {

    let app: INestApplication;
    
    beforeAll(async () => {
        const testModule = await Test.createTestingModule({
            imports: [ApiModule]
        })
        .compile();

        app = testModule.createNestApplication();
        await app.init();
    })

    it(`/GET techniques`, () => {
        return request(app.getHttpServer())
            .get('/techniques')
            .expect(200)
            .then((res) => {
                for(const technique of res.body) {
                    expect(validateObject(technique, Technique)).toBe(true);
                }
                
            })
    });

})