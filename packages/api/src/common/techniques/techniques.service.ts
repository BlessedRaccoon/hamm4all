import { Injectable } from "@nestjs/common";
import bombeEnDiagonaleTechnique from "./data/bombe-en-diagonale-technique";
import bombPoopingTechnique from "./data/bomb-pooping.technique";
import sautDeDimensionTechnique from "./data/saut-de-dimension.technique";
import sauteMoutonTechnique from "./data/saute-mouton.technique";
import tiersEtageTechnique from "./data/tiers-etage.technique";
import doubleImpulsionTechnique from "./data/double-impulsion.technique";
import techniqueDeKrpoTechnique from "./data/technique-de-krpo.technique";
import { Technique } from "@hamm4all/shared";

@Injectable()
export class TechniquesService {
    private readonly TECHNICS: Array<Technique>;

    constructor() {
        this.TECHNICS = [
            bombeEnDiagonaleTechnique,
            bombPoopingTechnique,
            sautDeDimensionTechnique,
            sauteMoutonTechnique,
            tiersEtageTechnique,
            techniqueDeKrpoTechnique,
            doubleImpulsionTechnique
        ]
    }

    findAll(): Array<Technique> {
        return this.TECHNICS;
    }
}