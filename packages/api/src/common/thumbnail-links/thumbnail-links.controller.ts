import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from "@nestjs/common";
import { ThumbnailSequencesService } from "../thumbnail-sequences/thumbnail-sequences.service";
import { ThumbnailLinksService } from "./thumbnail-links.service";
import { H4A_API, ThumbnailLink, ThumbnailLinkAddBody, ThumbnailLinkParam, ThumbnailLinkUpdateBody, ThumbnailSequenceParam } from "@hamm4all/shared";
import { GamePartsService } from "../game-parts/game-parts.service";
import { AuthGuard } from "../../shared/auth/auth.guard";
import { Request } from "express";

@Controller()
export class ThumbnailLinksController {
    constructor(
        private readonly thumbnailLinksService: ThumbnailLinksService,
        private readonly gamePartsService: GamePartsService,
        private readonly thumbnailSequencesService: ThumbnailSequencesService
    ) {}

    @Get(H4A_API.v1.thumbnailSequences.links.list.getFullRawUri())
    async list(
        @Param() params: ThumbnailSequenceParam
    ): Promise<ThumbnailLink[]> {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        const sequenceID = await this.thumbnailSequencesService.getThumbnailSequenceID(gamePartID, params.sequenceIndex)
        const links = await this.thumbnailLinksService.findAll(sequenceID)
        return links.map(this.thumbnailLinksService.toThumbnailLink)
    }

    @Post(H4A_API.v1.thumbnailSequences.links.add.getFullRawUri())
    @UseGuards(AuthGuard(1))
    async add(
        @Param() params: ThumbnailSequenceParam,
        @Body() body: ThumbnailLinkAddBody,
        @Req() request: Request
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        const sequenceID = await this.thumbnailSequencesService.getThumbnailSequenceID(gamePartID, params.sequenceIndex)
        await this.thumbnailLinksService.create(sequenceID, body, request.user.id)
    }

    @Put(H4A_API.v1.thumbnailSequences.links.update.getFullRawUri())
    @UseGuards(AuthGuard(1))
    async update(
        @Param() params: ThumbnailLinkParam,
        @Body() body: ThumbnailLinkUpdateBody,
        @Req() request: Request
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        const sequenceID = await this.thumbnailSequencesService.getThumbnailSequenceID(gamePartID, params.sequenceIndex)
        await this.thumbnailLinksService.update(sequenceID, params.thumbnailLinkIndex, body, request.user.id)
    }

    @Delete(H4A_API.v1.thumbnailSequences.links.delete.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.thumbnailSequences.links.delete.getRole()))
    async delete(
        @Param() params: ThumbnailLinkParam
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        const sequenceID = await this.thumbnailSequencesService.getThumbnailSequenceID(gamePartID, params.sequenceIndex)
        await this.thumbnailLinksService.delete(sequenceID, params.thumbnailLinkIndex)
    }
}