import { HttpException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { FindOptionsRelations, Repository } from "typeorm";
import { ThumbnailLinkEntity } from "../../database/entities/thumbnails/thumbnail-link.entity";
import { ThumbnailLink, ThumbnailLinkAddBody, ThumbnailLinkUpdateBody } from "@hamm4all/shared";
import { ExperienceService } from "../users/experience/experience.service";

@Injectable()
export class ThumbnailLinksService {
    constructor(
        @InjectRepository(ThumbnailLinkEntity) private readonly thumbnailLinkRepository: Repository<ThumbnailLinkEntity>,
        private readonly experienceService: ExperienceService
    ) {}

    toThumbnailLink(thumbnailLink: ThumbnailLinkEntity): ThumbnailLink {
        return {
            "orderNumber": thumbnailLink.order_num,
            "level": {
                "id": thumbnailLink.level.id,
                "name": thumbnailLink.level.name,
                "mainScreenshotID": thumbnailLink.level.screenshots.find(screenshot => screenshot.order_num === 1)?.id,
            },
            "position": thumbnailLink.coordinates
        }
    }

    private getClassicRelationOptions(): FindOptionsRelations<ThumbnailLinkEntity> {
        return {
            "level": {
                "screenshots": true
            }
        }
    }

    async findAll(thumbnailSequenceID: number): Promise<ThumbnailLinkEntity[]> {
        const thumbnailLinks = await this.thumbnailLinkRepository.find({
            "where": {
                "sequence_id": thumbnailSequenceID
            },
            "relations": this.getClassicRelationOptions(),
            "order": {
                "order_num": "ASC"
            }
        })

        return thumbnailLinks
    }

    async create(thumbnailSequenceID: number, creationData: ThumbnailLinkAddBody, userID: number) {
        const thumbnailLinks = await this.findAll(thumbnailSequenceID)
        const orderNumberToAdd = thumbnailLinks.length + 1

        const thumbnailLink = new ThumbnailLinkEntity()
        thumbnailLink.sequence_id = thumbnailSequenceID
        thumbnailLink.level_id = creationData.levelID
        thumbnailLink.coordinates = creationData.position
        thumbnailLink.order_num = orderNumberToAdd

        await this.thumbnailLinkRepository.save(thumbnailLink)
        await this.experienceService.addExperience(userID, 3)
    }

    async update(thumbnailSequenceID: number, thumbnailLinkIndex: number, updateData: ThumbnailLinkUpdateBody, userID: number) {
        const thumbnailLink = await this.thumbnailLinkRepository.findOneOrFail({
            "where": {
                "sequence_id": thumbnailSequenceID,
                "order_num": thumbnailLinkIndex
            }
        })

        thumbnailLink.level_id = updateData.levelID
        thumbnailLink.coordinates = updateData.position

        await this.thumbnailLinkRepository.save(thumbnailLink)
        await this.experienceService.addExperience(userID, 3)
    }

    async delete(thumbnailSequenceID: number, thumbnailLinkIndex: number) {
        const thumbnailLinks = await this.findAll(thumbnailSequenceID)
        const thumbnailLink = thumbnailLinks.find(link => link.order_num === thumbnailLinkIndex)
        if (!thumbnailLink) {
            throw new HttpException("Thumbnail link not found", 404)
        }

        await this.thumbnailLinkRepository.remove(thumbnailLink)

        // We need to update the order numbers of the other links
        await this.thumbnailLinkRepository.createQueryBuilder()
            .update(ThumbnailLinkEntity)
            .set({ "order_num": () => "order_num - 1" })
            .where("sequence_id = :sequenceID AND order_num > :orderNumber", {
                "sequenceID": thumbnailSequenceID,
                "orderNumber": thumbnailLinkIndex
            })
            .execute()
    }
}