import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from "@nestjs/common";
import { ThumbnailSequencesService } from "./thumbnail-sequences.service";
import { GamePartParam, H4A_API, ThumbnailSequence, ThumbnailSequenceAddBody, ThumbnailSequenceOrderBody, ThumbnailSequenceParam, ThumbnailSequenceUpdateBody } from "@hamm4all/shared";
import { GamePartsService } from "../game-parts/game-parts.service";
import { AuthGuard } from "../../shared/auth/auth.guard";
import { Request } from "express";

@Controller()
export class ThumbnailSequencesController {
    constructor(
        private readonly thumbnailSequencesService: ThumbnailSequencesService,
        private readonly gamePartsService: GamePartsService
    ) {}

    @Get(H4A_API.v1.thumbnailSequences.list.getFullRawUri())
    async list(
        @Param() params: GamePartParam
    ): Promise<ThumbnailSequence[]> {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        const sequences = await this.thumbnailSequencesService.findAll(gamePartID)
        const thumbnailSequences = sequences.map((seq) => this.thumbnailSequencesService.toThumbnailSequence(seq))
        return Promise.all(thumbnailSequences)
    }

    @Post(H4A_API.v1.thumbnailSequences.add.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.thumbnailSequences.add.getRole()))
    async add(
        @Param() params: GamePartParam,
        @Body() body: ThumbnailSequenceAddBody,
        @Req() request: Request
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        await this.thumbnailSequencesService.create(gamePartID, body, request.user.id)
    }

    @Put(H4A_API.v1.thumbnailSequences.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.thumbnailSequences.update.getRole()))
    async update(
        @Param() params: ThumbnailSequenceParam,
        @Body() body: ThumbnailSequenceUpdateBody,
        @Req() request: Request
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        await this.thumbnailSequencesService.update(gamePartID, params.sequenceIndex, body, request.user.id)
    }

    @Delete(H4A_API.v1.thumbnailSequences.delete.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.thumbnailSequences.delete.getRole()))
    async delete(
        @Param() params: ThumbnailSequenceParam
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        await this.thumbnailSequencesService.delete(gamePartID, params.sequenceIndex)
    }

    @Put(H4A_API.v1.thumbnailSequences.order.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.thumbnailSequences.order.update.getRole()))
    async updateOrder(
        @Param() params: GamePartParam,
        @Body() body: ThumbnailSequenceOrderBody
    ) {
        const gamePartID = await this.gamePartsService.getGamePartID(params.gameID, params.gamePartIndex)
        await this.thumbnailSequencesService.changeOrder(gamePartID, body.order)
    }

}