import { Module } from "@nestjs/common";
import { ThumbnailSequencesController } from "./thumbnail-sequences.controller";
import { ThumbnailSequencesService } from "./thumbnail-sequences.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { ThumbnailSequenceEntity } from "../../database/entities/thumbnails/thumbnail-sequence.entity";
import { GamePartsModule } from "../game-parts/game-parts.module";
import { ThumbnailLinkEntity } from "../../database/entities/thumbnails/thumbnail-link.entity";
import { LevelEntity } from "../../database/entities/level/level.entity";

@Module({
    "controllers": [ThumbnailSequencesController],
    "providers": [ThumbnailSequencesService],
    "exports": [ThumbnailSequencesService],
    "imports": [
        TypeOrmModule.forFeature([
            AuthTokenEntity,
            ThumbnailSequenceEntity,
            ThumbnailLinkEntity,
            LevelEntity
        ]),
        GamePartsModule
    ]
})
export class ThumbnailSequencesModule {}