import { HttpException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { ThumbnailSequenceEntity } from "../../database/entities/thumbnails/thumbnail-sequence.entity";
import { ThumbnailSequence, ThumbnailSequenceAddBody, ThumbnailSequenceUpdateBody } from "@hamm4all/shared";
import { FindOptionsRelations, Repository } from "typeorm";
import { ThumbnailLinkEntity } from "../../database/entities/thumbnails/thumbnail-link.entity";
import { LevelEntity } from "../../database/entities/level/level.entity";

@Injectable()
export class ThumbnailSequencesService {
    constructor(
        @InjectRepository(ThumbnailSequenceEntity) private readonly thumbnailSequenceRepository: Repository<ThumbnailSequenceEntity>,
        @InjectRepository(ThumbnailLinkEntity) private readonly thumbnailLinkRepository: Repository<ThumbnailLinkEntity>,
        @InjectRepository(LevelEntity) private readonly levelRepository: Repository<LevelEntity>
    ) {}

    public async toThumbnailSequence(thumbnailSequence: ThumbnailSequenceEntity): Promise<ThumbnailSequence> {
        const name = await this.getSequenceName(thumbnailSequence)
        return {
            "id": thumbnailSequence.id,
            "gamePartID": thumbnailSequence.game_part_id,
            "hasLabyrinth": thumbnailSequence.has_labyrinth,
            "createdAt": thumbnailSequence.created_at.toISOString(),
            "updatedAt": thumbnailSequence.updated_at.toISOString(),
            "updatedBy": {
                "id": thumbnailSequence.updated_by.id,
                "name": thumbnailSequence.updated_by.name
            },
            "isFirstLevelApart": thumbnailSequence.is_dimension,
            "orderNumber": thumbnailSequence.order_num,
            "name": name
        }
    }

    public async findAll(gamePartID: number): Promise<ThumbnailSequenceEntity[]> {
        const thumbnailSequences = await this.thumbnailSequenceRepository.find({
            "where": {
                "game_part_id": gamePartID
            },
            "relations": this.getClassicRelationOptions(),
            "order": {
                "order_num": "ASC"
            }
        })

        return thumbnailSequences
    }

    public async getSequenceName(sequence: ThumbnailSequenceEntity): Promise<string | undefined> {
        const orderNumOfLinkToCheckFor = sequence.is_dimension || sequence.has_labyrinth ? 2 : 1
        const link = await this.thumbnailLinkRepository.findOne({
            "where": {
                "sequence_id": sequence.id,
                "order_num": orderNumOfLinkToCheckFor
            }
        })
        if (!link) {
            return undefined
        }

        const level = await this.levelRepository.findOne({
            "select": {
                "land": {
                    "name": true
                }
            },
            "relations": {
                "land": true
            },
            "where": {
                "id": link.level_id
            }
        })

        return level?.land.name
    }

    private getClassicRelationOptions(): FindOptionsRelations<ThumbnailSequenceEntity> {
        return {
            "updated_by": true
        }
    }

    public async create(gamePartID: number, creationData: ThumbnailSequenceAddBody, userID: number): Promise<ThumbnailSequenceEntity> {

        const thumbnailSequences = await this.findAll(gamePartID)
        const orderNumberToAdd = thumbnailSequences.length + 1

        const thumbnailSequence = new ThumbnailSequenceEntity()
        thumbnailSequence.game_part_id = gamePartID
        thumbnailSequence.is_dimension = creationData.firstLevelApart
        thumbnailSequence.has_labyrinth = creationData.hasLabyrinth
        thumbnailSequence.order_num = orderNumberToAdd
        thumbnailSequence.updated_by_id = userID

        return await this.thumbnailSequenceRepository.save(thumbnailSequence)
    }

    public async update(gamePartID: number, orderNumber: number, updateData: ThumbnailSequenceUpdateBody, userID: number): Promise<void> {
        const thumbnailSequence = await this.thumbnailSequenceRepository.findOne({
            "where": {
                "game_part_id": gamePartID,
                "order_num": orderNumber
            }
        })

        if (!thumbnailSequence) {
            throw new HttpException(`Impossible de trouver la séquence de miniatures avec l'ID de partie de jeu "${gamePartID}" et le numéro d'ordre "${orderNumber}"`, 404)
        }

        thumbnailSequence.is_dimension = updateData.firstLevelApart
        thumbnailSequence.has_labyrinth = updateData.hasLabyrinth
        thumbnailSequence.updated_by_id = userID

        await this.thumbnailSequenceRepository.save(thumbnailSequence)
    }

    public async delete(gamePartID: number, orderNumber: number): Promise<void> {
        await this.thumbnailSequenceRepository.delete({
            "game_part_id": gamePartID,
            "order_num": orderNumber
        })
    }

    public async getThumbnailSequenceID(gamePartID: number, orderNumber: number): Promise<number> {
        const thumbnailSequence = await this.thumbnailSequenceRepository.findOne({
            "where": {
                "game_part_id": gamePartID,
                "order_num": orderNumber
            }
        })

        if (!thumbnailSequence) {
            throw new Error(`Impossible de trouver la séquence de miniatures avec l'ID de partie de jeu "${gamePartID}" et le numéro d'ordre "${orderNumber}"`)
        }

        return thumbnailSequence.id
    }

    public async changeOrder(gamePartID: number, newOrderNumbers: number[]): Promise<void> {
        const thumbnailSequences = await this.findAll(gamePartID)

        // To avoid conflicts with the same order number, we first offset all the order numbers
        const OFFSET = 1_000_000
        for (const thumbnailSequence of thumbnailSequences) {
            thumbnailSequence.order_num += OFFSET
        }
        await this.thumbnailSequenceRepository.save(thumbnailSequences)

        // Then we set the new order numbers
        for (let i = 0; i < thumbnailSequences.length; i++) {
            thumbnailSequences[i].order_num = newOrderNumbers[i]
        }
        await this.thumbnailSequenceRepository.save(thumbnailSequences)
    }
}