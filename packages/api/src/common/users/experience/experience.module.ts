import { Module } from "@nestjs/common";
import { ExperienceService } from "./experience.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "../../../database/entities/users/users.entity";

@Module({
    "controllers": [],
    "providers": [ExperienceService],
    "exports": [ExperienceService],
    "imports": [
        TypeOrmModule.forFeature([UserEntity])
    ]
})
export class ExperienceModule {}