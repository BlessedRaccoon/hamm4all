import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "../../../database/entities/users/users.entity";
import { Repository } from "typeorm";

@Injectable()
export class ExperienceService {
    constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>
    ) {}

    async addExperience(userID: number, experience: number) {
        const user = await this.userRepository.findOneBy({
            "id": userID
        })
        if (! user) {
            throw new Error("Impossible d'ajouter de l'expérience à un utilisateur inexistant")
        }

        user.experience += experience;
        await this.userRepository.save(user)
    }

}