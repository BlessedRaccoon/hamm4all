import { INestApplication } from "@nestjs/common";
import { Repository } from "typeorm";
import { UserEntity } from "../../database/entities/users/users.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import { AuthService } from "../../shared/auth/auth.service";
import { h4AAfterEach, h4ABeforeEach } from "../../test/tests.helper";
import { UsersService } from "./users.service";
import request from "supertest"
import { H4A_API } from "@hamm4all/shared";
import { validateObject } from "@hamm4all/shared/tests/tests.helper";
import { UserProfile } from "@hamm4all/shared/calls/routes/users/users.types";

describe("users", () => {
    let app: INestApplication;
    let usersRepository: Repository<UserEntity>;
    let usersService: UsersService;
    let authService: AuthService;

    beforeEach(async () => {
        const {application, testModule} = await h4ABeforeEach()
        app = application

        usersRepository = testModule.get(getRepositoryToken(UserEntity))
        usersService = testModule.get(UsersService)
        authService = testModule.get(AuthService)
    })

    afterEach(async () => {
        await h4AAfterEach(app)
    })

    it("should return an empty list of users", async () => {

        return request(app.getHttpServer())
            .get(H4A_API.v1.user.list.getFullRawUri())
            .expect(200)
            .then((response) => {
                expect(response.body).toEqual([])
            })
    })

    it("should return a list of users", async () => {
        await usersRepository.save({
            "name": "test",
            "password": Buffer.from("test")
        })

        return request(app.getHttpServer())
            .get(H4A_API.v1.user.list.getFullRawUri())
            .expect(200)
            .then((response) => {
                expect(response.body).toHaveLength(1)
                const user = response.body[0]

                expect(validateObject(user, UserProfile)).toBe(true)
                expect(user.name).toEqual("test")
            })
    })

    it("should not return any user profile", async () => {
        return request(app.getHttpServer())
            .get(H4A_API.v1.user.profile.getParsedUri({"userID": 42}))
            .expect(404)   
    })

    it("should return a user profile", async () => {
        const user = await usersRepository.save({
            "name": "test",
            "password": Buffer.from("test")
        })

        return request(app.getHttpServer())
            .get(H4A_API.v1.user.profile.getParsedUri({"userID": user.id}))
            .expect(200)
            .then((response) => {
                expect(validateObject(response.body, UserProfile)).toBe(true)
                expect(response.body.name).toEqual("test")
            })
            .catch((err) => {
                throw err
            })
    })

})