import { H4A_API, UserParam, USERS_PROFILE_PICTURE_FILE_FIELD_NAME } from "@hamm4all/shared";
import { Controller, FileTypeValidator, Get, MaxFileSizeValidator, Param, ParseFilePipe, Put, Req, UploadedFile, UseGuards, UseInterceptors } from "@nestjs/common";
import { UsersService } from "./users.service";
import { AuthGuard } from "../../shared/auth/auth.guard";
import { Request } from "express";
import { FileInterceptor } from "@nestjs/platform-express";

@Controller({
    "version": ["1"],
})
export class UsersController {

    constructor(
        private readonly usersService: UsersService
    ) {}

    @Get(H4A_API.v1.user.list.getFullRawUri())
    public async list() {
        const users = await this.usersService.findAll();
        return users.map((user) => this.usersService.toPublicUserProfile(user))
    }

    @Get(H4A_API.v1.user.profile.getFullRawUri())
    public async userProfile(
        @Param() {userID}: UserParam
    ) {
        return this.usersService.getUserProfileFromID(userID)
    }

    @Get(H4A_API.v1.user.session.getFullRawUri())
    @UseGuards(AuthGuard(1))
    public async userSession(
        @Req() request: Request
    ) {
        const session = this.usersService.getUserSession(request.user)
        return session
    }

    @Put(H4A_API.v1.user.profilePicture.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.user.profilePicture.update.getRole()))
    @UseInterceptors(FileInterceptor(USERS_PROFILE_PICTURE_FILE_FIELD_NAME))
    async updateProfilePicture(
        @Param() {userID}: UserParam,
        @UploadedFile(
            new ParseFilePipe({
                "validators": [
                    new MaxFileSizeValidator({
                        "maxSize": 1024 * 1024,
                        "message": "Une photo de profil ne doit pas dépasser 1Mo"
                    }),
                    new FileTypeValidator({
                        "fileType": "image/png",
                    })
                ]
            })
        ) profilePicture: Express.Multer.File,
        @Req() request: Request
    ) {
        return this.usersService.updateProfilePicture(userID, profilePicture, request.user)
    }
}