import { Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "../../database/entities/users/users.entity";
import { UsersController } from "./users.controller";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { BlobModule } from "../../shared/blobs/blobs.module";


@Module({
    imports: [
        TypeOrmModule.forFeature([UserEntity, AuthTokenEntity]),
        BlobModule
    ],
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService]
})
export class UsersModule {}