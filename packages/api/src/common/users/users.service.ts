import { ForbiddenException, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "../../database/entities/users/users.entity";
import { Repository } from "typeorm";
import { UserProfile } from "@hamm4all/shared";
import { BlobService } from "../../shared/blobs/blobs.service";
import { H4ARole } from "@hamm4all/shared/dist/calls/routes/api.roles";

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(UserEntity) private readonly usersRepository: Repository<UserEntity>,
        private readonly blobsService: BlobService
    ) {}

    async create(user: {username: string, password: Buffer}) {
        const userEntity = this.usersRepository.create({
            "name": user.username,
            "password": user.password
        });
        return await this.usersRepository.save(userEntity);
    }

    async findAll() {
        return await this.usersRepository.find();
    }

    public toPublicUserProfile(user: UserEntity): UserProfile {
        return {
            id: user.id,
            name: user.name,
            role: user.roleID,
            experience: user.experience,
            createdAt: user.createdAt.toISOString(),
            profilePicture: user.profilePictureID,
            ...this.getExperienceStats(user.experience)
        }
    }

    private getExperienceStats(experience: number) {
        const firstLevelExperience = 10;
        const experienceCoefficient = 1.15;
        const experienceConstant = 5;
        let currentLevel = 1;
        let currentLevelExperience = firstLevelExperience;
        let nextLevelExperience = firstLevelExperience;
        while (experience >= nextLevelExperience) {
            currentLevelExperience = nextLevelExperience;
            nextLevelExperience = Math.floor(
                currentLevelExperience * experienceCoefficient +
                experienceConstant * currentLevel
            );
            currentLevel++;
        }

        return {
            currentLevel: currentLevel,
            experienceToReachNextLevel: nextLevelExperience,
            experienceToReachCurrentLevel: currentLevelExperience
        }
    }

    async findOne(username: string) {
        return this.usersRepository.findOne({
            "where": {name: username}
        });
    }

    async findOneWithPassword(username: string) {
        return this.usersRepository.findOne({
            "where": {name: username},
            "select": {
                "id": true,
                "name": true,
                "password": true,
                "createdAt": true,
                "experience": true,
                "roleID": true,
                "profilePictureID": true,
            }
        });
    }

    async getUserProfileFromID(userID: number) {
        const user = await this.usersRepository.findOneBy({
            "id": userID
        })

        if (! user) {
            throw new NotFoundException(`The user with ID ${userID} has not been found`)
        }

        return this.toPublicUserProfile(user)
    }

    async getUserSession(user: UserEntity) {
        return this.toPublicUserProfile(user);
    }

    changeRole(userEntity: UserEntity, roleID: number) {
        userEntity.roleID = roleID;
        return this.usersRepository.save(userEntity);
    }

    async updateProfilePicture(userID: number, profilePicture: Express.Multer.File, userThatUpdates: UserEntity) {
        const savedBlob = await this.blobsService.add({
            "data": profilePicture.buffer,
            "mimetype": profilePicture.mimetype,
            "size": profilePicture.size
        });

        const user = await this.usersRepository.findOneBy({
            "id": userID
        });
        if (! user) {
            throw new NotFoundException(`L'utilisateur avec l'ID ${userID} n'a pas été trouvé`)
        }
        if (userID !== userThatUpdates.id && userThatUpdates.roleID < H4ARole.ADMINISTRATOR) {
            throw new ForbiddenException("Vous n'avez pas les droits nécessaires pour effectuer cette action");
        }

        user.profilePictureID = savedBlob.id;
        return this.usersRepository.save(user);
    }
}