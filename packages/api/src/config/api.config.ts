import { ModuleMetadata, Provider, ValidationPipe } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { APP_FILTER, APP_PIPE } from "@nestjs/core";
import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { DataSource } from "typeorm";
import { ChangelogModule } from "../common/changelog/changelog.module";
import { ItemsModule } from "../common/items/modules/items.module";
import { TechniquesModule } from "../common/techniques/techniques.module";
import { UsersModule } from "../common/users/users.module";
import { AuthModule } from "../shared/auth/auth.module";
import { BlobModule } from "../shared/blobs/blobs.module";
import { validateEnvironment } from "../shared/environment/environment.validation";
import { PageNotFoundFilter } from "../shared/page-not-found.filter";
import { registeredEntities } from "./entities";
import { FamiliesModule } from "../common/families/families.module";
import { QuestsModule } from "../common/quests/quests.module";
import { GamesModule } from "../common/games/games.module";
import { LandsModule } from "../common/lands/lands.module";
import { LevelsModule } from "../common/levels/levels.module";
import { GamePartsModule } from "../common/game-parts/game-parts.module";
import { LevelTransitionsModule } from "../common/levels/transitions/level-transitions.module";
import { AuthorsModule } from "../common/authors/authors.module";
import { LanguagesModule } from "../shared/languages/languages.module";
import { TestModule } from "../shared/test-endpoints/test.module";
import { LevelDescriptionsModule } from "../common/levels/descriptions/level-descriptions.module";
import { LevelVideosModule } from "../common/levels/videos/level-videos.module";
import { LevelScreenshotsModule } from "../common/levels/screenshots/level-screenshots.module";
import { FruitOrderModule } from "../common/levels/fruit-order/fruit-order.module";
import { BadRequestFilter } from "../shared/validation.filter";
import { ThumbnailLinksModule } from "../common/thumbnail-links/thumbnail-links.module";
import { ThumbnailSequencesModule } from "../common/thumbnail-sequences/thumbnail-sequences.module";
import { ExperienceModule } from "../common/users/experience/experience.module";
import { ThrottlerModule } from "@nestjs/throttler";

const registeredModules = [
    TechniquesModule,
    BlobModule,
    UsersModule,
    AuthModule,
    ChangelogModule,
    ItemsModule,
    FamiliesModule,
    QuestsModule,
    GamesModule,
    LandsModule,
    LevelsModule,
    GamePartsModule,
    LevelTransitionsModule,
    AuthorsModule,
    LanguagesModule,
    TestModule,
    LevelDescriptionsModule,
    LevelVideosModule,
    LevelScreenshotsModule,
    FruitOrderModule,
    ThumbnailLinksModule,
    ThumbnailSequencesModule,
    ExperienceModule
]

const productionDatabaseFactory: (args: any) => TypeOrmModuleOptions = (env: ConfigService) => ({
    type: "postgres",
    host: env.get("DB_HOST"),
    port: env.get("DB_PORT"),
    username: env.get("DB_USER"),
    password: env.get("DB_PASSWORD"),
    database: env.get("DB_NAME"),
    entities: registeredEntities,
    synchronize: false,
})

const testDatabaseFactory: (args: any) => TypeOrmModuleOptions = (env: ConfigService) => ({
    type: "postgres",
    host: env.get("DB_HOST"),
    port: env.get("DB_PORT"),
    username: env.get("DB_USER"),
    password: env.get("DB_PASSWORD"),
    database: env.get("DB_NAME"),
    entities: registeredEntities,
    synchronize: true,
    poolSize: 1,
    schema: "pg_temp"
})

const providers: Provider[] = [
    {
        provide: APP_PIPE,
        useValue: new ValidationPipe({"transform": true}),
    },
    {
        provide: APP_FILTER,
        useClass: PageNotFoundFilter,
    },
    {
        provide: APP_FILTER,
        useClass: BadRequestFilter
    }
]

export const apiModuleConfig: ModuleMetadata = {
    imports: [
        ConfigModule.forRoot({
            validate: validateEnvironment,
            isGlobal: true
        }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: productionDatabaseFactory,
        }),
        ThrottlerModule.forRoot({
            "throttlers": [
                {
                    "ttl": 10_000,
                    "limit": 3,
                    "name": "experience"
                }
            ]
        }),
        ...registeredModules
    ],
    controllers: [],
    providers: providers,
}


export const testApiModuleConfig: ModuleMetadata = {
    imports: [
        ConfigModule.forRoot({
            validate: validateEnvironment,
            isGlobal: true
        }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: testDatabaseFactory,
            dataSourceFactory: async (options) => {
                if (!options) throw new Error("No database options provided (should never happen)");

                const dataSource = await new DataSource(options).initialize();

                if (process.env.NODE_ENV === "test") {
                    for (const metadata of dataSource.entityMetadatasMap) {
                        const tableName = metadata[1].tableName
                        await dataSource.query(`DROP TABLE IF EXISTS pg_temp.${tableName} CASCADE`)
                        await dataSource.query(`CREATE TEMPORARY TABLE IF NOT EXISTS ${tableName} (LIKE ${tableName} INCLUDING ALL)`)
                    }
                }

                return dataSource;
            },
        }),
        ...registeredModules
    ],
    controllers: [],
    providers: providers,
}