import { AuthTokenEntity } from "../database/entities/auth-token.entity";
import { AuthorEntity } from "../database/entities/authors.entity";
import { BlobEntity } from "../database/entities/blobs.entity";
import { ChangelogEntity } from "../database/entities/changelog/changelog.entity";
import { GameGradeEntity } from "../database/entities/game/game-grade.entity";
import { GameLandEntity } from "../database/entities/game/game-lands.entity";
import { GamePartEntity } from "../database/entities/game/game-part.entity";
import { GameEntity } from "../database/entities/game/games.entity";
import { ItemNameEntity } from "../database/entities/items/item-names.entity";
import { ItemTypeEntity } from "../database/entities/items/item-types.entity";
import { ItemEntity } from "../database/entities/items/items.entity";
import { LanguageEntity } from "../database/entities/languages/language.entity";
import { LevelDescriptionEntity } from "../database/entities/level/level-description.entity";
import { LevelOrderEntity } from "../database/entities/level/level-order.entity";
import { LevelScreenshotEntity } from "../database/entities/level/level-screenshot.entity";
import { LevelTransitionEntity, LevelTransitionLabyEntity } from "../database/entities/level/level-transitions.entity";
import { LevelVideoEntity } from "../database/entities/level/level-video.entity";
import { LevelEntity } from "../database/entities/level/level.entity";
import { ThumbnailLinkEntity } from "../database/entities/thumbnails/thumbnail-link.entity";
import { ThumbnailSequenceEntity } from "../database/entities/thumbnails/thumbnail-sequence.entity";
import { UserRoleEntity } from "../database/entities/users/users-role.entity";
import { UserEntity } from "../database/entities/users/users.entity";

export const registeredEntities = [
    GameEntity,
    AuthorEntity,
    UserEntity,
    AuthTokenEntity,
    GameGradeEntity,
    GameLandEntity,
    LevelEntity,
    LevelTransitionEntity,
    ItemEntity,
    BlobEntity,
    ChangelogEntity,
    LanguageEntity,
    LevelScreenshotEntity,
    LevelOrderEntity,
    LevelDescriptionEntity,
    GamePartEntity,
    ThumbnailSequenceEntity,
    ThumbnailLinkEntity,
    LevelVideoEntity,
    ItemNameEntity,
    LevelTransitionLabyEntity,
    UserRoleEntity,
    ItemTypeEntity
]