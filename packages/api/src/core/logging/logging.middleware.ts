import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { APILogger } from '../../shared/logger/api-logger.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    private logger = new APILogger("HTTP");
    private requestCounter = 0;

    use(request: Request, response: Response, next: NextFunction): void {
        const actualCounter = this.requestCounter++;

        const { ip, method, baseUrl } = request;
        response.on("close", () => {
            const { statusCode } = response;

            this.logger.log(`[Response (${actualCounter})] ${method} ${baseUrl}: [${ip}] ${statusCode}`);
        });

        this.logger.log(`[Request (${actualCounter})] ${method} ${baseUrl}: [${ip}]`)
        if (method !== "GET" && method !== "DELETE") {
            this.logRequestBodyAndHidePassword(request)
        }
        
        next();
    }

    logRequestBodyAndHidePassword(request: Request) {
        this.logger.debug(`Body: ${JSON.stringify(this.getBodyWithoutPassword(request.body))}`);
    }

    getBodyWithoutPassword(requestBody: Request["body"]) {
        const bodyWithoutPassword = {...requestBody};
        for (const property of Object.keys(requestBody)) {
            if (/password/i.exec(property)) {
                bodyWithoutPassword[property] = "********";
            }
        }
        return bodyWithoutPassword;
    }
}