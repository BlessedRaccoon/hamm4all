import { Length } from "class-validator";
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { GameEntity } from "./game/games.entity";
//import { GameEntity } from "./games.entity";

export interface AuthorSchema {
    id: number;
    name: string;
}

@Entity({name: "authors"})
export class AuthorEntity implements AuthorSchema {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column("varchar", {nullable: false})
    @Length(1, 50)
    declare name: string;

    @ManyToMany(() => GameEntity)
    @JoinTable({
        name: "game_authors",
        inverseJoinColumn: {
            name: "game_id",
            referencedColumnName: "id",
        },
        joinColumn: {
            name: "authors_id",
            referencedColumnName: "id"
        }
    })
    declare games: GameEntity[];
}