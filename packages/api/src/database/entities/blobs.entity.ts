import { Max, Min } from "class-validator";
import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./users/users.entity";


@Entity({'name': 'blobs'})
export class BlobEntity {

    @PrimaryGeneratedColumn("uuid")
    declare id: string;

    @Column({
        "type": "text",
        "nullable": false,
    })
    @Max(255)
    declare media_type: string;

    @Column({
        "type": "integer",
        "nullable": false,
    })
    @Min(1)
    declare size: number;

    @CreateDateColumn()
    declare created_at: Date;

    @OneToOne(() => UserEntity, user => user.id)
    @JoinColumn({name: "created_by"})
    declare created_by?: UserEntity;
}