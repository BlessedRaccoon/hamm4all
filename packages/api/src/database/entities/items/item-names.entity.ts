import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";
import { ItemEntity } from "./items.entity";
import { LanguageEntity } from "../languages/language.entity";


@Entity({
    "name": "item_names"
})
export class ItemNameEntity {

    @PrimaryColumn("uuid")
    declare item_id: string;

    @ManyToOne(() => ItemEntity, (item) => item.names, {"onDelete": "CASCADE"})
    @JoinColumn({name: "item_id"})
    declare item: ItemEntity;

    @PrimaryColumn("varchar")
    declare language_code: string;

    @ManyToOne(() => LanguageEntity)
    @JoinColumn({name: "language_code"})
    declare language: LanguageEntity;

    @Column("varchar", {nullable: false})
    declare name: string;
}