import { Column, Entity, PrimaryColumn } from "typeorm";


@Entity({
    "name": "item_type",
})
export class ItemTypeEntity {

    @PrimaryColumn()
    declare id: number;

    @Column()
    declare name: string;

}