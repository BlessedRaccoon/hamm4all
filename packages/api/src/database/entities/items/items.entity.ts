import { Min } from "class-validator";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { GameEntity } from "../game/games.entity";
import { BlobEntity } from "../blobs.entity";
import { ItemNameEntity } from "./item-names.entity";

export type ItemType = 1 | 2

@Entity({
    "name": "items",
})
export class ItemEntity {

    @PrimaryGeneratedColumn("uuid")
    declare id: string;

    @Column({"type": "integer"})
    declare in_game_id: number;

    @OneToMany(() => ItemNameEntity, (name) => name.item, {"cascade": true})
    declare names: ItemNameEntity[];

    @Column({"length": 1024,"nullable": false})
    declare effect?: string;

    @Column({"nullable": false})
    @Min(1)
    declare coefficient: number;

    @Column({"nullable": false})
    declare type: ItemType;

    @Column({"nullable": true, "type": "integer"})
    declare value: number;

    @ManyToOne(() => BlobEntity)
    @JoinColumn({name: "picture_id"})
    declare picture: BlobEntity;

    @Column({"nullable": false})
    declare picture_id: string;

    @Column({"nullable": false})
    declare game_id: number;

    @ManyToOne(() => GameEntity, (game) => game.items)
    @JoinColumn({name: "game_id"})
    declare game: GameEntity;
}