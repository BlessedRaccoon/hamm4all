import { Length } from "class-validator";
import { Entity, PrimaryColumn } from "typeorm";


@Entity({
    "name": "languages"
})
export class LanguageEntity {

    @PrimaryColumn()
    @Length(2, 5)
    declare code: string
}