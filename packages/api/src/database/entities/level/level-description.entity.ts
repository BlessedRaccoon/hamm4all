import { Length } from "class-validator";
import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { LevelEntity } from "./level.entity";
import { UserEntity } from "../users/users.entity";


@Entity({
    "name": "descriptions"
})
export class LevelDescriptionEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column({"type": "varchar", "nullable": false})
    @Length(10, 8192)
    declare content: string;

    @Column()
    declare level_id: number;

    @ManyToOne(() => LevelEntity, (level) => level.description)
    @JoinColumn({ "name": "level_id" })
    declare level: LevelEntity[];

    @UpdateDateColumn()
    declare updated_at: Date;

    @OneToOne(() => UserEntity)
    @JoinColumn({ "name": "updated_by" })
    declare updated_by: UserEntity;

    @Column({ "name": "updated_by", "nullable": false })
    declare updated_by_id: number;

    
}