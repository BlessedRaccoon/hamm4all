import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { LevelScreenshotEntity } from "./level-screenshot.entity";
import { UserEntity } from "../users/users.entity";

@Entity({
    "name": "orders"
})
export class LevelOrderEntity {
    
    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column("integer", {nullable: false})
    declare order_num: number;

    @Column("integer", {nullable: false})
    declare x: number;

    @Column("integer", {nullable: false})
    declare y: number;

    @Column({"type": "uuid", "nullable": false})
    declare screenshot_id: string;

    @ManyToOne(() => LevelScreenshotEntity)
    @JoinColumn({name: "screenshot_id"})
    declare screenshot: LevelScreenshotEntity;

    @ManyToOne(() => UserEntity)
    @JoinColumn({name: "updated_by"})
    declare updated_by: UserEntity;

    @Column({name: "updated_by", type: "integer", nullable: false})
    declare updated_by_id: number;

    @UpdateDateColumn()
    declare updated_at: Date;

}