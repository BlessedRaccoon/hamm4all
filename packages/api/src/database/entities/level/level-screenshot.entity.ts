import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn } from "typeorm";
import { LevelEntity } from "./level.entity";
import { BlobEntity } from "../blobs.entity";
import { LevelOrderEntity } from "./level-order.entity";
import { UserEntity } from "../users/users.entity";


@Entity({
    "name": "levels_screenshots",
    "orderBy": {
        "order_num": "ASC"
    }
})
export class LevelScreenshotEntity {
    @PrimaryColumn({"onUpdate": "CASCADE"})
    declare id: string;

    @Column()
    declare level_id: number;

    @ManyToOne(() => LevelEntity)
    @JoinColumn({ "name": "level_id" })
    declare level: LevelEntity;

    @OneToOne(() => BlobEntity)
    @JoinColumn({ "name": "id" })
    declare screenshot: BlobEntity;

    @Column({"type": "integer", "nullable": false})
    declare order_num: number;

    @OneToMany(() => LevelOrderEntity, (order) => order.screenshot)
    declare orders: LevelOrderEntity[];

    @CreateDateColumn()
    declare created_at: Date;

    @Column({"type": "integer", "nullable": false, "name": "created_by"})
    declare created_by_id: number;

    @ManyToOne(() => UserEntity)
    @JoinColumn({ "name": "created_by" })
    declare created_by: UserEntity;
}