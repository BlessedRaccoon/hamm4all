import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { LevelEntity } from "./level.entity";
import { LevelTransitionClassicType } from "@hamm4all/shared";

@Entity({ 'name': "level_transitions" })
export class LevelTransitionEntity {

    @PrimaryGeneratedColumn("uuid")
    declare id: string;

    @Column({
        "type": "enum",
        "enum": ["simple", "dimension"],
        "nullable": false
    })
    declare type: LevelTransitionClassicType;

    @Column({
        "type": "integer",
        "nullable": false,
    })
    declare source_level_id: number;

    @Column({
        "type": "integer",
        "nullable": false,
    })
    declare target_level_id: number;

    @ManyToOne(() => LevelEntity, {"orphanedRowAction": "delete"})
    @JoinColumn({ "name": "source_level_id" })
    declare source: LevelEntity;

    @ManyToOne(() => LevelEntity, {"orphanedRowAction": "delete"})
    @JoinColumn({ "name": "target_level_id" })
    declare target: LevelEntity;
}

@Entity({ 'name': "level_transitions_laby" })
export class LevelTransitionLabyEntity {

    @PrimaryGeneratedColumn("uuid")
    declare id: string;

    @Column({
        "type": "enum",
        "enum": ["up", "right"],
        "nullable": false
    })
    declare type: "up" | "right";

    @Column({
        "type": "integer",
        "nullable": false,
    })
    declare source_level_id: number;

    @Column({
        "type": "integer",
        "nullable": false,
    })
    declare target_level_id: number;

    @ManyToOne(() => LevelEntity, {"orphanedRowAction": "delete"})
    @JoinColumn({ "name": "source_level_id" })
    declare source: LevelEntity;

    @ManyToOne(() => LevelEntity, {"orphanedRowAction": "delete"})
    @JoinColumn({ "name": "target_level_id" })
    declare target: LevelEntity;
}