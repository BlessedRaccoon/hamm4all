import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { GameLandEntity } from "../game/game-lands.entity";
import { LevelTransitionEntity, LevelTransitionLabyEntity } from "./level-transitions.entity";
import { UserEntity } from "../users/users.entity";
import { LevelDescriptionEntity } from "./level-description.entity";
import { ThumbnailLinkEntity } from "../thumbnails/thumbnail-link.entity";
import { LevelVideoEntity } from "./level-video.entity";
import { LevelScreenshotEntity } from "./level-screenshot.entity";

@Entity({
    "name": "levels",
})
export class LevelEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column({ "type": "varchar", "length": 255, "nullable": false })
    declare name: string;

    @ManyToOne(() => GameLandEntity)
    @JoinColumn({ "name": "land_id" })
    declare land: GameLandEntity;

    @Column({ "name": "land_id", "nullable": false })
    declare land_id: number;

    @OneToMany(() => LevelTransitionEntity, (transition) => transition.target, {"cascade": true})
    declare previous_levels: LevelTransitionEntity[];

    @OneToMany(() => LevelTransitionEntity, (transition) => transition.source, {"cascade": true})
    declare next_levels: LevelTransitionEntity[];

    @OneToMany(() => LevelTransitionLabyEntity, (transition) => transition.target, {"cascade": true})
    declare previous_laby_levels: LevelTransitionLabyEntity[];

    @OneToMany(() => LevelTransitionLabyEntity, (transition) => transition.source, {"cascade": true})
    declare next_laby_levels: LevelTransitionLabyEntity[];

    @CreateDateColumn({ "name": "created_at" })
    declare created_at: Date;

    @UpdateDateColumn({ "name": "updated_at" })
    declare updated_at: Date;

    @OneToOne(() => UserEntity)
    @JoinColumn({ "name": "updated_by" })
    declare updated_by: UserEntity;

    @Column({ "name": "updated_by", "nullable": false })
    declare updated_by_id: number;

    @OneToMany(() => LevelDescriptionEntity, (description) => description.level)
    declare description: LevelDescriptionEntity[];

    @OneToMany(() => ThumbnailLinkEntity, (link) => link.level)
    declare thumbnail_links: ThumbnailLinkEntity[];

    @OneToOne(() => LevelVideoEntity, (video) => video.level)
    declare video: LevelVideoEntity;

    @OneToMany(() => LevelScreenshotEntity, screen => screen.level)
    declare screenshots: LevelScreenshotEntity[]
}