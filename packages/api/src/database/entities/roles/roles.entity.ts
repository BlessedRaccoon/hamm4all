import { EntitySchema } from "typeorm";
import { UserEntity } from "../users/users.entity";

export type RoleID = 1 | 2 | 3 | 4 | 5;
export enum UserRole {
    USER = 1,
    VIP = 2,
    MODERATOR = 3,
    ADMIN = 4,
    OWNER = 5
}

interface Role {
    id: number;
    name: string;
    users: UserEntity[];
}

export const RoleEntity = new EntitySchema<Role>({
    "name": "roles",
    "columns": {
        "id": {
            "type": Number,
            "primary": true,
            "generated": true
        },
        "name": {
            "type": String,
            "length": 50,
            "nullable": false,
            "unique": true
        }
    },
    "relations": {
        "users": {
            "type": "one-to-many",
            "target": UserEntity,
            "inverseSide": "user_id"
        }
    },

    "checks": [
        {
            "name": "check_role_name_length",
            "expression": `length("name") > 3 AND length("name") < 50`
        }
    ]
});
