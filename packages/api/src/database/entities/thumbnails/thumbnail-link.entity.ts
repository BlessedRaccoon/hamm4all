import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, ValueTransformer } from "typeorm";
import { ThumbnailSequenceEntity } from "./thumbnail-sequence.entity";
import { LevelEntity } from "../level/level.entity";


interface Coordinates {
    x: number;
    y: number;
}

export class CoordinatesTransformer implements ValueTransformer {
    to(coordinate: Coordinates): string {
        if (!coordinate) {
            return null!
        }
        return `(${coordinate.x},${coordinate.y})`;
    }

    from(value: string): Coordinates {
        if (value === null) {
            return null!
        }

        const splitValue = value.slice(1, value.length - 1).split(",")
        const [x, y] = splitValue.map(v => parseInt(v))
        return {
            "x": x,
            "y": y
        }
    }
}

@Entity({
    name: "thumbnail_links"
})
export class ThumbnailLinkEntity {

    @PrimaryColumn({type: "integer"})
    declare sequence_id: number;

    @ManyToOne(() => ThumbnailSequenceEntity)
    @JoinColumn({name: "sequence_id"})
    declare sequence: ThumbnailSequenceEntity;

    @PrimaryColumn({type: "integer"})
    declare order_num: number;

    @Column({type: "integer", nullable: false})
    declare level_id: number;

    @ManyToOne(() => LevelEntity)
    @JoinColumn({name: "level_id"})
    declare level: LevelEntity;

    @Column({"nullable": false, "transformer": new CoordinatesTransformer(), "type": "varchar", "length": 255})
    declare coordinates?: Coordinates;
}