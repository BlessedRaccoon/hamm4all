import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { GamePartEntity } from "../game/game-part.entity";
import { UserEntity } from "../users/users.entity";
import { ThumbnailLinkEntity } from "./thumbnail-link.entity";


@Entity({
    "name": "thumbnail_sequences"
})
export class ThumbnailSequenceEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column({type: "integer", nullable: false})
    declare order_num: number;

    @Column({type: "integer", nullable: false})
    declare game_part_id: number;

    @ManyToOne(() => GamePartEntity)
    @JoinColumn({name: "game_part_id"})
    declare game_part: GamePartEntity;

    @CreateDateColumn()
    declare created_at: Date;

    @UpdateDateColumn()
    declare updated_at: Date;

    @Column({type: "integer", nullable: false, "name": "updated_by"})
    declare updated_by_id: number;

    @ManyToOne(() => UserEntity)
    @JoinColumn({name: "updated_by"})
    declare updated_by: UserEntity

    @Column()
    declare is_dimension: boolean;

    @OneToMany(() => ThumbnailLinkEntity, (link) => link.sequence)
    declare links: ThumbnailLinkEntity[];

    @Column()
    declare has_labyrinth: boolean;
}