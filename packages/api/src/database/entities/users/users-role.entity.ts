import { Length } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "roles"})
export class UserRoleEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column("varchar", {nullable: false, unique: true})
    @Length(1, 30)
    declare name: string;
}