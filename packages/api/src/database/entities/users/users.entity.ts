import { Length, Max, Min } from "class-validator";
import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { AuthTokenEntity } from "../auth-token.entity";
import { BlobEntity } from "../blobs.entity";
import { UserRoleEntity } from "./users-role.entity";

@Entity({name: "users"})
export class UserEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column("varchar", {nullable: false, unique: true})
    @Length(1, 30)
    declare name: string;

    @Column("bytea", {nullable: false, select: false})
    declare password: Buffer;

    @Column("integer", {nullable: false, name: "role", default: 1})
    @Max(5)
    @Min(1)
    declare roleID: number;

    @OneToOne(() => UserRoleEntity)
    @JoinColumn({name: "role"})
    declare role: UserRoleEntity;

    @CreateDateColumn({ "name": "created_at" })
    declare createdAt: Date;

    @Column("integer", {nullable: false, default: "0"})
    @Min(0)
    declare experience: number;

    @OneToOne(() => AuthTokenEntity)
    declare authToken: AuthTokenEntity;

    @Column("uuid", {"name": "profile_picture_id", nullable: true, default: null})
    declare profilePictureID?: string

    @JoinColumn({name: "profile_picture_id"})
    declare profilePicture?: BlobEntity;

}