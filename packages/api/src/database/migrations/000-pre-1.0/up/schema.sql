CREATE TABLE Roles ( -- CHECK !
    id SERIAL,
    nom varchar NOT NULL,
    primary key (id)
);

CREATE TABLE Experience_Levels ( -- CHECK !
    id SERIAL,
    experience integer NOT NULL,
    primary key (id)
);

CREATE TABLE Users (
    id SERIAL,
    name varchar NOT NULL UNIQUE,
    password bytea NOT NULL,
    role integer NOT NULL DEFAULT 1,
    created_at timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    experience integer NOT NULL DEFAULT 0,
    primary key (id),
    foreign key (role) REFERENCES Roles(id)
);

CREATE TABLE Lands ( -- CHECK !
    id SERIAL,
    name varchar NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    description varchar,
    difficulty real CHECK (difficulty >= 0 AND difficulty <= 10),
    primary key (id)
);

CREATE TABLE Authors ( -- CHECK !
    id SERIAL,
    name varchar NOT NULL,
    primary key (id)
);

CREATE TABLE Land_Authors ( -- CHECK !
    id_author integer NOT NULL,
    id_land integer NOT NULL,
    primary key (id_author, id_land)
);

CREATE TABLE Region (
    id SERIAL,
    id_land integer references lands(id),
    name varchar NOT NULL,
    primary key (id)
);

CREATE TABLE Levels ( -- CHECK !
    id SERIAL,
    land_id integer NOT NULL,
    name varchar NOT NULL,
    id_region integer references Region(id),
    description varchar,
    previous_id integer,
    next_id integer,
    primary key (id),
    foreign key (land_id) references Lands(id),
    foreign key (previous_id) references Levels(id),
    foreign key (next_id) references Levels(id) 
);

CREATE TABLE Dimension ( -- CHECK !
    id_base_level integer NOT NULL,
    id_destination_level integer NOT NULL,
    name varchar,
    facing varchar,
    primary key (id_base_level, id_destination_level),
    foreign key (id_base_level) REFERENCES Levels(id),
    foreign key (id_destination_level) REFERENCES Levels(id)
);

CREATE TABLE Land_Part ( -- CHECK !
    id SERIAL,
    name varchar NOT NULL,
    id_land integer references Lands(id) NOT NULL,
    difficulty real CHECK (difficulty >= 0 AND difficulty <= 10),
    description varchar,
    primary key (id)
);

CREATE TABLE Thumbnail_Sequence ( -- CHECK !
    id SERIAL,
    order_num SERIAL,
    id_start integer NOT NULL references Levels(id),
    id_land_part integer NOT NULL references Land_Part(id),
    primary key (id)
);

CREATE TABLE Thumbnail_Link ( -- CHECK !
    id_seq SERIAL references Thumbnail_Sequence(id),
    order_num SERIAL,
    id_level integer NOT NULL references Levels(id),
    primary key (id_seq, order_num)
);

CREATE TABLE Thumbnail_Link_Laby ( -- CHECK !
    x integer NOT NULL,
    y integer NOT NULL
) INHERITS (Thumbnail_Link);

CREATE TABLE Changelog (
    name varchar NOT NULL,
    date DATE DEFAULT CURRENT_DATE,
    changes varchar[] NOT NULL,
    primary key (name)
);

DROP TABLE Experience_Levels;

CREATE TABLE Fruits (
    id SERIAL,
    name VARCHAR NOT NULL,
    image varchar, 
    primary key (id)
);

CREATE TABLE Orders (
    id SERIAL,
    id_level integer NOT NULL REFERENCES Levels(id),
    id_fruit integer NOT NULL REFERENCES Fruits(id),
    order_num integer DEFAULT 1 CHECK (order_num > 0),
    x real NOT NULL CHECK (x >= 0 AND x < 20),
    y real NOT NULL CHECK (y >= 0 AND y < 25),
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    modified_by integer REFERENCES Users(id),
    primary key (id)
);

CREATE TABLE Descriptions (
    id SERIAL,
    id_level integer NOT NULL REFERENCES Levels(id),
    content varchar,
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    modified_by integer REFERENCES Users(id),
    primary key (id)
);

CREATE TABLE videos (
    id SERIAL,
    link varchar NOT NULL,
    has_order boolean NOT NULL DEFAULT FALSE,
    id_level integer NOT NULL REFERENCES Levels(id),
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    modified_by integer REFERENCES Users(id),
    primary key (id)
);

ALTER TABLE Levels
    DROP COLUMN description;

ALTER TABLE Levels
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_by integer REFERENCES Users(id);

UPDATE thumbnail_sequence SET id_start = 1598 WHERE id = 113;
DELETE FROM Thumbnail_sequence WHERE id = 117;

ALTER TABLE Lands
    ADD COLUMN modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_by integer REFERENCES Users(id);

ALTER TABLE Land_Part
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_by integer REFERENCES Users(id);

ALTER TABLE Thumbnail_Sequence
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_by integer REFERENCES Users(id);

UPDATE Lands SET difficulty = NULL;

ALTER TABLE Lands
    DROP COLUMN difficulty;

ALTER TABLE Lands
    ADD COLUMN has_difficulty boolean DEFAULT True;

UPDATE Lands SET has_difficulty = True;

CREATE TABLE Land_Graduation (
    id_land integer,
    id_user integer,
    grade real CHECK (grade >= 0 AND grade <= 10),
    primary key (id_land, id_user)
);

CREATE TYPE coords AS (
    x INTEGER,
    y INTEGER
);

ALTER TABLE Thumbnail_Link
    ADD COLUMN coordinates coords;

CREATE TABLE temp AS SELECT id_seq, order_num, id_level, x, y FROM Thumbnail_Link_Laby;
TRUNCATE TABLE Thumbnail_Link_Laby;

INSERT INTO Thumbnail_Link
    SELECT id_seq, order_num, id_level, ROW(x, y)::coords FROM temp;

DROP TABLE Thumbnail_Link_Laby;
TRUNCATE TABLE temp;
DROP TABLE temp;

ALTER TABLE Lands
    RENAME COLUMN has_difficulty TO has_variable_difficulty;

UPDATE Lands
    SET has_variable_difficulty = NOT has_variable_difficulty;

ALTER TABLE Lands
    ALTER COLUMN has_variable_difficulty SET DEFAULT FALSE;

ALTER TABLE Changelog
    ALTER COLUMN date TYPE Timestamp with time zone;

----------------------------------

ALTER TABLE Lands
    RENAME COLUMN created_at TO released_at;


CREATE EXTENSION "uuid-ossp";

CREATE TABLE Item_Type (
    id INTEGER,
    meaning VARCHAR(255) NOT NULL CHECK (meaning IN ('effect', 'points')),
    PRIMARY KEY (id)
);

INSERT INTO Item_Type (id, meaning) VALUES (1, 'effect');
INSERT INTO Item_Type (id, meaning) VALUES (2, 'points');

CREATE TABLE Blobs (
    id UUID DEFAULT uuid_generate_v4(),
    media_type VARCHAR(255) NOT NULL,
    size INTEGER NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Custom_Items (
    id UUID DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    type INTEGER NOT NULL,
    coefficient INTEGER NOT NULL CHECK (coefficient >= 0),
    land_id INTEGER NOT NULL,
    image_id UUID NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (type) REFERENCES Item_Type(id),
    FOREIGN KEY (land_id) REFERENCES Lands(id),
    FOREIGN KEY (image_id) REFERENCES Blobs(id)
);

INSERT INTO Roles VALUES (1, 'guest');
INSERT INTO Roles VALUES (2, 'vip');
INSERT INTO Roles VALUES (3, 'moderator');
INSERT INTO Roles VALUES (4, 'administrator');
INSERT INTO Roles VALUES (5, 'owner');