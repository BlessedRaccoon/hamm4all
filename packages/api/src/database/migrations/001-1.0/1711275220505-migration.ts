import { MigrationInterface, QueryRunner } from "typeorm";
import fs from "fs"
import { BLOBS_DIRECTORY } from "../../../shared/filesystem/paths";
import path from "path";
import { NESTED_DIRECTORIES_AMOUNT, CHARACTERS_PER_PATH } from "../../../shared/blobs/blobs.service";
import * as uuid from "uuid";
import Hashids from "hashids";

export class Migration1711275220505 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await this.readAndExecuteSqlFile(queryRunner, "000")
        await this.readAndExecuteSqlFile(queryRunner, "001")
        await this.readAndExecuteSqlFile(queryRunner, "002")

        await this.convertScreens(queryRunner)

        await this.readAndExecuteSqlFile(queryRunner, "003")

        const itemsJSONFile = fs.readFileSync(path.join(__dirname, "items.json"), "utf8")
        const {items} = JSON.parse(itemsJSONFile)
        for (const item of items) {
            const value = parseInt(item.value)
            const picture = fs.readFileSync(path.join(__dirname, "../../../../../../../hamm4all-db/assets/items", item.id + ".svg"))
            const picture_id = uuid.v4();
            await this.addFileToDatabase(queryRunner, {id: picture_id, mimetype: "image/svg+xml", size: picture.length})
            await this.store(picture_id, picture)

            const itemID = uuid.v4()
            await queryRunner.query(`INSERT INTO Items (id, game_id, coefficient, in_game_id, effect, value, type, picture_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, [itemID, 31, item.rarity, item.id, item.effect, isNaN(value) ? 0 : value, isNaN(value) ? 1 : 2, picture_id])
            for(const language of Object.keys(item.name)) {
                await queryRunner.query(`INSERT INTO Item_Names (item_id, language_code, name) VALUES ($1, $2, $3)`, [itemID, language, item.name[language]])
            }
        }

        const usersThumbnailsDirectory = path.join(__dirname, "../../../../../../../hamm4all-db/users")
        const usersDirectories = fs.readdirSync(usersThumbnailsDirectory)
        for (const userDirectory of usersDirectories) {
            const userThumbnail = fs.readFileSync(path.join(usersThumbnailsDirectory, userDirectory, "image.png"))
            const userThumbnailID = uuid.v4()
            await this.addFileToDatabase(queryRunner, {id: userThumbnailID, mimetype: "image/png", size: userThumbnail.length})
            await this.store(userThumbnailID, userThumbnail)
            await queryRunner.query(`UPDATE Users SET profile_picture_id = $1 WHERE id = $2`, [userThumbnailID, parseInt(userDirectory)])
        }
    }

    async convertScreens(queryRunner: QueryRunner) {
        const directories = fs.readdirSync(path.join(__dirname, "../../../../../../../hamm4all-db/lands"))
        for (const directory of directories) {
            const files = fs.readdirSync(path.join(__dirname, "../../../../../../../hamm4all-db/lands", directory))
            for (const fileNameWithExtension of files) {
                if (fileNameWithExtension === "min") continue;
                if (fileNameWithExtension === "image.png") {
                    const fileContent = fs.readFileSync(path.join(__dirname, "../../../../../../../hamm4all-db/lands/", directory, fileNameWithExtension))
                    const id = uuid.v4();
                    await this.addFileToDatabase(queryRunner, {id, mimetype: "image/png", size: fileContent.length})
                    await this.store(id, fileContent)
                    await queryRunner.query(`UPDATE Games SET thumbnail_id = $1 WHERE id = $2`, [id, parseInt(directory)])
                }

                if (fs.lstatSync(path.join(__dirname, "../../../../../../../hamm4all-db/lands", directory, fileNameWithExtension)).isDirectory()) continue;
                const fileName = this.removeExtension(fileNameWithExtension)
                const [levelName, extensionID] = fileName.split("_")
                const fileContent = fs.readFileSync(path.join(__dirname, "../../../../../../../hamm4all-db/lands/", directory, fileNameWithExtension))
                const id = uuid.v4();
                await this.addFileToDatabase(queryRunner, {id, mimetype: "image/png", size: fileContent.length})
                await this.store(id, fileContent)
                let order_num = 1;
                if (extensionID) {
                    let failSafe = 0;
                    const hashid = new Hashids(directory + levelName, 6)
                    console.log(extensionID, hashid.encode(1), hashid.encode(2))
                    while (hashid.encode(order_num) !== extensionID && failSafe < 500) {
                        order_num++;
                        failSafe++;
                    }
                    order_num++;
                }

                const level = await queryRunner.query(`SELECT levels.id, levels.modified_at, levels.modified_by, levels.created_at FROM Levels INNER JOIN Game_Lands ON levels.land_id = game_lands.id WHERE game_id = $1 and levels.name = $2`, [parseInt(directory), levelName])
                if ((!level) || Object.keys(level).length === 0 || level.length === 0) {
                    console.log(`Level ${levelName} not found in game ${directory}`)
                    continue;
                }

                await queryRunner.query(`INSERT INTO Levels_Screenshots (level_id, id, order_num, created_at, created_by) VALUES ($1, $2, $3, $4, $5)`, [level[0].id, id, order_num, level[0].created_at, level[0].modified_by])
            }
        }
    }

    /**
     * Removes the very last extension from a file name
     */
    removeExtension(fileName: string): string {
        return fileName.split(".").slice(0, -1).join(".")
    }

    async addFileToDatabase(queryRunner: QueryRunner, file: {id: string, mimetype: string, size: number, uploaderID?: number}) {
        await queryRunner.query(`INSERT INTO blobs (id, media_type, size, created_by) VALUES ($1, $2, $3, $4)`, [file.id, file.mimetype, file.size, file.uploaderID])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        console.log(queryRunner.isTransactionActive)
    }

    async readAndExecuteSqlFile(queryRunner: QueryRunner, name: string) {
        const fileContent = fs.readFileSync(`${__dirname}/up/${name}.sql`, "utf8")
        const queries = fileContent.split(";")
        for (const query of queries) {
            if (query.trim() !== "") {
                await queryRunner.query(query)
            }
        }
    }

    /**
     * Saves file to file system
     * @param id The id of the file
     * @param file The file to save
     * @returns The path to which the file was stored
     */
    private async store(id: string, file: Buffer | string): Promise<string> {
        const pathToFile = this.getPathToFile(id);

        if (!fs.existsSync(path.dirname(pathToFile))) {
            fs.mkdirSync(path.dirname(pathToFile), { recursive: true });
        }

        fs.writeFileSync(pathToFile, file);

        return pathToFile;
    }

    getPathToFile(fileNameUUID: string): string {
        let pathToFile = BLOBS_DIRECTORY;

        for (let nest = 0; nest < NESTED_DIRECTORIES_AMOUNT; nest++) {
            const startingRelevantLettersIndex = nest * CHARACTERS_PER_PATH;
            const relevantLetters = fileNameUUID.slice(
                startingRelevantLettersIndex,
                startingRelevantLettersIndex + CHARACTERS_PER_PATH,
            );
            pathToFile = path.join(pathToFile, relevantLetters);
        }

        return path.join(pathToFile, fileNameUUID);
    }
}
