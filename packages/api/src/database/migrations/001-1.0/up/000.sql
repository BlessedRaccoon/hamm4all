ALTER TABLE Levels DROP COLUMN previous_id;

CREATE TYPE Level_Transition_Type AS ENUM ('simple', 'dimension', 'up', 'right', 'left', 'down');

CREATE TABLE Level_Transitions (
    id UUID DEFAULT uuid_generate_v4(),
    source_level_id INTEGER NOT NULL,
    target_level_id INTEGER NOT NULL,
    type Level_Transition_Type NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(source_level_id) REFERENCES Levels(id),
    FOREIGN KEY(target_level_id) REFERENCES Levels(id)
);

ALTER TABLE Levels DROP COLUMN id_region;
DROP TABLE Region;

ALTER TABLE Lands
    RENAME TO Games;

ALTER TABLE Games
    ADD COLUMN thumbnail_id UUID;

ALTER TABLE Games
	ADD FOREIGN KEY (thumbnail_id) REFERENCES Blobs(id);

CREATE TABLE Game_Lands (
    id SERIAL,
    name TEXT NOT NULL,
    description TEXT,
    game_id INTEGER NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(game_id) REFERENCES Games(id)
);
INSERT INTO Game_lands (id, name, description, game_id)
    SELECT id, 'Sans nom', NULL, id FROM Games;
ALTER SEQUENCE Game_Lands_id_seq RESTART WITH 80; -- I shouldn't have to do this, but weird bugs happen if I don't...

ALTER TABLE Levels ADD COLUMN id_land INTEGER;
ALTER TABLE Levels ADD FOREIGN KEY(id_land) REFERENCES Game_Lands(id);
UPDATE Levels
    SET id_land = (SELECT id FROM Game_Lands WHERE Game_Lands.game_id = Levels.land_id LIMIT 1);
ALTER TABLE Levels ALTER COLUMN id_land SET NOT NULL;

INSERT INTO Level_Transitions
(source_level_id, target_level_id, type)
SELECT id as source_level_id, next_id as target_level_id, 'simple' FROM Levels WHERE next_id IS NOT NULL;

INSERT INTO Level_Transitions
(source_level_id, target_level_id, type)
SELECT id_base_level as source_level_id, id_destination_level as target_level_id, COALESCE(facing, 'dimension')::Level_Transition_Type as type FROM Dimension;