ALTER TABLE Users
    ADD COLUMN profile_picture_id UUID REFERENCES Blobs(id);

ALTER TABLE Land_Graduation
    RENAME TO Game_Graduation;

ALTER TABLE Orders
    DROP COLUMN id_fruit;

DROP TABLE Fruits;

CREATE TABLE Languages (
    code VARCHAR(5) PRIMARY KEY
);

-- IL MANQUE ENCORE "VALUE" DANS ITEMS, ET DANS LA CONVERSION
CREATE TABLE Items (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    in_game_id INTEGER,
    coefficient INTEGER NOT NULL,
    game_id INTEGER NOT NULL REFERENCES Games(id),
    description TEXT,
    type INTEGER NOT NULL REFERENCES Item_Type(id),
    picture_id UUID REFERENCES Blobs(id)
);

ALTER TABLE Item_Type
    RENAME COLUMN meaning TO type;

INSERT INTO Languages (code)
    VALUES ('fr'), ('en'), ('es');

CREATE TABLE Item_Names (
    item_id UUID NOT NULL REFERENCES Items(id),
    language_code VARCHAR(5) NOT NULL REFERENCES Languages(code),
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (item_id, language_code)
);

INSERT INTO Items (id, coefficient, description, type, picture_id, game_id)
    SELECT id, coefficient, description, type, image_id, land_id FROM Custom_Items;
INSERT INTO Item_Names (item_id, language_code, name)
    SELECT id, 'fr', name FROM Custom_Items;

DROP TABLE Custom_Items;

----------------------------------
CREATE TABLE Levels_Screenshots (
    id UUID PRIMARY KEY,
    level_id INTEGER NOT NULL,
    order_num INTEGER NOT NULL,
    FOREIGN KEY(level_id) REFERENCES Levels(id),
    FOREIGN KEY(id) REFERENCES Blobs(id),
    UNIQUE(level_id, order_num)
);

ALTER TABLE Blobs
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW();
ALTER TABLE Blobs
    ADD COLUMN created_by INTEGER REFERENCES Users(id);

ALTER TABLE Levels_Screenshots
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW();
ALTER TABLE Levels_Screenshots
    ADD COLUMN created_by INTEGER REFERENCES Users(id);