ALTER TABLE Game_Graduation
    RENAME COLUMN id_land TO game_id;
ALTER TABLE Game_Graduation
    RENAME COLUMN id_user TO user_id;
ALTER TABLE Game_Graduation
    ADD FOREIGN KEY (game_id) REFERENCES Games(id);
ALTER TABLE Game_Graduation
    ADD FOREIGN KEY (user_id) REFERENCES Users(id);

ALTER TABLE Land_Authors RENAME TO Game_Authors;
ALTER TABLE Game_Authors
    RENAME COLUMN id_land TO game_id;
ALTER TABLE Game_Authors
    RENAME COLUMN id_author TO author_id;
ALTER TABLE Game_Authors
    ADD FOREIGN KEY (game_id) REFERENCES Games(id);
ALTER TABLE Game_Authors
    ADD FOREIGN KEY (author_id) REFERENCES Authors(id);

-----------------------
ALTER TABLE Levels
    RENAME COLUMN land_id TO game_id;
ALTER TABLE Levels
    DROP COLUMN next_id;
ALTER TABLE Levels
    RENAME COLUMN id_land TO land_id;
-----------------------

ALTER TABLE Roles
    RENAME COLUMN nom to name;

ALTER TABLE Land_part RENAME TO Game_Parts;
ALTER TABLE Game_Parts
    DROP COLUMN difficulty;
ALTER TABLE Game_Parts
    RENAME COLUMN id_land TO game_id;
    
ALTER TABLE Thumbnail_Link RENAME TO Thumbnail_Links;
ALTER TABLE Thumbnail_Links
    RENAME COLUMN id_seq TO sequence_id;
ALTER TABLE Thumbnail_Links
    RENAME COLUMN id_level TO level_id;

ALTER TABLE Thumbnail_Sequence RENAME TO Thumbnail_Sequences;
ALTER TABLE Thumbnail_Sequences
    RENAME COLUMN id_land_part TO game_part_id;

ALTER TABLE Levels
    DROP COLUMN game_id;

-------------------------------------------------------
---- Traitement des premiers niveaux des séquences ----
INSERT INTO Thumbnail_Links (sequence_id, order_num, level_id)
    SELECT id, -1, id_start FROM Thumbnail_sequences;
UPDATE Thumbnail_Links
    SET order_num = order_num + 1000;
WITH renumbered AS (
    SELECT
        sequence_id,
        order_num,
        ROW_NUMBER() OVER (PARTITION BY sequence_id ORDER BY order_num ASC) AS new_order_num
    FROM
        thumbnail_links
)
UPDATE thumbnail_links
SET order_num = renumbered.new_order_num
FROM renumbered
WHERE thumbnail_links.sequence_id = renumbered.sequence_id
AND thumbnail_links.order_num = renumbered.order_num;


ALTER TABLE Thumbnail_Sequences
    ADD COLUMN is_dimension BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE Thumbnail_Sequences
    SET is_dimension = TRUE
    WHERE id_start IN (SELECT id_base_level FROM dimension)
    AND id IN (
        SELECT sequence_id FROM thumbnail_links WHERE order_num = 2 AND level_id IN (
            SELECT id_destination_level FROM dimension
        )
    );


------------------
ALTER TABLE Thumbnail_Sequences
    ADD COLUMN has_labyrinth BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE Thumbnail_Sequences
    SET has_labyrinth = TRUE
    WHERE id IN (SELECT sequence_id FROM thumbnail_links WHERE order_num >= 2 AND coordinates IS NOT NULL)
    ;

-------------------------
DROP TABLE DIMENSION;
-------------------------

ALTER TABLE Thumbnail_sequences
    DROP COLUMN id_start;

-------------------------------------------------------
ALTER TABLE descriptions
    RENAME COLUMN id_level TO level_id;

ALTER TABLE Orders
    ADD COLUMN screenshot_id UUID REFERENCES Levels_Screenshots(id);
UPDATE Orders
    SET screenshot_id = (SELECT id FROM Levels_Screenshots WHERE Levels_Screenshots.level_id = Orders.id_level LIMIT 1);
ALTER TABLE Orders
    ALTER COLUMN screenshot_id SET NOT NULL;

ALTER TABLE Orders
    DROP COLUMN id_level;

ALTER TABLE Videos
    RENAME COLUMN id_level TO level_id;

-------------------------------------------------------

INSERT INTO Users (name, password, role) VALUES ('admin', 'admin', 5);

-------------------------------------------------------
-- modified --> updated conversion --
ALTER TABLE Games
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE Games
    RENAME COLUMN modified_by TO updated_by;
UPDATE Games
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE Games
    ALTER COLUMN updated_by SET NOT NULL;

ALTER TABLE Levels
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE Levels
    RENAME COLUMN modified_by TO updated_by;
UPDATE Levels
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE Levels
    ALTER COLUMN updated_by SET NOT NULL;

ALTER TABLE Game_Parts
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE Game_Parts
    RENAME COLUMN modified_by TO updated_by;
UPDATE Game_Parts
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE Game_Parts
    ALTER COLUMN updated_by SET NOT NULL;

ALTER TABLE Thumbnail_Sequences
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE Thumbnail_Sequences
    RENAME COLUMN modified_by TO updated_by;
UPDATE Thumbnail_Sequences
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE Thumbnail_Sequences
    ALTER COLUMN updated_by SET NOT NULL;

ALTER TABLE descriptions
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE descriptions
    RENAME COLUMN modified_by TO updated_by;
UPDATE descriptions
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE descriptions
    ALTER COLUMN updated_by SET NOT NULL;

ALTER TABLE Orders
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE Orders
    RENAME COLUMN modified_by TO updated_by;
UPDATE Orders
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE Orders
    ALTER COLUMN updated_by SET NOT NULL;

ALTER TABLE Videos
    RENAME COLUMN modified_at TO updated_at;
ALTER TABLE Videos
    RENAME COLUMN modified_by TO updated_by;
UPDATE Videos
    SET updated_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE updated_by IS NULL;
ALTER TABLE Videos
    ALTER COLUMN updated_by SET NOT NULL;

---------------------------------------------


---------------------------------------------

ALTER TABLE Items
    ADD COLUMN value INTEGER NOT NULL DEFAULT 0;
ALTER TABLE Items
    RENAME COLUMN description TO effect;

---------------------------------------------

ALTER TABLE Game_Parts
    ADD CONSTRAINT unique_name_game UNIQUE (name, game_id);
ALTER TABLE Items
    ALTER COLUMN in_game_id TYPE INTEGER USING in_game_id::integer; 
ALTER TABLE Items
    ADD CONSTRAINT unique_in_game_id_game UNIQUE (in_game_id, game_id);

-- Nouvelle table pour gérer les liens des labyrinthes
CREATE TABLE Level_Transitions_Laby (
    id UUID DEFAULT uuid_generate_v4(),
    source_level_id INTEGER NOT NULL,
    target_level_id INTEGER NOT NULL,
    type VARCHAR(10) NOT NULL CHECK (type IN ('up', 'right')),
    PRIMARY KEY(id),
    FOREIGN KEY(source_level_id) REFERENCES Levels(id),
    FOREIGN KEY(target_level_id) REFERENCES Levels(id),
    UNIQUE (source_level_id, type)
);

-- Conversion et migration des transitions 'left' en 'right' et 'down' en 'up'
INSERT INTO Level_Transitions_Laby (id, source_level_id, target_level_id, type)
SELECT id, target_level_id, source_level_id, 
       CASE 
           WHEN type = 'left' THEN 'right'::VARCHAR(10)
           WHEN type = 'down' THEN 'up'::VARCHAR(10)
       END
FROM Level_Transitions
WHERE type IN ('left', 'down');

-- Migration des transitions 'up' et 'right'
INSERT INTO Level_Transitions_Laby (id, source_level_id, target_level_id, type)
    SELECT id, source_level_id, target_level_id, type
    FROM Level_Transitions
    WHERE type IN ('up', 'right')
    ON CONFLICT DO NOTHING;

-- Suppression des anciennes transitions 'up' 'right' 'left' 'down' dans la table de transitions normales
DELETE FROM Level_Transitions
    WHERE type IN ('up', 'right', 'left', 'down');

-- Modification du type de transition pour utiliser un string enum
ALTER TABLE Level_Transitions
    ALTER COLUMN type TYPE VARCHAR(10);
-- Ajout de la contrainte de type
ALTER TABLE Level_Transitions
    ADD CONSTRAINT type_check CHECK (type IN ('simple', 'dimension'));

-- Suppression de l'ancien type
DROP TYPE Level_Transition_Type;

------------------
ALTER TABLE Games
    ADD CONSTRAINT unique_name UNIQUE (name);

UPDATE Levels_Screenshots
    SET created_by = (SELECT id FROM Users WHERE name = 'admin' LIMIT 1)
    WHERE created_by IS NULL;
ALTER TABLE Levels_Screenshots
    ALTER COLUMN created_by SET NOT NULL;

------------------
ALTER TABLE Videos
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW();
ALTER TABLE Videos
    ADD COLUMN lives_lost INTEGER DEFAULT 0;
ALTER TABLE Videos
    ADD CONSTRAINT lives_lost_check CHECK (lives_lost >= 0);

------------------
INSERT INTO Roles VALUES (6, 'developer');
INSERT INTO Roles VALUES (7, 'tester');

------------------
-- After tests
ALTER Sequence Thumbnail_sequence_id_seq RESTART WITH 450;
ALTER Sequence Lands_id_seq RENAME TO Games_id_seq;
ALTER Sequence Games_id_seq RESTART WITH 80;
ALTER Sequence Land_part_id_seq RENAME TO Game_parts_id_seq;
ALTER Sequence Game_parts_id_seq RESTART WITH 150;
ALTER Sequence Users_id_seq RESTART WITH 80;