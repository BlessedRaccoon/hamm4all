import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1711879725372 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        WITH ranked_sequences AS (
            SELECT id,
                   game_part_id,
                   order_num,
                   ROW_NUMBER() OVER (PARTITION BY game_part_id ORDER BY order_num, id) as new_order_num
            FROM thumbnail_sequences
        )
        UPDATE thumbnail_sequences
        SET order_num = rs.new_order_num
        FROM ranked_sequences rs
        WHERE thumbnail_sequences.id = rs.id;          
        `)

        await queryRunner.query(`
        ALTER TABLE thumbnail_sequences
        ADD CONSTRAINT unique_game_part_id_order_num UNIQUE (game_part_id, order_num);
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TABLE thumbnail_sequences
        DROP CONSTRAINT unique_game_part_id_order_num;
        `)

        // This is a bit tricky, because we can't just revert the previous query
    }

}
