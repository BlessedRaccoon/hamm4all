import { Body, Controller, Logger, Post, Res, UseGuards } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { H4A_API, LoginBody, SignupBody } from "@hamm4all/shared";
import { Response } from "express";
import { AuthGuard } from "./auth.guard";
import { Throttle, ThrottlerGuard } from "@nestjs/throttler";

@Controller({
    "version": "1"
})
export class AuthController {

    private readonly AUTH_COOKIE_NAME = 'Authentication';
    private readonly logger = new Logger(AuthController.name);

    constructor(
        private readonly authService: AuthService
    ) {}

    @Post(H4A_API.v1.auth.login.getFullRawUri())
    @UseGuards(ThrottlerGuard)
    @Throttle({"default": {"limit": 3, "ttl": 60_000}})
    async login(
        @Body() {username, password}: LoginBody,
        @Res({"passthrough": true}) res: Response
    ) {
        const {token} = await this.authService.login(username, password);
        res.cookie(this.AUTH_COOKIE_NAME, token, {
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            sameSite: 'strict',
            maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 years
        });
    }

    @Post(H4A_API.v1.auth.signup.getFullRawUri())
    @UseGuards(ThrottlerGuard)
    @Throttle({"default": {"limit": 1, "ttl": 60_000}})
    async signup(
        @Body() {username, password}: SignupBody,
        @Res({"passthrough": true}) res: Response
    ): Promise<void> {
        const {token} = await this.authService.signup(username, password);
        res.cookie(this.AUTH_COOKIE_NAME, token, {
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            sameSite: 'strict',
            maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 years
        });
    }

    @Post(H4A_API.v1.auth.logout.getFullRawUri())
    @UseGuards(AuthGuard(1))
    async logout(
        @Res({"passthrough": true}) res: Response
    ): Promise<void> {
        res.clearCookie(this.AUTH_COOKIE_NAME);
    }
}