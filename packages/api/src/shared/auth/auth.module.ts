import { Module } from "@nestjs/common";
import { UsersModule } from "../../common/users/users.module";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "../../database/entities/users/users.entity";
import { RoleEntity } from "../../database/entities/roles/roles.entity";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";

@Module({
    imports: [
        UsersModule,
        TypeOrmModule.forFeature([UserEntity, RoleEntity, AuthTokenEntity]),
        ConfigModule,
    ],
    controllers: [AuthController],
    providers: [AuthService],
    exports: [AuthService]
})
export class AuthModule {}