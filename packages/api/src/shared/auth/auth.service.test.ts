import { HttpException, INestApplication } from "@nestjs/common";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserEntity } from "../../database/entities/users/users.entity";
import { AuthService } from './auth.service';
import { h4ABeforeEach } from '../../test/tests.helper';

describe('Auth', () => {

    let app: INestApplication;
    let authService: AuthService;
    let usersRepository : Repository<UserEntity>
    
    beforeAll(async () => {

        const {application, testModule} = await h4ABeforeEach()
        app = application

        app = testModule.createNestApplication();
        await app.init();

        authService = testModule.get(AuthService);
        usersRepository = testModule.get(getRepositoryToken(UserEntity))
    })

    it("should register the token", async () => {
        const date = new Date();
        const user: UserEntity = usersRepository.create({
            "id": 1,
            "name": "admin",
            "roleID": 1,
            "password": Buffer.from("dummy"),
            "experience": 0,
            "createdAt": date
        })

        const token = await authService.createAndSaveToken(user);

        expect(token).toHaveLength(AuthService.TOKEN_LENGTH);
        expect(await authService.getUserToken(user)).toEqual(token);
    })

    describe('login', () => {
        it('No user matches', async () => {
            expect(authService.login("ololo", "ololo")).rejects.toThrow(HttpException);
        })
    })

})