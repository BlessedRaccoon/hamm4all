import { BlobService } from "./blobs.service";
import { Request, Response } from "express";
import { Controller, ForbiddenException, Get, Header, HttpException, Param, ParseUUIDPipe, Res, StreamableFile } from "@nestjs/common";
import { H4A_API } from "@hamm4all/shared";
import { createReadStream } from "fs";

@Controller()
export class BlobController {

    constructor(private readonly fileService: BlobService) {}

    @Get(H4A_API.v1.files.get.getFullRawUri())
    @Header('Cache-Control', `max-age=${60 * 60 * 24 * 365}`)
    public async getBlobInfo(@Param("id", new ParseUUIDPipe()) id: string) {
        const blobInfo = await this.fileService.getFileFromDatabase(id);
        return blobInfo;
    }

    @Get(H4A_API.v1.files.getRaw.getFullRawUri())
    @Header('Cache-Control', `max-age=${60 * 60 * 24 * 365}`)
    public async getBlob(@Param("id", new ParseUUIDPipe()) id: string, @Res({"passthrough": true}) res: Response) {
        const blobInfo = await this.fileService.getFileFromDatabase(id);
        const blob = this.fileService.getPathToFile(blobInfo.id);
        this.fileService.checkIfFileExists(blob);
        const readStream = createReadStream(blob)
        res.contentType(blobInfo.media_type);
        return new StreamableFile(readStream)
    }

    public async addBlob(req: Request, res: Response) {
        if (! this.hasNecessaryRole(req.user?.roleID, 1)) throw new ForbiddenException("You don't have permission to access this resource")
        if (! req.files) {
            return new HttpException("No file provided", 400)
        }

        const blob = await this.fileService.add(req.files.blob instanceof Array ? req.files.blob[0] : req.files.blob);

        res.json(blob);
    }

    protected hasNecessaryRole(role: number | undefined, needed: number): boolean {
        if (role === undefined) return false;
        return role >= needed;
    }

}