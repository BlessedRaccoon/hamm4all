import { Module } from "@nestjs/common";
import { BlobController } from "./blobs.controller";
import { BlobService } from "./blobs.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BlobEntity } from "../../database/entities/blobs.entity";

@Module({
    imports: [TypeOrmModule.forFeature([BlobEntity])],
    controllers: [BlobController],
    providers: [BlobService],
    exports: [BlobService]
})
export class BlobModule {}