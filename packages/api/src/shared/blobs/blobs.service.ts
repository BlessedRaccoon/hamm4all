import * as path from 'path';
import * as fs from 'fs';
import { BlobDto } from './types/database-blob.dto';
import { HttpException, Injectable } from '@nestjs/common';
import { BLOBS_DIRECTORY } from '../filesystem/paths';
import { InjectRepository } from '@nestjs/typeorm';
import { BlobEntity } from '../../database/entities/blobs.entity';
import { Repository } from 'typeorm';
import { APILogger } from '../logger/api-logger.service';

export const NESTED_DIRECTORIES_AMOUNT = 2;
export const CHARACTERS_PER_PATH = 2;

const ONE_KB = 1024;
const ONE_MB = ONE_KB * ONE_KB;

export const MAX_FILE_SIZE = 10 * ONE_MB;

interface FileToSave {
    size: number;
    mimetype: string;
    data: Buffer | string;
}

@Injectable()
export class BlobService {
    private readonly logger = new APILogger('BlobService');

    constructor(
        @InjectRepository(BlobEntity) private readonly blobsRepository: Repository<BlobEntity>,
    ) {}

    async add(file: FileToSave, allowedMimeTypes?: string[], uploaderID?: number): Promise<BlobDto> {

        if (file.size > MAX_FILE_SIZE) {
            throw new Error(`File size is too big. Max file size is ${MAX_FILE_SIZE / ONE_MB} MB.`);
        }

        if (allowedMimeTypes && !allowedMimeTypes.includes(file.mimetype)) {
            throw new Error(`File type is not allowed. Allowed types are: ${allowedMimeTypes.join(', ')}`);
        }

        const [blob] = await this.blobsRepository.save([{
            "media_type": file.mimetype,
            "size": file.size,
            "updated_by": uploaderID,
        }])
        this.logger.debug(`File ${blob.id} created in database`);

        this.store(blob.id, file.data);
        return blob;
    }

    /**
     * Saves file to file system
     * @param id The id of the file
     * @param file The file to save
     * @returns The path to which the file was stored
     */
    private async store(id: string, file: Buffer | string): Promise<string> {
        const pathToFile = this.getPathToFile(id);

        if (!fs.existsSync(path.dirname(pathToFile))) {
            fs.mkdirSync(path.dirname(pathToFile), { recursive: true });
        }

        // Write the file in the chunk
        fs.writeFile(pathToFile, file, () => {
            this.logger.debug(`File ${id} stored at ${pathToFile}`);
        });

        return pathToFile;
    }

    getPathToFile(fileNameUUID: string): string {
        let pathToFile = BLOBS_DIRECTORY;

        for (let nest = 0; nest < NESTED_DIRECTORIES_AMOUNT; nest++) {
            const startingRelevantLettersIndex = nest * CHARACTERS_PER_PATH;
            const relevantLetters = fileNameUUID.slice(
                startingRelevantLettersIndex,
                startingRelevantLettersIndex + CHARACTERS_PER_PATH,
            );
            pathToFile = path.join(pathToFile, relevantLetters);
        }

        return path.join(pathToFile, fileNameUUID);
    }

    /**
     * Fetches a file from the storage system.
     * @param fileName The name of the file to get.
     * @returns The file in a Buffer format.
     */
    async getFileFromStorage(fileName: string): Promise<Buffer> {
        const pathToFile = this.getPathToFile(fileName);

        if (!fs.existsSync(pathToFile)) {
            throw new HttpException('File not found', 404);
        }

        const file = fs.readFileSync(pathToFile);

        return file;
    }

    async fileExists(fileName: string): Promise<boolean> {
        const pathToFile = this.getPathToFile(fileName);
        return fs.promises
            .access(pathToFile) // Check if the file exists. We use access instead of exists because it is asynchronous.
            .then(() => true)
            .catch(() => false);
    }

    async getFileFromDatabase(fileID: string): Promise<BlobDto> {
        const file = await this.blobsRepository.findOne({
            "where": {
                "id": fileID,
            }
        });

        if (!file) {
            throw new HttpException(`Le fichier ${fileID} n'a pas été trouvé`, 404);
        }

        return file;
    }

    checkIfFileExists(pathToFile: string) {
        if (!fs.existsSync(pathToFile)) {
            throw new HttpException(`Le fichier n'existe pas`, 404);
        }
    }
}
