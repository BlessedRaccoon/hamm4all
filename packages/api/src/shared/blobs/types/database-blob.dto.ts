export interface BlobDto {
    id: string;
    media_type: string;
    size: number;
}