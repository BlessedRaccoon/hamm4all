import "dotenv/config"
import { z } from "zod";

const EnvironmentSchema = z.object({
    "DB_NAME": z.string(),
    "DB_USER": z.string(),
    "DB_PASSWORD": z.string(),
    "DB_HOST": z.string(),
    "DB_PORT": z.string()
        .refine(port => parseInt(port) > 0 && parseInt(port) < 65535, { message: "Port must be between 0 and 65535" }),

    "JWT_SECRET": z.string(),
})

export type Environment = z.infer<typeof EnvironmentSchema>

export function validateEnvironment(config: Record<string, unknown>) {

    const parsedConfig = EnvironmentSchema.parse(config);

    return parsedConfig;
}

export const ENV = validateEnvironment(process.env)