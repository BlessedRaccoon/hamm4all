import { Controller, Get } from "@nestjs/common";
import { LanguagesService } from "./languages.service";
import { H4A_API } from "@hamm4all/shared";


@Controller()
export class LanguagesController {

    constructor(
        private readonly languagesService: LanguagesService
    ) {}

    @Get(H4A_API.v1.languages.list.getFullRawUri())
    public async getLanguages() {
        return this.languagesService.getLanguages();
    }

}