import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Request } from "express";
import { Observable } from "rxjs";

export type SupportedLanguage = "fr" | "en" | "es"

@Injectable()
export class LanguagesGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest<Request>();

        const languageInQueryParams = request.query.language
        if (this.isValidLanguage(languageInQueryParams)) {
            request.language = languageInQueryParams
        }

        const languageInHeader = request.headers["accept-language"]
        if (this.isValidLanguage(languageInHeader)) {
            request.language = languageInHeader;
            return true;
        }

        request.language ??= "fr"

        return true;
    }

    isValidLanguage(language: any): language is SupportedLanguage {
        return ["fr", "en", "es"].includes(language)
    }

}