import { Module } from "@nestjs/common";
import { LanguagesController } from "./languages.controller";
import { LanguagesService } from "./languages.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LanguageEntity } from "../../database/entities/languages/language.entity";


@Module({
    "controllers": [LanguagesController],
    "providers": [LanguagesService],
    "imports": [TypeOrmModule.forFeature([LanguageEntity])],
    "exports": [LanguagesService],
})
export class LanguagesModule {}