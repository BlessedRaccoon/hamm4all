import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { LanguageEntity } from "../../database/entities/languages/language.entity";
import { Repository } from "typeorm";

@Injectable()
export class LanguagesService {

    constructor(
        @InjectRepository(LanguageEntity) private readonly languageRepository: Repository<LanguageEntity>
    ) {}

    public async getLanguages(): Promise<LanguageEntity[]> {
        return this.languageRepository.find();
    }


}