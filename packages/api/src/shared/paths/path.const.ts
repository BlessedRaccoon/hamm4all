export function getLevelPath(landId: number, levelName: string): string {
    return `/games/${landId}/levels/${levelName}`;
}

export function getItemPath(itemId: number): string {
    return `/assets/items/${itemId}.svg`;
}

export function getEFUser(userId: string): string {
    return `https://eternalfest.net/users/${userId}`;
}

export default {
    getLevelPath,
    getItemPath,
    getEFUser
}