INSERT INTO Roles VALUES
    (1, 'guest'),
    (2, 'vip'),
    (3, 'moderator'),
    (4, 'administrator'),
    (5, 'owner'),
    (6, 'developer'),
    (7, 'tester');

INSERT INTO Users VALUES
    (1, 'test1', '\x7ff1d62df3b3420ed2141eb8006a1429451c42a3d7507e272983c50701252db172e6b6f25043591fa09f3f5762870cae5c0bc902ee8f23529daa9a0e9f2bf65d55098b409d38f061a21570946a807bc4', 1, DEFAULT, 0, NULL),
    (4, 'test4', '\xd0daf70c5d37b47ddb86b5e16b7edc4d1ef5a3ede58dae5dd53272cd5d63a9ce100f1f7f971d267630e2a002856ba28902c6c63be9a8efa6f00759d686f30e243dea7b17f4cf648f37744e87af805321', 4, DEFAULT, 0, NULL),
    (7, 'test7',
    '\xf4e7502996cd27cce5c9f03885e9d76b3f7e9117db4cedeffac343991c305ce8a4d881625eef163df344a139fb8dde734cd7777af5a216c1bf1a4220a814e35cc4434d2c466e9bb13366f0a7c580e97b', 7, DEFAULT, 0, NULL);

INSERT INTO Authors VALUES
    (1, 'maxdefolsch'),
    (2, 'Jj'),
    (3, 'Sky');

-- Populating 'Languages'
INSERT INTO languages (code) VALUES 
('en'),
('fr'),
('es');

-- Populating 'Item Type'
INSERT INTO item_type (id, type) VALUES
(1, 'effect'),
(2, 'points');

-- Populating 'Game Authors'
INSERT INTO game_authors (author_id, game_id) VALUES
(1, 1),
(2, 2),
(3, 3);