import { Controller, Post, UseGuards } from "@nestjs/common";
import { TestService } from "./test.service";
import { AuthGuard } from "../auth/auth.guard";


@Controller({
    "path": "/api/v1/test",
    "version": "1"
})
export class TestController {

    constructor(private readonly testService: TestService) {}

    @Post("/run/start")
    @UseGuards(AuthGuard(7))
    async startTestRun() {
        return this.testService.startTestRun();
    }

    @Post("/run/end")
    @UseGuards(AuthGuard(7))
    async endTestRun() {
        return this.testService.endTestRun();
    }

}