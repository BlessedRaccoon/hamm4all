import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { TestService } from "./test.service";
import { TestController } from "./test.controller";


@Module({
    "imports": [TypeOrmModule.forFeature([AuthTokenEntity])],
    "providers": [TestService],
    "controllers": [TestController]
})
export class TestModule {}