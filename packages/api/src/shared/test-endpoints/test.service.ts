import { Injectable } from "@nestjs/common";
import { InjectDataSource } from "@nestjs/typeorm";
import { DataSource } from "typeorm";
import fs from "fs";
import path from "path";

@Injectable()
export class TestService {

    private readonly testSetAsSQL;

    constructor(
        @InjectDataSource() private readonly dataSource: DataSource
    ) {
        const testSetPath = path.join(process.cwd(), "src/shared/test-endpoints/test-set.sql");
        this.testSetAsSQL = fs.readFileSync(testSetPath).toString();
    }

    async startTestRun() {

        for (const metadata of this.dataSource.entityMetadatasMap) {
            const tableName = metadata[1].tableName
            await this.dataSource.query(`CREATE TEMPORARY TABLE ${tableName} (LIKE ${tableName} INCLUDING ALL)`)
        }

        await this.dataSource.query(this.testSetAsSQL);
    }

    async endTestRun() {
        for (const metadata of this.dataSource.entityMetadatasMap) {
            const tableName = metadata[1].tableName
            await this.dataSource.query(`DROP TABLE pg_temp.${tableName} CASCADE`)
        }
    }

    


}