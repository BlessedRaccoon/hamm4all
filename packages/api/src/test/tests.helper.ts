import { Test } from "@nestjs/testing";
import { TestApiModule } from "../api.module";
import { INestApplication } from "@nestjs/common";


export async function h4ABeforeEach() {

    const testModule = await Test.createTestingModule({
        "imports": [
            TestApiModule
        ]
    })
    .compile();

    const application = testModule.createNestApplication();
    await application.init();

    return {
        application,
        testModule: testModule
    }
}

export async function h4AAfterEach(application: INestApplication) {
    await application.close();
}