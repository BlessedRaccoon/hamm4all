export enum H4ARole {
    NOT_AUTHENTICATED = 0,
    GUEST = 1,
    VIP = 2,
    MODERATOR = 3,
    ADMINISTRATOR = 4,
    OWNER = 5,
    DEVELOPER = 6,
    TESTER = 7,
}