import { Length } from "class-validator";

export class LoginBody {

    @Length(3, 32)
    declare username: string;

    @Length(5, 64)
    declare password: string;
}