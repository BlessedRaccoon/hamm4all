import { HTTPMethod } from "../../../api.call";
import { APIEndpointAction, APIEndpointErrorParams, APIEndpointSuccessParams, APIVersion, URIBuilder } from "../../../api.endpoint";
import { H4ARole } from "../../api.roles";
import { SignupBody } from "./signup.body";

export class SignupCall extends APIEndpointAction<{}, SignupBody, {}> {
    protected getVersion(): APIVersion {
        return 1
    }

    protected getUriBuilder(): URIBuilder<{}> {
        return new URIBuilder()
            .addPath("auth")
            .addPath("signup")
    }

    public getMethod(): HTTPMethod {
        return "POST"
    }

    public getRole(): H4ARole {
        return H4ARole.NOT_AUTHENTICATED
    }

    public override getErrorMessage(params: APIEndpointErrorParams<{}, SignupBody>): string {
        return `Impossible de créer l'utilisateur ${params.body.username} : ${params.error.data.message}`
    }

    public override getSuccessMessage(params: APIEndpointSuccessParams<{}, {}>): string {
        return `Bienvenue !`
    }
}