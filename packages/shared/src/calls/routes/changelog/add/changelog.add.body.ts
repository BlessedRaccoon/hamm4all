import { ArrayNotEmpty, IsArray, IsNotEmpty, IsString, MaxLength } from "class-validator";

export class AddChangelogBody {
    @IsNotEmpty()
    declare versionName: string;

    @IsNotEmpty()
    @MaxLength(8192)
    declare description: string;
}