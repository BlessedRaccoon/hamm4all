import { Item } from "../items/items.types";

export interface Family {
    id: number;
    name: {
        en: string;
        fr: string;
        es: string;
    };
    items: OldFamilyItem[];
}

// This shall be converted to regular Item once the Families is migrated to API
export interface OldFamilyItem {
    id: number;
    rarity: number;
    unlock?: number;
    value?: number | string;
    name: {
        en: string;
        fr: string;
        es: string;
    };
    img: string;
}