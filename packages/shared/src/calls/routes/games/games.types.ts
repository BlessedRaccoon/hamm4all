import { Author } from "../authors/authors.types";

export interface Game extends ReducedGame {
    difficulty: number | null;
    description: string;
    has_variable_difficulty: boolean;
    released_at: string;
    level_count: number;
    authors: Author[];
}

export interface ReducedGame {
    id: number;
    name: string;
    thumbnail_id: string;
}