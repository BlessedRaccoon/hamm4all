import { IsNumber, Max, Min } from "class-validator";


export class GameGradeAddBody {
    
    @Min(0)
    @Max(10)
    @IsNumber()
    declare grade: number;
}