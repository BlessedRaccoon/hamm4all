import { IsInt } from "class-validator";
import { Type } from "class-transformer";

export class LandParam {
    @IsInt()
    @Type(() => Number)
    declare landID: number;
}