import { ReducedLevel } from "../../levels/levels.types";


export interface ReducedLand {
    id: number;
    name: string;
}

export interface Land extends ReducedLand {
    levels: ReducedLevel[];
    gameID: number;
}