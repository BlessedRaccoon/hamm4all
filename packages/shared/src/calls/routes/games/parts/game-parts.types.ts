import { ReducedGame } from "../games.types";
import { ReducedUser } from "../../users/users.types";

export interface GamePart {
    id: number;
    name: string
    game: ReducedGame;
    description: string;
    created_at: string;
    updated_at: string;
    updated_by: ReducedUser;
    level_count: number;
}

export interface ReducedGamePart {
    id: number;
    name: string
    game: ReducedGame;
}