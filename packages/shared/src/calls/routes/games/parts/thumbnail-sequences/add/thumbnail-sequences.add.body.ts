import { IsBoolean } from "class-validator";


export class ThumbnailSequenceAddBody {
    @IsBoolean()
    declare firstLevelApart: boolean;

    @IsBoolean()
    declare hasLabyrinth: boolean;
}