import { ArrayNotEmpty, IsArray, IsInt } from "class-validator";

export class ThumbnailSequenceOrderBody {
    @IsArray()
    @ArrayNotEmpty()
    @IsInt({ each: true })
    declare order: number[]
}