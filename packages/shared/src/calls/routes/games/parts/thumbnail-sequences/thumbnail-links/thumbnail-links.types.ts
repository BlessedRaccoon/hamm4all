import { ReducedLevel } from "../../../../levels/levels.types"
import { LevelCoordinates } from "./add/thumbnail-links.add.body"


export class ThumbnailLink {
    declare orderNumber: number
    declare level: ReducedLevel
    declare position?: LevelCoordinates
}