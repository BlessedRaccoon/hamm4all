export type GamesThumbnailUpdateBody = FormData

export const GAMES_THUMBNAIL_FILE_FIELD_NAME = 'thumbnail'