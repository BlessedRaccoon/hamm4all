import { HTTPMethod } from "../../../api.call";
import { APIEndpointAction, APIEndpointErrorParams, APIEndpointSuccessParams, APIVersion, URIBuilder } from "../../../api.endpoint";
import { H4ARole } from "../../api.roles";
import { GameParam } from "../games.params";
import { GamesThumbnailUpdateBody } from "./games.thumbnail.update.body";


export class GamesThumbnailUpdateCall extends APIEndpointAction<GameParam, GamesThumbnailUpdateBody, {}> {
    
    protected getVersion(): APIVersion {
        return 1;
    }

    protected getUriBuilder() {
        return new URIBuilder<GameParam>()
            .addPath("games")
            .addParam("gameID")
            .addPath("thumbnail")
    }

    public getMethod(): HTTPMethod {
        return "PUT"
    }

    public override isFileForm(): boolean {
        return true
    }

    public getRole(): H4ARole {
        return H4ARole.GUEST
    }

    public override getErrorMessage(params: APIEndpointErrorParams<GameParam, FormData>): string {
        return `Une erreur est survenue lors de la mise à jour de la miniature du jeu : ${params.error.data.message}`
    }

    public override getSuccessMessage(params: APIEndpointSuccessParams<GameParam, FormData, {}>): string {
        return `La miniature du jeu a bien été mise à jour !`
    }
}