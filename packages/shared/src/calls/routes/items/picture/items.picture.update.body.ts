export type ItemsPictureUpdateBody = FormData

export const ITEMS_PICTURE_FILE_FIELD_NAME = 'thumbnail'