import { IsNotEmpty, IsString } from "class-validator";
import { GameParam } from "../games/games.params";


export class LevelsParam extends GameParam {

    @IsString()
    @IsNotEmpty()
    declare levelName: string;
}