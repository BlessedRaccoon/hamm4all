import { HTTPMethod } from "../../../api.call";
import { APIEndpointAction, APIVersion, URIBuilder } from "../../../api.endpoint";
import { H4ARole } from "../../api.roles";
import { RandomLevel } from "./levels.random.types";

export class LevelsRandomCall extends APIEndpointAction<{}, {}, RandomLevel> {

    protected getVersion(): APIVersion {
        return 1
    }

    protected getUriBuilder() {
        return new URIBuilder()
            .addPath("random-level")
    }

    public getRole(): H4ARole {
        return H4ARole.NOT_AUTHENTICATED
    }

    public override getMethod(): HTTPMethod {
        return "POST"
    }
}
