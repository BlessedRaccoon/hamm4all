import { Type } from "class-transformer";
import { IsArray, IsNumber, Max, Min, ValidateNested } from "class-validator";

export class FruitOrderUpdateBody {
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => Coordinates)
    order: Coordinates[]
}

class Coordinates {
    @IsNumber()
    @Min(0)
    @Max(21)
    declare x: number;

    @IsNumber()
    @Min(0)
    @Max(25)
    declare y: number;
}