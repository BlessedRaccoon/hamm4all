export type LevelsScreenshotUploadBody = FormData

export const LEVELS_SCREENSHOT_FILE_FIELD_NAME = 'screenshot'