export interface LevelTransitionsClassic {
    previous: LevelTransitionClassic[];
    next: LevelTransitionClassic[];
}

export interface LevelTransitionClassic {
    level: {
        id: number;
        name: string;
    };
    type: LevelTransitionClassicType;
}

export type LevelTransitionClassicType = "simple" | "dimension";