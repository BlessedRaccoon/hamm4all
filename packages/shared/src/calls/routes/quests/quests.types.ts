import { Item } from "../items/items.types";

export interface Quest {
    id: number;
    name: {
        en: string;
        fr: string;
        es: string;
    }
    desc: {
        en: string;
        fr: string;
        es: string;
    }
    items: OldQuestItem[];
    give?: {
        families?: Array<FamilyAction>
        tokens?: number
        bank?: number;
        mode?: string;
        option?: string;
        log?: boolean
    }
    remove?: {
        families?: Array<FamilyAction>
    }
}

interface FamilyAction {
    id: number;
    name?: {
        en: string;
        fr: string;
        es: string;
    }
}

// This shall be converted to regular QuestItem once the Quests is migrated to API
export interface OldQuestItem {
    id: number;
    quantity: number;
    name: {
        en: string;
        fr: string;
        es: string;
    };
    img: string;
}

export interface QuestItem extends Omit<Item, "unlock" | "rarity"> {
    quantity: number;
}