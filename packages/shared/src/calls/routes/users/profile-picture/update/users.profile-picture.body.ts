export type UserProfilePictureUpdateBody = FormData

export const USERS_PROFILE_PICTURE_FILE_FIELD_NAME = 'profilePicture'