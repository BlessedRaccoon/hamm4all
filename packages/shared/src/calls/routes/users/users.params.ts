import { Type } from "class-transformer";
import { IsInt } from "class-validator";

export class UserParam {
    @IsInt()
    @Type(() => Number)
    declare userID: number;
}