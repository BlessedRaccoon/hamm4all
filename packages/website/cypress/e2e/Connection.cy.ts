describe('Connection setup', function () {

    beforeEach(() => {
        cy.startTest()
    })

    afterEach(() => {
        cy.endTest()
    })

    it("Username does not exist", function () {
        cy.visit('/connect')

        cy.get("[data-cy=login-username]").type("this_user_does_not_exist")
        cy.get("[data-cy=login-password]").type("password{enter}")

        cy.expectNotification("error")
    })

    it("Wrong password", function() {
        cy.visit('/connect')

        cy.get("[data-cy=login-username]").type("test1")
        cy.get("[data-cy=login-password]").type("password")

        cy.get("[data-cy=login-submit]").click()

        cy.expectNotification("error")
    })

    it("should connect", () => {
        cy.visit('/connect')

        cy.get("[data-cy=login-username]").type("test1")
        cy.get("[data-cy=login-password]").type("test1")

        cy.get("[data-cy=login-submit]").click()

        cy.expectNotification("success")

        cy.isAuthenticated("test1")

        cy.url().should('include', '/')
    })

    it("should redirect at the previous page", () => {
        cy.visit("/techniques")
        cy.get("[data-cy=connect-page-button]").click({"force": true})

        cy.url().should('include', '/connect')

        cy.get("[data-cy=login-username]").type("test1")
        cy.get("[data-cy=login-password]").type("test1")

        cy.get("[data-cy=login-submit]").click()

        cy.expectNotification("success")

        cy.url().should('include', '/techniques')
    })

    it("should not register (username already taken)", () => {
        cy.visit('/connect')

        cy.get("[data-cy=register-username]").type("test1")
        cy.get("[data-cy=register-password]").type("test1")

        cy.get("[data-cy=register-submit]").click()

        cy.expectNotification("error")
    })

    it("should register and redirect to the previous page", () => {
        cy.visit("/techniques")
        cy.get("[data-cy=connect-page-button]").click({"force": true})

        cy.url().should('include', '/connect')

        cy.get("[data-cy=register-username]").type("new_user")
        cy.get("[data-cy=register-password]").type("new_user")

        cy.get("[data-cy=register-submit]").click()

        cy.expectNotification("success")

        cy.url().should('include', '/techniques')
    })

    it("should logout", () => {
        cy.login("test1", "test1")

        cy.get("[data-cy=logout-button]").click({"force": true})

        cy.get("[data-cy=connect-page-button]").should("exist", {"force": true})
    })
})