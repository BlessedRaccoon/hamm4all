describe('Games tests', function () {

    beforeEach(() => {
        cy.startTest()
    })

    afterEach(() => {
        cy.endTest()
    })

    it("can't create game if role too low", function() {
        cy.login("test1", "test1")

        cy.visit("/games")
        cy.get("[data-cy=games-card-zone] .fa-plus").should("not.exist")
    })

    it("can create game if role high enough", function() {
        cy.login("test4", "test4")

        cy.visit("/games")
        cy.get("[data-cy=games-card-zone]").within(() => {
            cy.get("svg").should("exist")
        })
    })

    it("Unsuccessful game creation attempt", function() {
        cy.login("test4", "test4")

        cy.visit("/games")
        cy.get("[data-cy=games-card-zone]").within(() => {
            cy.get("svg").click()
        })

        cy.get(".modal.is-active [type=submit]").click()

        cy.expectNotification("error")
    })

    it("Successful game creation", function() {
        cy.addGame({
            "name": "Test game",
            "description": "Test description",
            "releaseDate": "2021-01-01",
            "difficulty": "fixed",
            "authors": ["1"]
        })

        cy.expectNotification("success")
    })

    it("should not have 2 games with the same name", function() {
        cy.addGame({
            "name": "Test game",
            "description": "Test description",
            "releaseDate": "2021-01-01",
            "difficulty": "fixed",
            "authors": ["1", "2"]
        })

        cy.addGame({
            "name": "Test game",
            "description": "Test description 2",
            "releaseDate": "2021-03-01",
            "difficulty": "variable",
            "authors": ["1", "3"]
        })

        cy.expectNotification("error")
    })

    describe("Games toolbar", function() {

        before(() => {
            cy.startTest()

            cy.addGame({
                "name": "Test game 1",
                "description": "Test description",
                "releaseDate": "2021-01-01",
                "difficulty": "fixed",
                "authors": ["1"]
            })

            cy.addGame({
                "name": "Test game 2",
                "description": "Test description 2",
                "releaseDate": "2021-03-01",
                "difficulty": "variable",
                "authors": ["1", "3"]
            })

            cy.addGame({
                "name": "Test game 3",
                "description": "Test description 3",
                "releaseDate": "2021-02-01",
                "difficulty": "fixed",
                "authors": ["1", "2"]
            })
        })

        after(() => {
            cy.endTest()
        })

        it("should filter games correctly", function() {
            cy.get("[data-cy=games-sorter]").select("name")
            cy.get("[data-cy=games-card-zone] .columns").within(() => {
                cy.get("a").eq(0).contains("Test game 1")
                cy.get("a").eq(1).contains("Test game 2")
                cy.get("a").eq(2).contains("Test game 3")
            })

            cy.get("[data-cy=games-sorter]").select("release_date")
            cy.get("[data-cy=games-card-zone] .columns").within(() => {
                cy.get("a").eq(0).contains("Test game 1")
                cy.get("a").eq(1).contains("Test game 3")
                cy.get("a").eq(2).contains("Test game 2")
            })

            cy.get("[data-cy=games-sorter]").select("id")
            cy.get("[data-cy=games-card-zone] .columns").within(() => {
                cy.get("a").eq(0).contains("Test game 1")
                cy.get("a").eq(1).contains("Test game 2")
                cy.get("a").eq(2).contains("Test game 3")
            })
        })

        it("should search games correctly", function() {
            cy.get("[data-cy=games-card-zone]").within(() => {
                cy.get("a").should("have.length", 3)
            })

            cy.get("[data-cy=games-card-zone] [type=search]").type("Test game 1")
            cy.get("[data-cy=games-card-zone]").within(() => {
                cy.get("a").should("have.length", 1)
                cy.get("a").contains("Test game 1")
            })

            cy.get("[data-cy=games-card-zone] [type=search]").clear().type("Test game 2")
            cy.get("[data-cy=games-card-zone]").within(() => {
                cy.get("a").should("have.length", 1)
                cy.get("a").contains("Test game 2")
            })

            cy.get("[data-cy=games-card-zone] [type=search]").clear().type("Test game 3")
            cy.get("[data-cy=games-card-zone]").within(() => {
                cy.get("a").should("have.length", 1)
                cy.get("a").contains("Test game 3")
            })
        })

    })
})