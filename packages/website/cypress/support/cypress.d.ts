/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable<Subject = any> {
        startTest(): Chainable<any>;
        endTest(): Chainable<any>;
        isAuthenticated(name: string): Chainable<any>;
        login(username: string, password: string): Chainable<any>;
        expectNotification(type: "success" | "error"): Chainable<any>;
        addGame(props: AddGameProps): Chainable<any>;
    }
}