export function getLocalDateDisplayFromDateString(dateString: string): string {
    return new Date(dateString).toLocaleDateString("fr-FR");
}