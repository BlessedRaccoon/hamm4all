import { faPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Clickable from "../clickable/clickable.component"
import styles from "./adder.module.css"

export function AdderIcon(props: Readonly<{toggleModal: (toggle: boolean) => void}>) {
    return <Clickable onClick={() => props.toggleModal(true)} className={styles.adder}>
        <FontAwesomeIcon icon={faPlus} size={"7x"} style={{color: "gray"}} />
    </Clickable>
}