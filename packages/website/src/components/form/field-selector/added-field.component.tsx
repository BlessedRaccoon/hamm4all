import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface AddedFieldProps {
    addonLabel: string;
    addonValue: string;
    value?: string;
    onChange: (value: string) => void;
    onRemove: () => void;
}

export default function AddedField({ addonLabel, addonValue, value, onChange, onRemove }: Readonly<AddedFieldProps>) {
    return (
        <div className="field has-addons">
            <div className="control">
                <div className='select'>
                    <select value={addonValue} tabIndex={-1}>
                        <option value={addonValue}>{addonLabel}</option>
                    </select>
                </div>
            </div>
            <div className="control is-expanded">
                <input className='input' type="text" defaultValue={value} onChange={(e) => onChange(e.target.value)} />
            </div>
            <div className="control">
                <button className={`button is-danger`} onClick={onRemove} tabIndex={-1}>
                    <FontAwesomeIcon icon={faTrash} />
                </button>
            </div>
        </div>
    );
};