import {v4} from "uuid"

export interface SelectorOption {
    label: string;
    value: string;
}

interface SelectorProps {
    options: SelectorOption[];
    onSelect: (selectedOption: SelectorOption) => void;
}

function SelectorRow({ label, value }: Readonly<SelectorOption>) {
    return <option value={value}>{label}</option>;
}

export default function Selector({ options, onSelect }: Readonly<SelectorProps>) {

    return (
        <div className="select">
            <select onChange={(e) => onSelect(options.find(opt => opt.value === e.target.value)!)}>
                <option value="">Select an option</option>
                {options.map((option) => (
                    <SelectorRow key={v4()} label={option.label} value={option.value}  />
                ))}
            </select>
        </div>
    );
};
