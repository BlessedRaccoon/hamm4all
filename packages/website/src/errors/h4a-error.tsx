

export abstract class H4AError extends Error {
    abstract getDisplay(): JSX.Element;
}