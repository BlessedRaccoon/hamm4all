import React, { ReactElement, useState } from "react";
import { Tabs } from "react-bulma-components";

interface H4ATabProps extends ReactElement<HTMLLinkElement> {}

interface H4ATabsProps {
    children: [
        ReactElement<H4ATabListProps>,
        ...Array<ReactElement<H4ATabPanelProps>>
    ];
    initialActiveIndex?: number;
}

interface H4ATabPanelProps {
    children: ReactElement
}

interface H4ATabListProps {
    children: Array<ReactElement<H4ATabProps>>
}

export function H4ATabList(props: Readonly<H4ATabListProps>) {
    return <div>{props.children}</div>
}

export function H4ATabPanel(props: Readonly<H4ATabPanelProps>) {
    return <div>
        {props.children}
    </div>
}

export function H4ATab(props: any) {
    return <>{props.children}</>
}

export function H4ATabs(props: Readonly<H4ATabsProps>) {

    const [selectedIndex, setSelectedIndex] = useState<number>(props.initialActiveIndex ?? 0)
    const [tabList, ...panels] = props.children

    function selectTab(index: number) {
        setSelectedIndex(index)
    }

    return <>
        <Tabs type={"boxed"} className="is-centered">
            {React.Children.map(tabList.props.children, (child, index) => {
                return (
                    <li
                        key={child.key}
                        className={index === selectedIndex ? "is-active" : ""}
                    >
                        <a href="#/" onClick={() => selectTab(index)}>
                            {child}
                        </a>
                    </li>
                )
            })}
        </Tabs>
        {React.Children.map(panels, (panel, index) => {
            return (
                <div key={panel.key} style={(index !== selectedIndex) ? {"display": "none"} : {}}>
                    {panel}
                </div>
            )
        })}
    </>
}