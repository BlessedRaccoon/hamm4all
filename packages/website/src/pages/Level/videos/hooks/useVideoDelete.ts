import { H4A_API, LevelsVideoParam } from "@hamm4all/shared";
import useAPIMutation from "../../../../core/api/useAPIMutation";

interface UseVideoDeleteProps extends LevelsVideoParam {
    closePopup?: () => void
}

export default function useVideoDelete(props: Readonly<UseVideoDeleteProps>) {
    const videoMutation = useAPIMutation({
        "endpoint": H4A_API.v1.levels.videos.delete,
        "params": props,
        "onSuccess": props.closePopup
    })

    return videoMutation

}