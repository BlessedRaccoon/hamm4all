import { H4A_API } from "@hamm4all/shared";
import useAPIQuery from "../../../hooks/use-api-query.hook";

export default function useUsers() {
    const usersQuery = useAPIQuery({
        "endpoint": H4A_API.v1.user.list,
        "params": {},
        "query": {}
    })

    return {
        users: usersQuery.data,
        ...usersQuery
    }
}