import { H4A_API } from "@hamm4all/shared";
import useAPIQuery from "../../../hooks/use-api-query.hook";

export default function useSession() {
    const session = useAPIQuery({
        "endpoint": H4A_API.v1.user.session,
        "params": {},
        "query": {},
    })

    return session
}