import { H4A_API, UserProfile } from "@hamm4all/shared";

export async function updateSession(): Promise<UserProfile> {
    const connectFunction = H4A_API.v1.user.session.getCallFunction({}, {})
    const connectQuery = await connectFunction({})
    return connectQuery.data
}