import { useState } from "react"
import { Button, Form } from "react-bulma-components"
import useSignup from "../hooks/useSignup"

export default function SignupForm({onSignup}: Readonly<{onSignup: () => void}>) {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const signupMutation = useSignup({
        "onSuccess": async () => onSignup()
    })

    return (
        <form id="register-form" onSubmit={(e) => e.preventDefault()}>
            <Form.Field>
                <Form.Label>Nom d'utilisateur *</Form.Label>
                <Form.Control>
                    <Form.Input type="text" name="username" maxLength={50} autoFocus required
                        onChange={(event) => setUsername(event.target.value)} data-cy={"register-username"} />
                </Form.Control>
            </Form.Field>
            <Form.Field>
                <Form.Label>Mot de passe *</Form.Label>
                <Form.Control>
                    <Form.Input type="password" name="password" autoFocus required
                        onChange={(event) => setPassword(event.target.value)} data-cy={"register-password"} />
                </Form.Control>
            </Form.Field>
            <Form.Field>
                <Form.Control>
                    <Button
                        className="is-link"
                        onClick={() => signupMutation.mutate({
                            "username": username,
                            "password": password
                        })}
                        data-cy={"register-submit"}
                    >
                        S'inscrire
                    </Button>
                </Form.Control>
            </Form.Field>
        </form>
    )
}