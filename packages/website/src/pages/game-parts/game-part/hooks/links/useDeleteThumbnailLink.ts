import { H4A_API, ThumbnailLinkParam } from "@hamm4all/shared";
import useAPIMutation from "../../../../../core/api/useAPIMutation";

interface UseDeleteThumbnailLinkProps extends ThumbnailLinkParam {

}

export default function useDeleteThumbnailLink(props: Readonly<UseDeleteThumbnailLinkProps>) {
    const DeleteThumbnailLinkMutation = useAPIMutation({
        "endpoint": H4A_API.v1.thumbnailSequences.links.delete,
        "params": props,
    })

    return DeleteThumbnailLinkMutation
}