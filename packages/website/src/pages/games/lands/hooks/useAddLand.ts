import { GameParam, H4A_API } from "@hamm4all/shared";
import useAPIMutation from "../../../../core/api/useAPIMutation";

interface UseAddLandProps extends GameParam {
    closePopup: () => void;
}

export default function useAddLand(props: Readonly<UseAddLandProps>) {
    const {gameID} = props
    const addMutation = useAPIMutation({
        "endpoint": H4A_API.v1.lands.add,
        "params": {gameID},
        "onSuccess": props.closePopup
    })

    return addMutation
}