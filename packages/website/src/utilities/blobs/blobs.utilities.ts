

export function getBlobPathFromId(id: string): string {
    return `/api/v1/files/${id}/raw`
}